define({ "api": [
  {
    "type": "post",
    "url": "/api/card/add",
    "title": "Add",
    "name": "Add_card",
    "group": "Card",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "front_img",
            "description": "<p>base64 encode string</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "back_img",
            "description": "<p>base64 encode string</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name on card</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "company_name",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "company_mobile",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "company_address",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "website",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>OK</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Bạn đã thêm card thành công</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Card info</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": \"OK\",\n    \"message\": \"Bạn đã thêm card thành công\",\n    \"data\": {\n        \"id\": \"10\",\n        \"user_id\": \"5\",\n        \"name\": \"Nguyen Vu Loc\",\n        \"mobile\": \"0908833061\",\n        \"email\": \"crazyfrogs2@mailnesia.com\",\n        \"address\": \"767A Nguyen Anh Thu\",\n        \"company_name\": null,\n        \"company_mobile\": null,\n        \"company_address\": null,   \n        \"website\": null,\n        \"status\": \"1\",\n        \"deleted\": \"0\",\n        \"front_img\": \"/uploads/123456789_uf.jpg\",\n        \"back_img\": \"/uploads/123456789_ub.jpg\",   \n        \"created_at\": \"2017-01-15 22:55:25\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/CardController.php",
    "groupTitle": "Card"
  },
  {
    "type": "delete",
    "url": "/api/card/delete/:id",
    "title": "Delete",
    "name": "Delete_card",
    "group": "Card",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>OK</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": \"OK\",\n  \"message\": \"Card đã xóa\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/CardController.php",
    "groupTitle": "Card"
  },
  {
    "type": "get",
    "url": "/api/card/get/:id",
    "title": "Get",
    "name": "Get_card_by_id",
    "group": "Card",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>OK</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Card info</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": \"OK\",\n    \"data\": {\n        \"id\": \"10\",\n        \"user_id\": \"5\",\n        \"name\": \"Nguyen Vu Loc\",\n        \"mobile\": \"0908833061\",\n        \"email\": \"crazyfrogs2@mailnesia.com\",\n        \"address\": \"767A Nguyen Anh Thu\",\n        \"company_name\": null,\n        \"company_mobile\": null,\n        \"company_address\": null,\n        \"website\": null,\n        \"status\": \"1\",\n        \"front_img\": \"/uploads/123456789_uf.jpg\",\n        \"back_img\": \"/uploads/123456789_ub.jpg\",\n        \"deleted\": \"0\",\n        \"created_at\": \"2017-01-15 22:55:25\",\n        \"balance\": \"50\",\n        \"design_deal_amount\": \"300\",\n        \"deisng_deal_status\": \"1\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/CardController.php",
    "groupTitle": "Card"
  },
  {
    "type": "post",
    "url": "/api/card/initdefault",
    "title": "Init Default",
    "name": "Init_default_card",
    "group": "Card",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "front_img",
            "description": "<p>base64 encode string</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "back_img",
            "description": "<p>base64 encode string</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name on card</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "company_name",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "company_mobile",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "company_address",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "website",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>OK</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": \"OK\",\n    \"data\": {\n        \"id\": \"19\",\n        \"user_id\": \"7\",\n        \"name\": \"Nguyen Vu Loc\",\n        \"mobile\": \"01283538080\",\n        \"email\": \"asddasdas@ddef.com\",\n        \"address\": \"\",\n        \"company_name\": null,\n        \"company_mobile\": null,\n        \"company_address\": null\n        \"website\": null,\n        \"status\": \"1\",\n        \"deleted\": \"0\",\n        \"front_img\": \"/uploads/12430831_uf.jpg\",\n        \"back_img\": \"/uploads/123456789_ub.jpg\",\n        \"created_at\": \"2017-01-23 14:26:21\",\n        \"is_default\": \"1\",\n    }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/CardController.php",
    "groupTitle": "Card"
  },
  {
    "type": "get",
    "url": "/api/card/listshareable",
    "title": "List shareable",
    "name": "List_card_able_to_share",
    "group": "Card",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>OK</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": \"OK\",\n    \"data\": [\n        {\n            \"id\": \"19\",\n            \"user_id\": \"7\",\n            \"name\": \"Nguyen Vu Loc\",\n            \"mobile\": \"01283538080\",\n            \"email\": \"asddasdas@ddef.com\",\n            \"address\": \"\",\n            \"company_name\": null,\n            \"company_mobile\": null,\n            \"company_address\": null\n            \"website\": null,\n            \"status\": \"1\",\n            \"deleted\": \"0\",\n            \"created_at\": \"2017-01-23 14:26:21\",\n            \"is_default\": \"1\",\n        },\n        {\n            \"id\": \"18\",\n            \"user_id\": \"7\",\n            \"name\": \"Nguyen Vu Loc\",\n            \"mobile\": \"01283538080\",\n            \"email\": \"asddasdas@ddef.com\",\n            \"address\": \"\",\n            \"company_name\": null,\n            \"company_mobile\": null,\n            \"company_address\": null\n            \"website\": null,\n            \"status\": \"1\",\n            \"deleted\": \"0\",\n            \"created_at\": \"2017-01-23 14:26:21\",\n            \"is_default\": \"1\",\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/CardController.php",
    "groupTitle": "Card"
  },
  {
    "type": "get",
    "url": "/api/card/listcard",
    "title": "List card",
    "name": "List_card_id_belong_to_this_user",
    "group": "Card",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>OK</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "count",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>Mảng info card của user</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": \"OK\",\n  \"count\": 1,\n  \"data\": [\n      {\n          \"id\": \"14\",\n          \"user_id\": \"7\",\n          \"name\": \"Nguyen Tuan Dang\",\n          \"mobile\": \"092131242\",\n          \"email\": \"crazyfrogs004@mailnesia.com\",\n          \"address\": \"767A Nguyen ANh Thur\",\n          \"company_name\": null,\n          \"company_mobile\": null,\n          \"company_address\": null,\n          \"website\": null,\n          \"status\": \"1\",\n          \"deleted\": \"0\",\n          \"created_at\": \"2017-01-16 16:22:04\",\n          \"is_default\": \"0\"\n      }\n   ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/CardController.php",
    "groupTitle": "Card"
  },
  {
    "type": "post",
    "url": "/api/card-print/calculatetotal",
    "title": "Calculate Total",
    "name": "Calculate_order_total",
    "group": "CardPrint",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "quantity",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "paper_type",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "print_type",
            "description": "<p><code>[1: In thường, 2: In nhanh]</code></p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>OK</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Card info</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": \"OK\",\n    \"data\": {\n        \"total\": 600\n    }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/CardPrintController.php",
    "groupTitle": "CardPrint"
  },
  {
    "type": "get",
    "url": "/api/card-print/listpapertype",
    "title": "List paper type",
    "name": "List_paper_type_available",
    "group": "CardPrint",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>OK</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Card info</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": \"OK\",\n  \"data\": [\n    {\n      \"id\": 1,\n      \"label\": \"Giấy lụa\",\n      \"price\": 10\n    },\n    {\n      \"id\": 2,\n      \"label\": \"Giấy bóng\",\n      \"price\": 20\n    },\n    {\n      \"id\": 3,\n      \"label\": \"Giấy carton\",\n      \"price\": 30\n    }       \n  ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/CardPrintController.php",
    "groupTitle": "CardPrint"
  },
  {
    "type": "post",
    "url": "/api/card-print/order/:id",
    "title": "Print",
    "name": "Order_print",
    "group": "CardPrint",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "quantity",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "paper_type",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "print_type",
            "description": "<p><code>[1: In thường, 2: In nhanh]</code></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>OK</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Bạn đã thêm card thành công</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": \"OK\",\n  \"message\": \"Bạn đã order thành công\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/CardPrintController.php",
    "groupTitle": "CardPrint"
  },
  {
    "type": "put",
    "url": "/api/card/update/:id",
    "title": "Update",
    "name": "Update_card_by_id",
    "group": "Card",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "front_img",
            "description": "<p>base64 encode string</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "back_img",
            "description": "<p>base64 encode string</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name on card</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "company_name",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "company_mobile",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "company_address",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "website",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>OK</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Card info</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": \"OK\",\n    \"message\": \"Bạn đã thêm card thành công\",\n    \"data\": {\n        \"id\": \"10\",\n        \"user_id\": \"5\",\n        \"name\": \"Nguyen Vu Loc\",\n        \"mobile\": \"0908833061\",\n        \"email\": \"crazyfrogs2@mailnesia.com\",\n        \"address\": \"767A Nguyen Anh Thu\",\n        \"company_name\": null,\n        \"company_mobile\": null,\n        \"company_address\": null,\n        \"website\": null,\n        \"status\": \"1\",\n        \"deleted\": \"0\",\n        \"front_img\": \"/uploads/123456789_uf.jpg\",\n        \"back_img\": \"/uploads/123456789_ub.jpg\",\n        \"created_at\": \"2017-01-15 22:55:25\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/CardController.php",
    "groupTitle": "Card"
  },
  {
    "type": "Post",
    "url": "/api/card-report/add/:id",
    "title": "Add",
    "name": "Create_card_error_report",
    "group": "Card_report",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Content",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>OK</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Card info</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"message\": \"Gửi thành công\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/CardReportController.php",
    "groupTitle": "Card_report"
  },
  {
    "type": "get",
    "url": "/api/config/androidversion",
    "title": "Android Version",
    "name": "Android_Version",
    "group": "Config",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>OK</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Android app version</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": \"OK\",\n  \"data\": {\n    \"version\": \"1.0\",\n    \"message\": \"Bạn cần update lên phiên bản mới để tiếp tục\",\n    \"download\": \"https://play.google.com/store/apps/details?id=com.tfl.ob.onbusiness\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/ConfigController.php",
    "groupTitle": "Config"
  },
  {
    "type": "get",
    "url": "/api/config/designfee",
    "title": "Design fee",
    "name": "Get_card_design_fee",
    "group": "Config",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>OK</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": \"OK\",\n  \"data\": {\n    \"design_fee\": \"150\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/ConfigController.php",
    "groupTitle": "Config"
  },
  {
    "type": "post",
    "url": "/api/config/get",
    "title": "Config value",
    "name": "Get_config_value",
    "group": "Config",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "key",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>OK</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": \"OK\",\n  \"data\": {\n    \"value\": \"20\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/ConfigController.php",
    "groupTitle": "Config"
  },
  {
    "type": "get",
    "url": "/api/config/ios",
    "title": "iOS",
    "name": "Get_iOS_config",
    "group": "Config",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>OK</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": \"OK\",\n  \"data\": [\n    {\n      \"id\": \"1\",\n      \"key\": \"iOSPublishVersion\",\n      \"value\": \"1\"\n    },\n    {\n      \"id\": \"2\",\n      \"key\": \"iOSAppVersion\",\n      \"value\": \"1\"\n    }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/ConfigController.php",
    "groupTitle": "Config"
  },
  {
    "type": "get",
    "url": "/api/contact/fetch",
    "title": "Fetch",
    "name": "Fetch_all_contact_of_current_user",
    "group": "Contact",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>OK</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\":\"OK\",\n    \"data\":[\n        {\n            \"id\":\"29\",\n            \"mobile\":\"0976315664\",\n            \"name\":\"Nguyen Vu Loc\",\n            \"company\": \"Tech for life\" \n         }, \n         {\n             \"id\":\"30\",\n             \"mobile\":\"01647287765\",\n             \"name\":\"Tran Ngoc Phuc\",\n             \"company\": \"Dien may cho lon\"\n         }\n     ]\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/ContactController.php",
    "groupTitle": "Contact"
  },
  {
    "type": "get",
    "url": "/api/contact/detail/:id",
    "title": "Detail",
    "name": "Get_contact_detail_info",
    "group": "Contact",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>OK</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": \"OK\",\n  \"data\": {\n    \"id\": \"35\",\n    \"mobile\": \"0976315664\",\n    \"name\": \"Nguyen Vu Loc\",\n    \"company\": null,\n    \"front_img\": \"/img/default_card_back.png\",\n    \"back_img\": \"/img/default_card_front.png\",\n    \"cards\": [\n      {\n        \"id\": \"21\",\n        \"user_id\": \"7\",\n        \"name\": \"Nguyen Vu Loc\",\n        \"mobile\": \"0976315664\",\n        \"email\": \"ngynvuloc@gmail.com\",\n        \"address\": \"767A Nguyen Anh Thu, Q.12, Tp.HCM\",\n        \"company_name\": null,\n        \"company_mobile\": null,\n        \"website\": null,\n        \"status\": \"1\",\n        \"qr_code\": null,\n        \"deleted\": \"0\",\n        \"created_at\": \"2017-01-24 08:28:28\",\n        \"default\": \"1\",\n        \"company_address\": null\n      },\n      {\n        \"id\": \"24\",\n        \"user_id\": \"7\",\n        \"name\": \"Nguyen Vu Loc\",\n        \"mobile\": \"0976315664\",\n        \"email\": \"nvuloc@gmail.com\",\n        \"address\": \"767A Nguyen Anh Thu, Q.12, Tp.HCM\",\n        \"company_name\": \"Tech For Life\",\n        \"company_mobile\": null,\n        \"website\": null,\n        \"status\": \"1\",\n        \"qr_code\": null,\n        \"deleted\": \"0\",\n        \"created_at\": \"2017-01-24 14:06:16\",\n        \"default\": \"0\",\n        \"company_address\": null\n      }\n    ]\n  }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/ContactController.php",
    "groupTitle": "Contact"
  },
  {
    "type": "post",
    "url": "/api/contact/init",
    "title": "Init",
    "name": "Init_user_contact",
    "group": "Contact",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "mobile",
            "description": "<p>vd: <code>['0908833061', '0976315664', '01647287765']</code></p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>OK</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\":\"OK\",\n    \"data\":[\n        {\n            \"id\":\"29\",\n            \"mobile\":\"0976315664\",\n            \"name\":\"Nguyen Vu Loc\",\n            \"company\": \"Tech for life\"\n         }, \n         {\n             \"id\":\"30\",\n             \"mobile\":\"01647287765\",\n             \"name\":\"Tran Ngoc Phuc\",\n             \"company\": \"Dien may cho lon\"\n         }\n     ]\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/ContactController.php",
    "groupTitle": "Contact"
  },
  {
    "type": "put",
    "url": "/api/contact/scan/:id",
    "title": "Scan",
    "name": "Scan_card",
    "group": "Contact",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>OK</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "card_id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Contact info</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\":\"OK\",\n    \"card_id\": \"1\",\n    \"data\": {\n        \"id\": 12,\n        \"mobile\": \"0976315664\",\n        \"name\": \"Nguyen Vu Loc\",\n        \"company\": null,\n    }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/ContactController.php",
    "groupTitle": "Contact"
  },
  {
    "type": "post",
    "url": "/api/feedback",
    "title": "Send",
    "name": "create_new_Feedback",
    "group": "Feedback",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>OK</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/FeedbackController.php",
    "groupTitle": "Feedback"
  },
  {
    "type": "get",
    "url": "/api/log/get",
    "title": "Get",
    "name": "Get_user_action_log",
    "group": "Log",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>OK</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>List of log</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  \nHTTP/1.1 200 OK\n  {\n    \"status\": \"OK\",\n    \"data\": [\n      {\n        \"id\": \"2\",\n        \"user_id\": \"3\",\n        \"title\": \"This is a title\"\n        \"message\": \"Bạn sử dụng 500 để thiết kế card\",\n        \"type\": \"coin_charge\",\n        \"created_at\": \"2017-02-13 16:46:53\"\n      }.\n      {\n        \"id\": \"1\",\n        \"user_id\": \"3\",\n        \"title\": \"This is a title\",\n        \"message\": \"Bạn vừa nạp 10 xu\",\n        \"type\": \"coin_recharge\",\n        \"created_at\": \"2017-02-13 16:46:53\"\n      }\n    ]\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/LogController.php",
    "groupTitle": "Log"
  },
  {
    "type": "get",
    "url": "/api/notification/countunread",
    "title": "Count unread",
    "name": "Count_unread",
    "group": "Notification",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>OK</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": \"OK\",\n  \"data\": {\n      \"count\": 2\n  }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/NotificationController.php",
    "groupTitle": "Notification"
  },
  {
    "type": "get",
    "url": "/api/notification/get",
    "title": "Get",
    "name": "Get_Notification",
    "group": "Notification",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>OK</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>List of notification</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  \nHTTP/1.1 200 OK\n  {\n    \"status\": \"OK\",\n    \"data\": [\n      {\n        \"id\": \"1\",\n        \"user_id\": \"9\",\n        \"title\": \"Adminh đã duyệt card %name%\",\n        \"body\": \"Chi phí thiết kết là %amount% xu\",\n        \"params\": [\n            {\"key\": \"name\", \"value\": \"Nguyen Vu Loc\"} \n            {\"key\": \"amount\", \"value\": 10}\n        ]\n        \"click_action\": \"CARD_VIEW|68\",\n        \"created_at\": \"2017-02-09 15:14:18\",\n        \"has_read\": \"0\"\n      }\n    ]\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/NotificationController.php",
    "groupTitle": "Notification"
  },
  {
    "type": "put",
    "url": "/api/notification/markread/:id",
    "title": "Mark Read",
    "name": "Mark_notification_as_read",
    "group": "Notification",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>OK</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  \nHTTP/1.1 200 OK\n  {\n    \"status\": \"OK\"\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/NotificationController.php",
    "groupTitle": "Notification"
  },
  {
    "type": "post",
    "url": "/api/payment/inapppurchase",
    "title": "Apple In App Purchase",
    "name": "Make_payment_via_Apple_In_app_purchase",
    "group": "Payment",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "receipt_id",
            "description": "<p>Mã receipt trả về từ Apple sau khi giao dịch</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>OK</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "current_balance",
            "description": "<p>Số dư trong tài khoản</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": \"OK\",\n    \"current_balance\": \"2000\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/PaymentController.php",
    "groupTitle": "Payment"
  },
  {
    "type": "post",
    "url": "/api/payment/mobilecard",
    "title": "Pay with mobile card",
    "name": "Make_payment_via_Ngan_Luong_gatway",
    "group": "Payment",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "pin_card",
            "description": "<p>Mã pin trên thẻ cào điện (không bao gồm dấu &quot;-&quot;)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "card_serial",
            "description": "<p>Số Serial trên card</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type_card",
            "description": "<p>Loại card <code>VMS =&gt; MobiFone, VNP =&gt; VinaPhone, VIETTEL =&gt; Viettel, VCOIN =&gt; VTCCoin, GATE =&gt; FPTGate</code></p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>OK</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "current_balance",
            "description": "<p>Số dư trong tài khoản</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": \"OK\",\n    \"current_balance\": \"20000\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/PaymentController.php",
    "groupTitle": "Payment"
  },
  {
    "type": "get",
    "url": "/api/payment/log",
    "title": "Log",
    "name": "Payment_log",
    "group": "Payment",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>OK</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>Nhật kí giao dịch</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": \"OK\",\n    \"data\": [\n        {\n            \"gateway\": \"Ngan Luong\",\n            \"status\": \"Thành công\",\n            \"amount\": \"10000\",\n            \"error\": null\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/PaymentController.php",
    "groupTitle": "Payment"
  },
  {
    "type": "put",
    "url": "/api/user/setfirebasetoken",
    "title": "Set Firebase token",
    "name": "Change_Firebase_token",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "firebase_token",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>OK</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/UserController.php",
    "groupTitle": "User"
  },
  {
    "type": "put",
    "url": "/api/user/changepassword",
    "title": "Change Password",
    "name": "Change_password",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password_old",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password_new",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>OK</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/UserController.php",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/api/user/get/",
    "title": "Get",
    "name": "Get_user_profile",
    "group": "User",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>OK</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": \"OK\",\n    \"data\": {\n      \"id\": \"6\",\n      \"mobile\": \"0908833061\",\n      \"email\": \"crazyfrogs1@mailnesia.com\",\n      \"fullname\": \"Nguyen Vu Loc\",\n      \"dob\": \"07-09-2002\",\n      \"address\": \"2 Nguyen Anh Thu, Q.12\",\n      \"skype\": \"crazyfrogs1\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/UserController.php",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/api/user/login",
    "title": "Login",
    "name": "Login",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": "<p>Chỉ chấp nhận ký tự số 0-9</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>eyJ0eXAiOiJKV1QiLCJhbGzi...</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Bạn đã đăng nhập thành công</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": \"OK\",\n  \"token\": \"eyJ0eXAiOiJKV1QiLCJhbGzi\",\n  \"message\": \"Bạn đã đăng nhập thành công\",\n  \"user\": {\n      \"id\": \"6\",\n      \"mobile\": \"0908833061\",\n      \"email\": \"crazyfrogs1@mailnesia.com\",\n      \"fullname\": \"Nguyen Vu Loc\",\n      \"dob\": \"07-09-2002\",\n      \"address\": \"2 Nguyen Anh Thu, Q.12\",\n      \"skype\": \"crazyfrogs1\"\n   }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/UserController.php",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/api/user/logout",
    "title": "Logout",
    "name": "Logout",
    "group": "User",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>OK</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/UserController.php",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/api/user/register",
    "title": "Register",
    "name": "Register",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "fullname",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": "<p>Chỉ chấp nhận ký tự số 0-9</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Đúng định dạng mail abc@qwe.example</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": "<p>Code returned by Facebook</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>OK</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>eyJ0eXAiOiJKV1QiLCJhbGzi...</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Bạn đã đăng ký thành công</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": \"OK\",\n  \"token\": \"eyJ0eXAiOiJKV1QiLCJhbGzi\",\n  \"message\": \"Bạn đã đăng ký thành công\"       \n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/UserController.php",
    "groupTitle": "User"
  },
  {
    "type": "put",
    "url": "/api/user/update",
    "title": "Update",
    "name": "Update",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "fullname",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "skype",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "DateTime",
            "optional": false,
            "field": "dob",
            "description": "<p>DD-MM-YYYY</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>OK</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Bạn đã cập nhập thông tin thành công</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": \"OK\",\n    \"message\": \"Bạn đã cập nhập thông tin thành công\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/UserController.php",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/api/user/validate",
    "title": "Validate",
    "name": "Validate_user_info_before_register",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>OK</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": \"OK\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/controllers/api/UserController.php",
    "groupTitle": "User"
  }
] });
