-- 02/03/2017
ALTER TABLE `tfl_onbusiness`.`cards` CHANGE COLUMN `is_default` `is_default` TINYINT(2) NULL DEFAULT '0' AFTER `deleted`;
alter table `cards` add column `cfront_img` varchar(255) default null;
alter table `cards` add column `cback_img` varchar(255) default null;
ALTER TABLE `tfl_onbusiness`.`cards` ADD COLUMN `updated_at` TIMESTAMP NULL ON UPDATE CURRENT_TIMESTAMP AFTER `back_img`;

ALTER TABLE `tfl_onbusiness`.`cards` 
ADD INDEX `fk_cards_user_idx` (`user_id` ASC);
ALTER TABLE `tfl_onbusiness`.`cards` 
ADD CONSTRAINT `fk_cards_user`
  FOREIGN KEY (`user_id`)
  REFERENCES `tfl_onbusiness`.`users` (`id`)
  ON DELETE CASCADE
  ON UPDATE NO ACTION;

alter table coin_spending_logs add constraint `fk_coin_spending_logs_user` foreign key (`user_id`) references `users`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
alter table card_print_order add constraint `fk_order_print_card` foreign key (`card_id`) references `cards`(`id`) on delete CASCADE on update NO ACTION;


ALTER TABLE `tfl_onbusiness`.`notifications` 
DROP FOREIGN KEY `fk_notifications_user`;
ALTER TABLE `tfl_onbusiness`.`notifications` 
ADD CONSTRAINT `fk_notifications_user`
  FOREIGN KEY (`user_id`)
  REFERENCES `tfl_onbusiness`.`users` (`id`)
  ON DELETE CASCADE
  ON UPDATE NO ACTION;

ALTER TABLE `tfl_onbusiness`.`feedback` 
DROP FOREIGN KEY `fk_feedback_user`;
ALTER TABLE `tfl_onbusiness`.`feedback` 
ADD CONSTRAINT `fk_feedback_user`
  FOREIGN KEY (`user_id`)
  REFERENCES `tfl_onbusiness`.`users` (`id`)
  ON DELETE CASCADE
  ON UPDATE NO ACTION;


ALTER TABLE `tfl_onbusiness`.`payment_logs` 
ADD INDEX `fk_payment_logs_user_idx` (`user_id` ASC);
ALTER TABLE `tfl_onbusiness`.`payment_logs` 
ADD CONSTRAINT `fk_payment_logs_user`
  FOREIGN KEY (`user_id`)
  REFERENCES `tfl_onbusiness`.`users` (`id`)
  ON DELETE CASCADE
  ON UPDATE NO ACTION;
  
alter table cards modify column `deleted` tinyint(4) default 0;

