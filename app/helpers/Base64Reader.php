<?php
namespace app\helpers;

class Base64Reader {
	public $fileContent;
	public $mime_type;
	public $extension;

	public function __construct($encoded) {
		$this->fileContent = base64_decode($encoded);
		// Read file info
		$fi = finfo_open();
		$this->mime_type = finfo_buffer($fi, $this->fileContent, FILEINFO_MIME_TYPE);
		$this->extension = explode("/", $this->mime_type)[1];
	}

	public function getMimeType() {
		return $this->mime_type;
	}

	public function getExtension() {
		return $this->extension;
	}

	public function writeToFile ($path) {
		file_put_contents($path, $this->fileContent);
	}

	public function isImage() {
		return preg_match('/image\/(jpeg|jpg|gif|png|tiff)/i', $this->mime_type);
	}


}