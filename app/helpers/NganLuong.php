<?php
namespace app\helpers;

class NganLuong {
	private $chargeLink = 'https://www.nganluong.vn/mobile_card.api.post.v2.php';

	private $merchant_id       = '48799';
	private $merchant_password = '951b152946d94c9f53c3ec73cb554841';
	private $merchant_account  = 'chutieu7@gmail.com';
	private $apiVersion = '2.0';

	private $proceed_code;

	public $pin_card;
	public $type_card;
	public $card_serial;

	public $client_fullname;
	public $client_email;
	public $client_mobile;

	/** Attributes after proceed */
	public $error = false;
	public $card_amount = 0;
	public $transaction_amount;
	public $transaction_id;


	/** Card type */
	const card_MobiFone  = 'VMS';  
	const card_VinaPhone = 'VNP';  
	const card_Viettel   = 'VIETTEL';  
	const card_VTCCoin   = 'VCOIN';  
	const card_FPTGate   = 'GATE';  

	public function __construct($clientInfo, $setting = null) {
		// Init properties
		foreach ($clientInfo as $key => $value) {
			$this->$key = $value;
		}

		if (!is_null($setting)) {
			foreach ($setting as $key => $value) {
				$this->$key = $value;
			}
		}
	}

	public function cardCharge($cardInfo) {
		$data = [
			'func'              => 'CardCharge',
			'version'           => $this->apiVersion,
			'merchant_id'       => $this->merchant_id,
			'merchant_account'  => $this->merchant_account,
			'merchant_password' => md5(implode('|', [$this->merchant_id, $this->merchant_password])),
			'pin_card'          => $cardInfo['pin_card'],
			'card_serial'       => $cardInfo['card_serial'],
			'type_card'         => $cardInfo['type_card'],
			'client_fullname'   => $this->client_fullname,
			'client_email'      => $this->client_email,
			'client_mobile'     => $this->client_mobile
		];

		$ch = curl_init($this->chargeLink);
		curl_setopt_array($ch, [
			CURLOPT_POST           => true,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POSTFIELDS     => $data
		]);
		
		$result = curl_exec($ch);
		curl_close($ch);

		$chargeInfo = array_combine([
			'error_code',
			'merchant_id',
			'merchant_account',
			'pin_card',
			'card_serial',
			'type_card',
			'ref_code',
			'client_fullname',
			'client_email',
			'client_mobile',
			'card_amount',
			'transaction_amount',
			'transaction_id',
		], explode('|', $result));
		
		$this->proceed_code = $chargeInfo['error_code'];

		if ($chargeInfo['error_code'] == '00') {
			$transactionParams = ['card_amount', 'transaction_id', 'transaction_amount', 'pin_card', 'card_serial', 'type_card'];
			foreach ($transactionParams as $param) {
				$this->$param = $chargeInfo[$param];
			}
			return true;			
		} else {
			return false;
		}
	}

	public function getMessage() {
		$errorTranslate = [
			'00' => 'Giao dịch thành công',
			'99' => 'Lỗi, tuy nhiên lỗi chưa được định nghĩa hoặc chưa xác định được nguyên nhân',
			'01' => 'Lỗi, địa chỉ IP truy cập API của NgânLượng.vn bị từ chối',
			'02' => 'Lỗi, tham số gửi từ merchant tới NgânLượng.vn chưa chính xác (thường sai tên tham số hoặc thiếu tham số)',
			'03' => 'Lỗi, Mã merchant không tồn tại hoặc merchant đang bị khóa kết nối tới NgânLượng.vn',
			'04' => 'Lỗi, Mã checksum không chính xác (lỗi này thường xảy ra khi mật khẩu giao tiếp giữa merchant và NgânLượng.vn không chính xác, hoặc cách sắp xếp các tham số trong biến params không đúng)',
			'05' => 'Tài khoản nhận tiền nạp của merchant không tồn tại',
			'06' => 'Tài khoản nhận tiền nạp của merchant đang bị khóa hoặc bị phong tỏa, không thể thực giao dịch nạp tiền',
			'07' => 'Thẻ đã được sử dụng',
			'08' => 'Thẻ bị khóa',
			'09' => 'Thẻ hết hạn sử dụng',
			'10' => 'Thẻ chưa được kích hoạt hoặc không tồn tại',
			'11' => 'Mã thẻ sai định dạng',
			'12' => 'Sai số serial của thẻ',
			'13' => 'Thẻ chưa được kích hoạt hoặc không tồn tại',
			'14' => 'Thẻ không tồn tại',
			'15' => 'Thẻ không sử dụng được',
			'16' => 'Số lần thử (nhập sai liên tiếp) của thẻ vượt quá giới hạn cho phép',
			'17' => 'Hệ thống Telco bị lỗi hoặc quá tải, thẻ chưa bị trừ',
			'18' => 'Hệ thống Telco bị lỗi hoặc quá tải, thẻ có thể bị trừ, cần phối hợp với NgânLượng.vn để tra soát',
			'19' => 'Kết nối từ NgânLượng.vn tới hệ thống Telco bị lỗi, thẻ chưa bị trừ (thường do lỗi kết nối giữa NgânLượng.vn với Telco, ví dụ sai tham số kết nối, mà không liên quan đến merchant)',
			'20' => 'Kết nối tới telco thành công, thẻ bị trừ nhưng chưa cộng tiền trên NgânLượng.vn',
		];
		return $errorTranslate[$this->proceed_code];
	}

	public function getCardType() {
		$translate = [
			'VMS'     =>  'MobiFone',
			'VNP'     =>  'VinaPhone',
			'VIETTEL' =>  'Viettel',
			'VCOIN'   =>  'VTC Coin',
			'GATE'    =>  'FPT Gate',
		];
		return $translate[$this->type_card];
	}

	public function cardChargeSummary() {
		$summaryAttr = ['card_amount', 'transaction_id', 'transaction_amount', 'pin_card', 'card_serial', 'type_card'];
		return json_encode([
			'card_amount'        => $this->card_amount,
			'transaction_id'     => $this->transaction_id,
			'transaction_amount' => $this->transaction_amount,
			'pin_card'           => $this->pin_card,
			'card_serial'        => $this->card_serial,
			'type_card'          => $this->type_card
		]);
	}
}