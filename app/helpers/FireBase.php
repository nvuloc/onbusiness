<?php
namespace app\helpers;

class FireBase {

	// Cloud messasing config
	private $server_key        = null;
	private $legacy_server_key = null;
	private $sender_id         = null;

	private $sendNotificationAPI = 'https://fcm.googleapis.com/fcm/send';

	public function __construct($config) {
		$this->server_key        = $config['server_key'] ?? null;
		$this->sender_id         = $config['sender_id'] ?? null;
		$this->legacy_server_key = $config['legacy_server_key'] ?? null;
	}

	public function sendNotification($firebase_token, $data) {
		if (empty($firebase_token)) return true;
		$ch = curl_init($this->sendNotificationAPI);
		
		$data = [
			'to'           => $firebase_token,
			'notification' => $data,
		];

		curl_setopt_array($ch, [
			CURLOPT_POST           => true,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POSTFIELDS     => json_encode($data),
			CURLOPT_HTTPHEADER => [
				"Authorization: key={$this->legacy_server_key}",
				'Content-Type: application/json',
			]
		]);

		$result = curl_exec($ch);
		curl_close($ch);

		return $result;
	}

	
}