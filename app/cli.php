<?php

use Phalcon\Di\FactoryDefault\Cli as CliDI;
use Phalcon\Cli\Console as ConsoleApp;
use Phalcon\Loader;
use Phalcon\Queue\Beanstalk;
use Phalcon\Translate\Adapter\NativeArray;

include __DIR__.'/helpers/FireBase.php';

error_reporting(E_ALL);

define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/app');

// Using the CLI factory default services container
$di = new CliDI();

/**
 * Register the autoloader and tell it to register the tasks directory
 */
$loader = new Loader();



// Load the configuration file (if any)

$configFile = __DIR__ . "/config/config.php";

if (is_readable($configFile)) {
    $config = include $configFile;

    $di->set("config", $config);

    # Database
    $di->setShared('db', function () {
		$config = $this->getConfig();

		$class = 'Phalcon\Db\Adapter\Pdo\\' . $config->database->adapter;
		$params = [
			'host'     => $config->database->host,
			'username' => $config->database->username,
			'password' => $config->database->password,
			'dbname'   => $config->database->dbname,
			'charset'  => $config->database->charset
		];

		if ($config->database->adapter == 'Postgresql') {
			unset($params['charset']);
		}

		$connection = new $class($params);

		return $connection;
	});

    # Queue
    $di->setShared('queue', function() {
		$queue = new Beanstalk([
	        "host" => "localhost",
	        "port" => "11300",
	    ]);

	    return $queue;
	});

    # Firebase
	$di->setShared('firebase', function() {
		$firebase = new app\helpers\FireBase([
			'legacy_server_key' => 'AIzaSyBTLokL5_68zJnXikUgtSDmqJHXF85JIjQ',
			'sender_id'         => '953322816364',
			'server_key'        => 'AAAA3fZ3w2w:APA91bHdFKSPsER_v578OdgG11bZyU9Jk-uKcSdNAFZJUxQa_Adym5Cbm5UAAcoL6GCrh4Z2J0dyHHVAHrOkolXCZnA3d3XYfqWbTNspFoILZEWvJvULy-h_arofGqQpLO5EFI0Mu62A',
		]);
		return $firebase;
	});

	# Translate
	$di->setShared('translate', function() {
		$translate = new NativeArray([
			'content' => include APP_PATH.'/config/messages.php'
		]);
		return $translate;
	});
}

$loader->registerDirs(
    [
        __DIR__ . "/tasks",
        $config->application->modelsDir
    ]
);
$loader->registerNamespaces([
    # Models
    'app\models' => $config->application->modelsDir,

	# External resource
    'Facebook'      => BASE_PATH.'/vendor/facebook/graph-sdk/src/Facebook',
    'Lcobucci\JWT'  => BASE_PATH.'/vendor/lcobucci/jwt/src',
    'app\helpers'   => APP_PATH.'/helpers',
]);
$loader->register();

// Create a console application
$console = new ConsoleApp();
$console->setDI($di);

/**
 * Process the console arguments
 */
$arguments = [];

foreach ($argv as $k => $arg) {
    if ($k === 1) {
        $arguments["task"] = $arg;
    } elseif ($k === 2) {
        $arguments["action"] = $arg;
    } elseif ($k >= 3) {
        $arguments["params"][] = $arg;
    }
}



try {
    // Handle incoming arguments
    $console->handle($arguments);
} catch (\Phalcon\Exception $e) {
    echo $e->getMessage();

    exit(255);
}