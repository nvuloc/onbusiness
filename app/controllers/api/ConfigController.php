<?php
namespace app\controllers\api;

use app\models\AppConfig;

class ConfigController extends ControllerBase
{
    public function initialize() {
        $this->view->disable();
        $action = $this->dispatcher->getActionName();

        # Validate authorization for specific action
        $authActions = [];
        parent::authenticate($action, $authActions);

        # Validate HTTP method
        $rules = [
            'isGet' => ['ios', 'androidversion'],
            'isPost' => [],
            'isPut' => [],
            'isDelete' => []
        ];
        parent::checkMethod($action, $rules);

        parent::initialize();
    }

    /**
    * @api {get} /api/config/ios iOS
    * @apiName Get iOS config
    * @apiGroup Config
    *
    * @apiSuccess {String} status OK
    * @apiSuccess {Array} data 
    * @apiSuccessExample {json} Success-Response:
    *     HTTP/1.1 200 OK
    *     {
    *       "status": "OK",
    *       "data": [
    *         {
    *           "id": "1",
    *           "key": "iOSPublishVersion",
    *           "value": "1"
    *         },
    *         {
    *           "id": "2",
    *           "key": "iOSAppVersion",
    *           "value": "1"
    *         }
    *       ]
    *     }
    */
    public function iosAction() {
        $params = ['iOSPublishVersion', 'iOSAppVersion'];
        $config = AppConfig::find(["key in ('iOSPublishVersion', 'iOSAppVersion')"])->toArray();

        $this->response->setJsonContent([
            'status' => 'OK',
            'data'   => $config
        ]);
        return $this->response;
    }

    /**
    * @api {get} /api/config/androidversion Android Version
    * @apiName Android Version
    * @apiGroup Config
    *
    * @apiSuccess {String} status OK
    * @apiSuccess {Object} data Android app version
    * @apiSuccessExample {json} Success-Response:
    *     HTTP/1.1 200 OK
    *     {
    *       "status": "OK",
    *       "data": {
    *         "version": "1.0",
    *         "message": "Bạn cần update lên phiên bản mới để tiếp tục",
    *         "download": "https://play.google.com/store/apps/details?id=com.tfl.ob.onbusiness"
    *       }
    *     }
    */
    public function androidversionAction() {
        $params = ['AdVersion'];

        $version = AppConfig::findFirst(["key = 'AdVersion'"]);
        $download = AppConfig::findFirst(["key = 'AdNewVersionLink'"]);

        $this->response->setJsonContent([
            'status' => 'OK',
            'data' => [
                'version' => $version->value,
                'message' => 'Bạn cần update lên phiên bản mới để tiếp tục',
                'download' => $download->value
            ]
        ]);
        return $this->response;
    }

    /**
    * @api {get} /api/config/designfee Design fee
    * @apiName Get card design fee
    * @apiGroup Config
    *
    * @apiSuccess {String} status OK
    * @apiSuccess {Object} data 
    * @apiSuccessExample {json} Success-Response:
    *     HTTP/1.1 200 OK
    *     {
    *       "status": "OK",
    *       "data": {
    *         "design_fee": "150"
    *       }
    *     }
    */
    public function designfeeAction() {
        $config = AppConfig::findFirst(["key = 'ObCardDesignFee'"]);
        $this->response->setJsonContent([
            'status' => 'OK',
            'data' => [
                'design_fee' => $config->value
            ]
        ]);
        return $this->response;
    }

    /**
    * @api {post} /api/config/get Config value
    * @apiName Get config value
    * @apiGroup Config
    *
    * @apiParam {String} key
    *
    * @apiSuccess {String} status OK
    * @apiSuccess {Object} data 
    * @apiSuccessExample {json} Success-Response:
    *     HTTP/1.1 200 OK
    *     {
    *       "status": "OK",
    *       "data": {
    *         "value": "20"
    *       }
    *     }
    */
    public function getAction() {
        $requestParams = $this->request->getJsonRawBody();

        $this->response->setJsonContent([
            'status' => 'OK',
            'data' => [
                'value' => $this->obConfig[$requestParams->key],                
            ]
        ]);
        return $this->response;
    }
}

