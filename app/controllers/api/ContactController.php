<?php
namespace app\controllers\api;

use Phalcon\Mvc\Model\Query;

use app\helpers\Base64Reader;

use app\models\{Cards, Users, CardShareMaps, Contact, Notifications};


class ContactController extends ControllerBase
{

	public function initialize() {
		$this->view->disable();
		$action = $this->dispatcher->getActionName();

		# Validate authorization for specific action
		$authActions = ['init', 'fetch', 'detail', 'scan'];
		parent::authenticate($action, $authActions);

		# Validate HTTP method
		$rules = [
			'isGet' => ['fetch', 'detail'],
			'isPost' => ['init'],
			'isPut' => ['scan'],
			'isDelete' => []
		];
		parent::checkMethod($action, $rules);
		parent::initialize();
	}

	/**
	* @api {post} /api/contact/init Init
	* @apiName Init user contact
	* @apiGroup Contact
	*
	* @apiParam {Array} mobile vd: `['0908833061', '0976315664', '01647287765']`
	*
	* @apiSuccess {String} status OK
	* @apiSuccess {Array} data
	* @apiSuccessExample Success-Response:
	*     HTTP/1.1 200 OK
	*     {
	*         "status":"OK",
	*         "data":[
	*             {
	*                 "id":"29",
	*                 "mobile":"0976315664",
	*                 "name":"Nguyen Vu Loc",
	*                 "company": "Tech for life"
	*              },
	*              {
	*                  "id":"30",
	*                  "mobile":"01647287765",
	*                  "name":"Tran Ngoc Phuc",
	*                  "company": "Dien may cho lon"
	*              }
	*          ]
	*      }
	*/
	public function initAction() {
		$input = $this->request->getJsonRawBody();

		if (empty ($input->mobile) || !is_array($input->mobile) ) {
			$this->response->setStatusCode(403, 'Not Accepted');
			$this->response->setJsonContent([
				'message' => 'Trường dữ liệu không hợp lệ',
			]);
			return $this->response;
		}

		// Sync each phone mobile
		$rawQueryResult = Contact::query()->columns(["mobile"])->where("user_id = :userId:")->bind(['userId' => $this->userId])->execute()->toArray();
		$currentContactMobiles = empty($rawQueryResult) ? [] : array_column($rawQueryResult, 'mobile');

		foreach($input->mobile as $phoneNumber) {
			$phoneNumber = preg_replace("/^(\+84|84)/", "0", $phoneNumber);
			# Filter invalide phone number
			if (!is_numeric($phoneNumber) || strlen($phoneNumber) < 10) continue;
			try {
				# Do nothing if this mobile existed in user contact
				if (!empty($currentContactMobiles) && in_array($phoneNumber, $currentContactMobiles) ) continue;

				# Find cards same phone number
				$firstCardMatch = Cards::findFirst(["mobile = {$phoneNumber}"]);

				# Continue when no card existed or user can't import himseft to contact
				if ( empty($firstCardMatch) || $firstCardMatch->user_id == $this->userId ) continue;

				# Create new contact
				$contact = new Contact;
				$contactAttributes = [
					'user_id'        => $this->userId,
					'mobile'         => $phoneNumber,
					'user_target_id' => $firstCardMatch->user_id,
					'card_id'        => $firstCardMatch->id,
				];
				if ( !$contact->save($contactAttributes) ) {
					throw new \Exception(print_r($contact->getMessages(), true));
				}
			} catch (\Exception $e) {
				echo $e->getMessage();
				return $this->response;
			}
		}

		$contact = Contact::find(['user_id = '.$this->userId]);
		$contactList = [];
		foreach ($contact as $ct) {
			$contactList[] = $ct->info;
		}

		$this->response->setJsonContent([
			'status' => 'OK',
			'data'   => $contactList
		]);
		return $this->response;
	}

	/**
	* @api {get} /api/contact/fetch Fetch
	* @apiName Fetch all contact of current user
	* @apiGroup Contact
	*
	* @apiSuccess {String} status OK
	* @apiSuccess {Array} data
	* @apiSuccessExample Success-Response:
	*     HTTP/1.1 200 OK
	*     {
	*         "status":"OK",
	*         "data":[
	*             {
	*                 "id":"29",
	*                 "mobile":"0976315664",
	*                 "name":"Nguyen Vu Loc",
	*                 "company": "Tech for life"
	*              },
	*              {
	*                  "id":"30",
	*                  "mobile":"01647287765",
	*                  "name":"Tran Ngoc Phuc",
	*                  "company": "Dien may cho lon"
	*              }
	*          ]
	*      }
	*/
	public function fetchAction() {
		$contact = Contact::find(['user_id = '. $this->userId]);
		$contactList = [];
		foreach ($contact as $ct) {
			$contactList[] = $ct->info;
		}

		$this->response->setJsonContent([
			'status' => 'OK',
			'data'   =>  $contactList
		]);
		return $this->response;
	}


	/**
	* @api {get} /api/contact/detail/:id Detail
	* @apiName Get contact detail info
	* @apiGroup Contact
	*
	* @apiSuccess {String} status OK
	* @apiSuccess {Array} data
	* @apiSuccessExample Success-Response:
	*     HTTP/1.1 200 OK
	*     {
	*       "status": "OK",
	*       "data": {
	*         "id": "35",
	*         "mobile": "0976315664",
	*         "name": "Nguyen Vu Loc",
	*         "company": null,
	*         "front_img": "/img/default_card_back.png",
    *         "back_img": "/img/default_card_front.png",
	*         "cards": [
	*           {
	*             "id": "21",
	*             "user_id": "7",
	*             "name": "Nguyen Vu Loc",
	*             "mobile": "0976315664",
	*             "email": "ngynvuloc@gmail.com",
	*             "address": "767A Nguyen Anh Thu, Q.12, Tp.HCM",
	*             "company_name": null,
	*             "company_mobile": null,
	*             "website": null,
	*             "status": "1",
	*             "qr_code": null,
	*             "deleted": "0",
	*             "created_at": "2017-01-24 08:28:28",
	*             "default": "1",
	*             "company_address": null
	*           },
	*           {
	*             "id": "24",
	*             "user_id": "7",
	*             "name": "Nguyen Vu Loc",
	*             "mobile": "0976315664",
	*             "email": "nvuloc@gmail.com",
	*             "address": "767A Nguyen Anh Thu, Q.12, Tp.HCM",
	*             "company_name": "Tech For Life",
	*             "company_mobile": null,
	*             "website": null,
	*             "status": "1",
	*             "qr_code": null,
	*             "deleted": "0",
	*             "created_at": "2017-01-24 14:06:16",
	*             "default": "0",
	*             "company_address": null
	*           }
	*         ]
	*       }
	*     }
	*/
	public function detailAction($id) {
		$contact = Contact::findFirst(['id = '. $id]);
		if ( empty($contact) || $contact->user_id != $this->userId ) {
			$this->response->setStatusCode(404, 'Not Found');
			$this->response->setJsonContent([
				'message' => 'Không tìm thấy contact'
			]);
		} else {
			$this->response->setJsonContent([
				'status' => 'OK',
				'data'   => $contact->getFullInfo()
			]);
		}
		return $this->response;
	}


	/**
	* @api {put} /api/contact/scan/:id Scan
	* @apiName Scan card
	* @apiGroup Contact
	*
	* @apiSuccess {String} status OK
	* @apiSuccess {Integer} card_id
	* @apiSuccess {Object} data Contact info
	* @apiSuccessExample Success-Response:
	*     HTTP/1.1 200 OK
	*     {
	*         "status":"OK",
	*         "card_id": "1",
	*         "data": {
	*             "id": 12,
	*             "mobile": "0976315664",
	*             "name": "Nguyen Vu Loc",
	*             "company": null,
	*         }
	*     }
	*/
	public function scanAction($id) {
		$card = Cards::findFirst($id);
		if (empty($card)) {
			return $this->requestFail(404, ['message' => $this->_t('api_card_not_found')]);
		}

		// Check contact exist for scanner
		$scannerContact = Contact::findFirst([
			"user_id = ?1 AND card_id = ?2",
			'bind' => [1 => $this->userId, 2 => $card->id]
		]);
		if (!empty($scannerContact)) {
			$this->response->setJsonContent([
				'status'  => 'OK',
				'card_id' => $card->id,
				'data'    => $scannerContact->info
			]);
			return $this->response;
		}

		// Check contact exist for card owner
		$scannerDefaultCard = Cards::findFirst(["user_id = {$this->userId} AND is_default = 1"]);
		$cardOwnerContact = Contact::findFirst([
			"user_id = ?1 AND card_id = ?2",
			'bind' => [
				1 => $card->user_id,
				2 => $scannerDefaultCard->id
			]
		]);
		if (!empty($cardOwnerContact)) {
			$scannerContact = new Contact;
			$scannerContact->save([
				'user_id'        => $this->userId,
				'user_target_id' => $card->user_id,
				'card_id'        => $card->id,
				'mobile'         => $card->mobile
			]);

			$this->response->setJsonContent([
				'status'  => 'OK',
				'card_id' => $card->id,
				'data'    => $scannerContact->info
			]);
			return $this->response;
		}

		// Add card owner to scanner contact
		$scannerContact = new Contact;
		$scannerContact->save([
			'user_id'        => $this->userId,
			'user_target_id' => $card->user_id,
			'card_id'        => $card->id,
			'mobile'         => $card->mobile,
		]);

		// Add scanner to scanner contact
		$cardOwnerContact = new Contact;
		$cardOwnerContact->save([
			'user_id'        => $card->user_id,
			'user_target_id' => $this->userId,
			'card_id'        => $scannerDefaultCard->id,
			'mobile'         => $scannerDefaultCard->mobile
		]);

		// Notifications
		$scanner   = Users::findFirst($this->userId);
		$cardowner = Users::findFirst($card->user_id);
		//-- card owner
		$notifyCardOwner = new Notifications;
		$notifyCardOwner->save([
			'user_id' => $card->user_id,
			'click_action' => 'CONTACT_VIEW|'.$cardOwnerContact->id,
			'title'        => $this->_t('notification_contact_scan_success_title'),
			'body'         => $this->_t('notification_contact_scan_success_body'),
			'params' => [
				['key' => 'name', 'value' => $scanner->fullname]
			],
		]);
		//-- scanner
		$notifyScanner = new Notifications;
		$notifyScanner->save([
			'user_id' => $this->userId,
			'click_action' => 'CONTACT_VIEW|'.$scannerContact->id,
			'title'        => $this->_t('notification_contact_scan_success_title'),
			'body'         => $this->_t('notification_contact_scan_success_body'),
			'params' => [
				['key' => 'name', 'value' => $cardowner->fullname]
			],
		]);

		// Response to scanner
		$this->response->setJsonContent([
			'status'  => 'OK',
			'card_id' => $card->id,
			'data'	  => $scannerContact->info,
		]);
		return $this->response;
	}
}
