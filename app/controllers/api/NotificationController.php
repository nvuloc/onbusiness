<?php
namespace app\controllers\api;

use app\models\{Notifications, Users};
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

class NotificationController extends ControllerBase
{
	public function initialize() {
		$this->view->disable();
		$action = $this->dispatcher->getActionName();

		# Validate authorization for specific action
		$authActions = ['get', 'markread', 'countunread'];
		parent::authenticate($action, $authActions);

		# Validate HTTP method
		$rules = [
            'isGet' => ['get', 'countunread'],
			'isPost' => [],
			'isPut' => ['markread'],
            'isDelete' => []
		];
		parent::checkMethod($action, $rules);

        parent::initialize();
    }


    /**
    * @api {get} /api/notification/get Get
    * @apiName Get Notification
    * @apiGroup Notification
    *
    * @apiSuccess {String} status OK
    * @apiSuccess {Object} data List of notification
    * @apiSuccessExample {json} Success-Response:
    *     
    *   HTTP/1.1 200 OK
    *     {
	*       "status": "OK",
	*       "data": [
	*         {
	*           "id": "1",
	*           "user_id": "9",
	*           "title": "Adminh đã duyệt card %name%",
	*           "body": "Chi phí thiết kết là %amount% xu",
	*           "params": [
    *               {"key": "name", "value": "Nguyen Vu Loc"} 
    *               {"key": "amount", "value": 10}
    *           ]
	*           "click_action": "CARD_VIEW|68",
	*           "created_at": "2017-02-09 15:14:18",
	*           "has_read": "0"
	*         }
	*       ]
	*     }
    */
    public function getAction() {
    	$notifications = Notifications::find([
    		"user_id = {$this->userId}",
    		'order' => 'created_at desc',
    	]);

    	$paginator = new PaginatorModel([
			"data"  => $notifications,
			"limit" => 15,
			"page"  => (int) $this->request->getQuery('page', null),
		]);

		$this->response->setJsonContent([
			'status' => 'OK',
			'data'   => $paginator->getPaginate()->items 
		]);
		return $this->response;
    }

    /**
    * @api {put} /api/notification/markread/:id Mark Read
    * @apiName Mark notification as read
    * @apiGroup Notification
    *
    * @apiSuccess {String} status OK
    * @apiSuccessExample {json} Success-Response:
    *     
    *   HTTP/1.1 200 OK
    *     {
	*       "status": "OK"
	*     }
    */
    public function markreadAction($id) {
    	$notification = Notifications::findFirst($id);
        $tmp = $notification->params;
        $notification->params = $tmp;
    	$notification->has_read = 1;
    	$notification->save();
    	$this->response->setJsonContent(['status' => 'OK']);
    	return $this->response;
    }

    /**
     * @api {get} /api/notification/countunread Count unread
     * @apiName Count unread
     * @apiGroup Notification
     *
     * @apiSuccess {String} status OK
     * @apiSuccess {Integer} data 
     * @apiSuccessExample {json} Success-Response:
     *    HTTP/1.1 200 OK
     *    {
     *      "status": "OK",
     *      "data": {
     *          "count": 2
     *      }
     *    }
     */
    public function countunreadAction() {
        $count = Notifications::find([
            "user_id = {$this->userId} AND has_read = 0"
        ])->count();
        $this->response->setJsonContent([
            'status' => 'OK',
            'data' => [
                'count' => $count
            ],
        ]);
        return $this->response;
    }
}