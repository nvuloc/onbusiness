<?php
namespace app\controllers\api;

use app\models\{Cards, Users, CoinSpendingLogs, CardDesignDeal};

class CoinspendingController extends \Phalcon\Mvc\Controller
{

	public function initialize() {
		$this->view->disable();
		$action = $this->dispatcher->getActionName();

		# Validate authorization for specific action
		$authActions = ['acceptcarddesign'];
		parent::authenticate($action, $authActions);

		# Validate HTTP method
		$rules = [
			'isGet'    => [],
			'isPost'   => ['acceptcarddesign'],
			'isPut'    => [],
			'isDelete' => []
		];
		parent::checkMethod($action, $rules);
    }

    public function acceptcarddesignAction($deal_id) {
    	$user = Users::findFirst($this->userId);
    	if (empty($user)) {
    		return $this->requestFail(404, ['message' => 'Không tìm thấy người dùng tương ứng']);
    	}

    	$deal = CardDesignDeal::findFirst($deal_id);
    	if (empty($deal)) {
    		return $this->requestFail(400, ['message' => 'Action không hợp lệ']);
    	}

    	$log = new CoinSpendingLogs;
    	$log->save([
    		'user_id' => $this->userId,

    	]);
    }

}

