<?php
namespace app\controllers\api;

use app\models\{CardPrintOrder, Cards, CoinSpendingLogs, CardPrintMaterial};

class CardPrintController extends ControllerBase
{
    public function initialize() {
        $this->view->disable();
        $action = $this->dispatcher->getActionName();

        # Validate authorization for specific action
        $authActions = ['order'];
        parent::authenticate($action, $authActions);

        # Validate HTTP method
        $rules = [
            'isGet' => ['listpapertype'],
            'isPost' => ['order', 'calculatetotal'],
            'isPut' => [],
            'isDelete' => []
        ];
        parent::checkMethod($action, $rules);

        parent::initialize();
    }


    /**
    * @api {post} /api/card-print/order/:id Print
    * @apiName Order print
    * @apiGroup CardPrint
    *
    * @apiParam {Integer} quantity
    * @apiParam {Integer} paper_type
    * @apiParam {Integer} print_type `[1: In thường, 2: In nhanh]`
    * @apiParam {String} address
    * @apiParam {String} mobile
    *
    * @apiSuccess {String} status OK
    * @apiSuccess {String} message Bạn đã thêm card thành công
    * @apiSuccessExample {json} Success-Response:
    *   HTTP/1.1 200 OK
    *   {
    *     "status": "OK",
    *     "message": "Bạn đã order thành công"
    *   }
    */
    public function orderAction($id) {
        $card = Cards::findFirst($id);
        $user = $card->user;

        if (empty($card)) {
            return $this->requestFail(404, ['message' => $this->_t('api_card_not_found')]);
        }
        if ($card->user_id != $this->userId) {
            return $this->requestFail(403, ['message' => $this->_t('api_card_unauthorize') ]);
        }

        $input = $this->request->getJsonRawBody();
        

        $order = new CardPrintOrder;
        $attributes = ['quantity', 'paper_type', 'print_type', 'address', 'mobile'];
        foreach ($attributes as $attr) {
            $order->$attr = $input->$attr ?? null;
        }
        $order->card_id = $card->id;
        $order->calculateTotal();
        $order->status = CardPrintOrder::STATUS_NEW;

        if ($order->total > $user->balance) {
            return $this->requestFail(401, ['message' => $this->_t('api_card_balance_not_available_for_print_card')]);
        }

        if ($order->save()) {
            try {
                // decrease user balance due to print fee
                $user->balance -= $order->total;
                if (!$user->save()) throw new \Exception('Error');

                // Coin spending log
                $log = new CoinSpendingLogs;
                $saveLog = $log->save([
                    'user_id'     => $this->userId,
                    'action_type' => CoinSpendingLogs::ACTIONTYPE_CARDPRINT,
                    'action_id'   => $order->id,
                    'amount'      => $order->total,
                ]);
                if (!$saveLog) throw new \Exception('Error');
            } catch (\Exception $e) {
                print_r($log->getMessages()); die;
            }            

            $this->response->setJsonContent([
                'status' => 'OK',
                'message' => $this->_t('api_card_print_order_success'),
            ]);
            return $this->response;
        } else {
            $errors = $order->getMessages();
            return $this->requestFail(406, ['message' => $errors[0]->getMessage()]);
        }
    }

    /**
    * @api {get} /api/card-print/listpapertype List paper type 
    * @apiName List paper type available
    * @apiGroup CardPrint
    *
    * @apiSuccess {String} status OK
    * @apiSuccess {Object} data Card info
    * @apiSuccessExample {json} Success-Response:
    *     HTTP/1.1 200 OK
    *     {
    *       "status": "OK",
    *       "data": [
    *         {
    *           "id": 1,
    *           "label": "Giấy lụa",
    *           "price": 10
    *         },
    *         {
    *           "id": 2,
    *           "label": "Giấy bóng",
    *           "price": 20
    *         },
    *         {
    *           "id": 3,
    *           "label": "Giấy carton",
    *           "price": 30
    *         }       
    *       ]
    *     }
    */
    public function listpapertypeAction() {
        $cardPrintMaterial = CardPrintMaterial::find(["active = 1"])->toArray();
        foreach ($cardPrintMaterial as &$material) {
            $material['label'] = $material['name'];
        }
        $this->response->setJsonContent([
            'status' => 'OK',
            'data' => $cardPrintMaterial
        ]);
        return $this->response;
        /*$cardsPrintMaterial = unserialize($this->obConfig['onbusiness.ObPrintPaperType']);
        var_dump($cardsPrintMaterial); die;
        $this->response->setJsonContent([
            'status' => 'OK',
            'data' => array_values(CardPrintOrder::paperType())
        ]);
        return $this->response;*/
    }

    /**
    * @api {post} /api/card-print/calculatetotal Calculate Total
    * @apiName Calculate order total
    * @apiGroup CardPrint
    * @apiParam {Integer} quantity
    * @apiParam {Integer} paper_type
    * @apiParam {Integer} print_type `[1: In thường, 2: In nhanh]`
    * 
    *
    * @apiSuccess {String} status OK
    * @apiSuccess {Object} data Card info
    * @apiSuccessExample {json} Success-Response:
    *   HTTP/1.1 200 OK
    *   {
    *       "status": "OK",
    *       "data": {
    *           "total": 600
    *       }
    *   }
    */
    public function calculatetotalAction() {
        $input = $this->request->getJsonRawBody();
        $order = new CardPrintOrder;

        $order->paper_type = $input->paper_type ?? 0;
        $order->quantity   = $input->quantity ?? 0;
        $order->print_type = $input->print_type ?? 1;
        
        $this->response->setJsonContent([
            'status' => 'OK',
            'data' => [
                'total' => $order->calculateTotal()
            ]
        ]);
        return $this->response;
    }
}

