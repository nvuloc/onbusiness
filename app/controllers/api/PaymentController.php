<?php
namespace app\controllers\api;

use app\models\{PaymentLogs, Users, Notifications, AppLogs};

use  app\helpers\{NganLuong, FireBase};

class PaymentController extends ControllerBase {
    

    public function initialize() {
        $this->view->disable();
        $action = $this->dispatcher->getActionName();

        # Validate authorization for specific action
        $authActions = ['mobilecard', 'log', 'inapppurchase'];
        parent::authenticate($action, $authActions);

        # Validate HTTP method
        $rules = [
            'isGet'    => ['log'],
            'isPost'   => ['mobilecard', 'inapppurchase'],
            'isPut'    => [],
            'isDelete' => []
        ];
        parent::checkMethod($action, $rules);

        parent::initialize();
    }


    /**
    * @api {post} /api/payment/mobilecard Pay with mobile card
    * @apiName Make payment via Ngan Luong gatway
    * @apiGroup Payment
    *
    * @apiParam {String} pin_card Mã pin trên thẻ cào điện (không bao gồm dấu "-")
    * @apiParam {String} card_serial Số Serial trên card
    * @apiParam {String} type_card  Loại card `VMS => MobiFone, VNP => VinaPhone, VIETTEL => Viettel, VCOIN => VTCCoin, GATE => FPTGate`
    *
    * @apiSuccess {String} status OK
    * @apiSuccess {Integer} current_balance Số dư trong tài khoản
    * @apiSuccessExample {json} Success-Response:
    *   HTTP/1.1 200 OK
    *   {
    *       "status": "OK",
    *       "current_balance": "20000"
    *   }
    */
    public function mobilecardAction() {
        $input = (array)$this->request->getJsonRawBody();

        # Validate input
        if ( empty($input['pin_card']) || empty($input['card_serial']) || empty($input['type_card']) ) {
            $this->response->setStatusCode(403, 'Not Acceptable');
            $this->response->setJsonCOntent([
                'message' => 'Thông tin card không hợp lệ'
            ]);
            return $this->response;
        }

        $user = Users::findFirst($this->userId);

        $nl = new NganLuong([
            'client_fullname' => $user->fullname,
            'client_email'    => $user->email,
            'client_mobile'   => $user->mobile
        ]);

        $paymentAction = $nl->cardCharge([
            'pin_card'    => $input['pin_card'],
            'card_serial' => $input['card_serial'],
            'type_card'   => $input['type_card'],
        ]);

        $paymentlog = new PaymentLogs;
        $paymentlog->user_id = $user->id;
        $paymentlog->gateway = PaymentLogs::GATEWAY_NGANLUONG;
        $paymentlog->content = $nl->cardChargeSummary();

        if ($paymentAction) { # Save to payment log
            $paymentlog->status = PaymentLogs::STATUS_SUCCESS;
            $paymentlog->amount = $nl->card_amount;
            $paymentlog->xu_recharge = $paymentlog->xu;

            // Update user current balance
            $user->balance += intval($paymentlog->xu);
            $user->save();

            $this->response->setJsonContent([
                'status' => 'OK',
                'current_balance' => $user->balance
            ]);

            // Send notification to user
            $firebase = new FireBase($this->config->firebase->cloud_messaging);
            $firebase->sendNotification($user->firebase_token, [
                'title' => $this->_t('api_payment_success_coin_import_title'),
                'body' => $this->_t('api_payment_success_coin_import_body', [
                    'amount'  => $paymentlog->xu, 
                    'balance' => $user->balance,
                ]),
                'click_action' => 'USER_PROFILE'
            ]);

        } else {
            $paymentlog->status = PaymentLogs::STATUS_FAIL;
            $paymentlog->error_message = $nl->getMessage();

            $this->response->setStatusCode(403, 'Not Acceptable');
            $this->response->setJsonContent([
                'message' => $nl->getMessage()
            ]);
        }

        if (!$paymentlog->save()) {
            var_dump($paymentlog->getMessages()); die;
        }
        return $this->response;
    }

    /**
    * @api {get} /api/payment/log Log
    * @apiName Payment log
    * @apiGroup Payment
    *
    *
    * @apiSuccess {String} status OK
    * @apiSuccess {Array} data Nhật kí giao dịch
    * @apiSuccessExample {json} Success-Response:
    *   HTTP/1.1 200 OK
    *   {
    *       "status": "OK",
    *       "data": [
    *           {
    *               "gateway": "Ngan Luong",
    *               "status": "Thành công",
    *               "amount": "10000",
    *               "error": null
    *           }
    *       ]
    *   }
    */
    public function logAction() {       
        $log = PaymentLogs::getLog($this->userId);      
        $this->response->setJsonContent([
            'status' => 'OK',
            'data' => $log
        ]);
        return $this->response;
    }


    /**
    * @api {post} /api/payment/inapppurchase Apple In App Purchase
    * @apiName Make payment via Apple In app purchase
    * @apiGroup Payment
    *
    * @apiParam {String} receipt_id Mã receipt trả về từ Apple sau khi giao dịch
    *
    * @apiSuccess {String} status OK
    * @apiSuccess {Integer} current_balance Số dư trong tài khoản
    * @apiSuccessExample {json} Success-Response:
    *   HTTP/1.1 200 OK
    *   {
    *       "status": "OK",
    *       "current_balance": "2000"
    *   }
    */
    public function inapppurchaseAction() {
        $user = Users::findFirst($this->userId);
        $input = $this->request->getJsonRawBody();

        if (empty($input->receipt_id)) {
            return $this->requestFail(406, [
                'message' => 'receipt_id không hợp lệ'
            ]);
        }

        // Check in-app-purchase amount
        $apple_api = 'https://sandbox.itunes.apple.com/verifyReceipt';
        // $apple_api = 'https://buy.itunes.apple.com/verifyReceipt';
        $ch = curl_init($apple_api);
        curl_setopt_array($ch, [
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json'
            ],
            CURLOPT_POST           => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS     => json_encode(["receipt-data" => $input->receipt_id]),
        ]);

        $result = curl_exec($ch);
        curl_close($ch);

        $purchaseInfo = json_decode($result);
        $paymentlog              = new PaymentLogs;
        $paymentlog->gateway     = PaymentLogs::GATEWAY_INAPPPURCHASE;
        $paymentlog->user_id     = $this->userId;

        if ($purchaseInfo->status == 0) {
            $productId = $purchaseInfo->receipt->in_app[0]->product_id;
            if ($paymentlog->validateAppPurchaseProduct($productId)) {
                $paymentlog->status      = PaymentLogs::STATUS_SUCCESS;
                $paymentlog->amount      = $productId;
                $paymentlog->xu_recharge = $paymentlog->xu;

                // Update user balance
                $user->balance += $paymentlog->xu;
                $user->save();

                // Send notification to user
                $firebase = new FireBase($this->config->firebase->cloud_messaging);
                $firebase->sendNotification($user->firebase_token, [
                    'title' => $this->_t('api_payment_success_coin_import_title'),
                    'body' => $this->_t('api_payment_success_coin_import_body', [
                        'amount'  => $paymentlog->xu, 
                        'balance' => $user->balance,
                    ]),
                    'click_action' => 'USER_PROFILE'
                ]);

                $this->response->setJsonContent([
                    'status' => 'OK',
                    'current_balance' => $user->balance
                ]);        

            } else {
                $paymentlog->status = PaymentLogs::STATUS_FAIL;                
            }
            
            $paymentlog->content = $result;

        } else {
            $paymentlog->status        = PaymentLogs::STATUS_FAIL;
            $paymentlog->error_message = $purchaseInfo->status;
            $paymentlog->content       = $result;
        }
        
        if (!$paymentlog->save()) {
            var_dump($paymentlog->getMessages()); die;
        }
        
        return $this->response;
    }
}