<?php
namespace app\controllers\api;

use app\models\{AppLogs};
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

class LogController extends ControllerBase
{
    public function initialize() {
        $this->view->disable();
        $action = $this->dispatcher->getActionName();

        # Validate authorization for specific action
        $authActions = ['get'];
        parent::authenticate($action, $authActions);

        # Validate HTTP method
        $rules = [
            'isGet' => ['get'],
            'isPost' => [],
            'isPut' => [],
            'isDelete' => []
        ];
        parent::checkMethod($action, $rules);

        parent::initialize();
    }

    /**
    * @api {get} /api/log/get Get
    * @apiName Get user action log
    * @apiGroup Log
    *
    * @apiSuccess {String} status OK
    * @apiSuccess {Object} data List of log
    * @apiSuccessExample {json} Success-Response:
    *     
    *   HTTP/1.1 200 OK
    *     {
    *       "status": "OK",
    *       "data": [
    *         {
    *           "id": "2",
    *           "user_id": "3",
    *           "title": "This is a title"
    *           "message": "Bạn sử dụng 500 để thiết kế card",
    *           "type": "coin_charge",
    *           "created_at": "2017-02-13 16:46:53"
    *         }.
    *         {
    *           "id": "1",
    *           "user_id": "3",
    *           "title": "This is a title",
    *           "message": "Bạn vừa nạp 10 xu",
    *           "type": "coin_recharge",
    *           "created_at": "2017-02-13 16:46:53"
    *         }
    *       ]
    *     }
    */
    public function getAction() {
        $logs = AppLogs::find([
            "user_id = {$this->userId}",
            'order' => 'created_at desc',
        ]);

        $paginator = new PaginatorModel([
            "data"  => $logs,
            "limit" => 15,
            "page"  => (int) $this->request->getQuery('page', null),
        ]);

        $logs = $paginator->getPaginate()->items;
        

        $this->response->setJsonContent([
            'status' => 'OK',
            'data'   => $logs 
        ]);
        return $this->response;
    }

}

