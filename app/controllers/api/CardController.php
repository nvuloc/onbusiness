<?php
namespace app\controllers\api;

use Phalcon\Http\{Request, Response};
use Phalcon\Db\RawValue;

use app\helpers\Base64Reader;
use app\models\{Cards, Users, CardShareMaps, CardModifiedLogs, Contact, CardImages, Notifications, CoinSpendingLogs};

class CardController extends ControllerBase
{
    public function initialize() {
        $this->view->disable();
        $action = $this->dispatcher->getActionName();

        # Validate authorization for specific action
        $authActions = ['add', 'update', 'get', 'getbymobile', 'share', 'listcard', 'listshare', 'delete', 'initdefault', 'listshareable'];
        parent::authenticate($action, $authActions);

        # Validate HTTP method
        $rules = [
            'isGet' => ['get', 'getbymobile', 'listcard', 'listshare', 'listshareable'],
            'isPost' => ['add', 'share', 'initdefault'],
            'isPut' => ['update'],
            'isDelete' => ['delete']
        ];
        parent::checkMethod($action, $rules);

        parent::initialize();
    }

    /**
    * @api {get} /api/card/get/:id Get
    * @apiName Get card by id
    * @apiGroup Card
    *
    * @apiSuccess {String} status OK
    * @apiSuccess {Object} data Card info
    * @apiSuccessExample {json} Success-Response:
    *     HTTP/1.1 200 OK
    *     {
    *         "status": "OK",
    *         "data": {
    *             "id": "10",
    *             "user_id": "5",
    *             "name": "Nguyen Vu Loc",
    *             "mobile": "0908833061",
    *             "email": "crazyfrogs2@mailnesia.com",
    *             "address": "767A Nguyen Anh Thu",
    *             "company_name": null,
    *             "company_mobile": null,
    *             "company_address": null,
    *             "website": null,
    *             "status": "1",
    *             "front_img": "/uploads/123456789_uf.jpg",
    *             "back_img": "/uploads/123456789_ub.jpg",
    *             "deleted": "0",
    *             "created_at": "2017-01-15 22:55:25",
    *             "balance": "50",
    *             "design_deal_amount": "300",
    *             "deisng_deal_status": "1"
    *         }
    *     }
    */
    public function getAction($id) {
        $card = Cards::findFirst($id);
        if ( empty($card) ) {
            return $this->requestFail(404, ['message' => 'Không tìm thấy card']);
        }

        $this->response->setJsonContent([
            'status' => 'OK',
            'data'   => array_merge(
                $card->toArray(), 
                $card->getImages()
            )
        ]);
        return $this->response;
    }

    /**
    * @api {post} /api/card/add Add
    * @apiName Add card
    * @apiGroup Card
    *
    * @apiParam {String} front_img base64 encode string
    * @apiParam {String} back_img base64 encode string
    * @apiParam {String} name Name on card
    * @apiParam {String} mobile
    * @apiParam {String} email
    * @apiParam {String} address
    * @apiParam {String} company_name
    * @apiParam {String} company_mobile
    * @apiParam {String} company_address
    * @apiParam {String} website
    *
    * @apiSuccess {String} status OK
    * @apiSuccess {String} message Bạn đã thêm card thành công
    * @apiSuccess {Object} data Card info
    * @apiSuccessExample {json} Success-Response:
    *     HTTP/1.1 200 OK
    *     {
    *         "status": "OK",
    *         "message": "Bạn đã thêm card thành công",
    *         "data": {
    *             "id": "10",
    *             "user_id": "5",
    *             "name": "Nguyen Vu Loc",
    *             "mobile": "0908833061",
    *             "email": "crazyfrogs2@mailnesia.com",
    *             "address": "767A Nguyen Anh Thu",
    *             "company_name": null,
    *             "company_mobile": null,
    *             "company_address": null,   
    *             "website": null,
    *             "status": "1",
    *             "deleted": "0",
    *             "front_img": "/uploads/123456789_uf.jpg",
    *             "back_img": "/uploads/123456789_ub.jpg",   
    *             "created_at": "2017-01-15 22:55:25"
    *         }
    *     }
    */
    public function addAction() {        
        $input = $this->request->getJsonRawBody();
        $user = Users::findFirst($this->userId);

        # Check if user have to pay for this card
        $cardCount = Cards::find(["user_id = {$this->userId} AND deleted = 0"])->count();
        if ($cardCount >= $this->obConfig['onbusiness.ObMaxFreeCard']) {
            if ($user->balance < $this->obConfig['onbusiness.ObCardDesignFee']) {
                return $this->requestFail(401, ['message' => $this->_t('api_card_balance_not_available_for_new_card')]);
            }
            $is_free = false;
        } else {
            $is_free = true;
        }

        $card           = new Cards();
        $card->scenario = Cards::SCENARIO_ADDNEW;
        $card->deleted = 0;
        $card->created_at = new RawValue('NOW()');

        $card->user_id = $this->userId;
        $card->status  = Cards::STATUS_NEW;
        $fields = ['front_img', 'back_img', 'name', 'mobile', 'email', 'address', 'company_name', 'company_mobile', 'company_address', 'website'];
        foreach ($fields as $field) {
            $card->$field = $input->$field ?? null;
        }

        if (!$card->save()) {
            $errors = $card->getMessages();
            return $this->requestFail(406, ['message' => $errors[0]->getMessage() ]);
        } else {
            $card->saveImages();
            # Log card
            $log = new CardModifiedLogs;
            $log->save([
                'card_id' => $card->id,
                'content' => $card->serialize()
            ]);

            // Decrease balance by amount for design
            if (!$is_free) {
                $user->balance -= intval($this->obConfig['onbusiness.ObCardDesignFee']);
                $user->save();                
            }

            // Log coin spending
            $coinspendlog = new CoinSpendingLogs;
            $coinspendlog->save([
                'action_type' => CoinSpendingLogs::ACTIONTYPE_CARDDESIGN,
                'action_id'   => $card->id,
                'amount'      => intval($this->obConfig['onbusiness.ObCardDesignFee']),
                'user_id'     => $this->userId
            ]);
        }

        $this->response->setJsonContent([
            'status'  => 'OK',
            'message' => $this->_t('api_card_success_card_add_title'),
            'data'    => array_merge($card->toArray(), $card->getImages())
        ]);
        return $this->response;
    }

    /**
    * @api {put} /api/card/update/:id Update
    * @apiName Update card by id
    * @apiGroup Card
    *
    * @apiParam {String} front_img base64 encode string
    * @apiParam {String} back_img base64 encode string
    * @apiParam {String} name Name on card
    * @apiParam {String} mobile
    * @apiParam {String} email
    * @apiParam {String} address
    * @apiParam {String} company_name
    * @apiParam {String} company_mobile
    * @apiParam {String} company_address
    * @apiParam {String} website
    *
    * @apiSuccess {String} status OK
    * @apiSuccess {String} message 
    * @apiSuccess {Object} data Card info
    * @apiSuccessExample {json} Success-Response:
    *     HTTP/1.1 200 OK
    *     {
    *         "status": "OK",
    *         "message": "Bạn đã thêm card thành công",
    *         "data": {
    *             "id": "10",
    *             "user_id": "5",
    *             "name": "Nguyen Vu Loc",
    *             "mobile": "0908833061",
    *             "email": "crazyfrogs2@mailnesia.com",
    *             "address": "767A Nguyen Anh Thu",
    *             "company_name": null,
    *             "company_mobile": null,
    *             "company_address": null,
    *             "website": null,
    *             "status": "1",
    *             "deleted": "0",
    *             "front_img": "/uploads/123456789_uf.jpg",
    *             "back_img": "/uploads/123456789_ub.jpg",
    *             "created_at": "2017-01-15 22:55:25"
    *         }
    *     }
    */
    public function updateAction($id) {
        $card = Cards::findFirst($id);

        if ($this->userId != $card->user_id) {
            return $this->requestFail(401, ['message' => 'Không thể sửa card của người khác']);
        }

        $input = $this->request->getJsonRawBody();

        #-- Scenario: Update name card only
        $inputArray = (array)$input;
        if (count($inputArray) == 1 && !empty($input->name) && $input->name != $card->name) {
            $this->queue->put(['notifyFriendsCardUpdate' => [
                'card_id' => $card->id]
            ]);
            $card->name = $input->name;
        } 
        #-- Scenario: Normal
        else {
            $card->scenario = Cards::SCENARIO_UPDATE;
            $fields = ['front_img', 'back_img', 'name', 'mobile', 'email', 'address', 'company_name', 'company_mobile', 'company_address', 'website'];
            foreach ($fields as $field) {
                if (empty($input->$field)) continue;
                $card->$field = $input->$field;
            }

            $card->saveImages();

            # Reset status to `new` - feedback #18
            $card->status = Cards::STATUS_NEW;
            # Reset card images - feedback #18
            $card->cback_img = '/img/default_card_back.png';
            $card->cfront_img = '/img/default_card_front.png';
            $card->updated_at = new RawValue('NOW()');            
        }

        # Save card
        if ( !$card->save() ) {
            $errors = $card->getMessages();
            return $this->requestFail(406, ['message' => $errors[0]->getMessage()]);
        } else {
            // Log card
            $log = new CardModifiedLogs;
            $log->save([
                'card_id' => $card->id,
                'content' => $card->serialize()
            ]);
        }
        
        $this->response->setJsonContent([
            'status'  => 'OK',
            'message' => 'Bạn đã cập nhập card thành công',
            'data'    => array_merge($card->toArray(), $card->getImages())
        ]);
        return $this->response;
    }

    /**
    * @api {get} /api/card/listcard List card
    * @apiName List card id belong to this user
    * @apiGroup Card
    *
    * @apiSuccess {String} status OK
    * @apiSuccess {Integer} count
    * @apiSuccess {Array} data Mảng info card của user
    * @apiSuccessExample {json} Success-Response:
    *     HTTP/1.1 200 OK
    *     {
    *       "status": "OK",
    *       "count": 1,
    *       "data": [
    *           {
    *               "id": "14",
    *               "user_id": "7",
    *               "name": "Nguyen Tuan Dang",
    *               "mobile": "092131242",
    *               "email": "crazyfrogs004@mailnesia.com",
    *               "address": "767A Nguyen ANh Thur",
    *               "company_name": null,
    *               "company_mobile": null,
    *               "company_address": null,
    *               "website": null,
    *               "status": "1",
    *               "deleted": "0",
    *               "created_at": "2017-01-16 16:22:04",
    *               "is_default": "0"
    *           }
    *        ]
    *     }
    */
    public function listcardAction() {
        $cards = Cards::find([
            'user_id = ?1 and (deleted = 0 OR deleted is NULL)', 
            'bind' => [
                1 => $this->userId
            ]
        ]);

        $this->response->setJsonContent([
            'status' => 'OK',
            'count' => $cards->count(),
            'data' => $cards->toArray()
        ]);
        return $this->response;
    }

    /**
    * @api {delete} /api/card/delete/:id Delete
    * @apiName Delete card
    * @apiGroup Card
    *
    * @apiSuccess {String} status OK
    * @apiSuccess {String} message 
    * @apiSuccessExample {json} Success-Response:
    *     HTTP/1.1 200 OK
    *     {
    *       "status": "OK",
    *       "message": "Card đã xóa"
    *     }
    */
    public function deleteAction($id) {
        $card = Cards::findFirst($id);
        # Check exsiten
        if ( empty($card) ) {
            return $this->requestFail(404, ['message' => 'Card không tồn tại']);
        }
        # Check authtority
        if ($card->user_id != $this->userId) {
            return $this->requestFail(401, ['message' => 'Card này không phải của bạn']);
        }
        if ($card->is_default == $card::DEFAULT) {
            return $this->requestFail(401, ['message' => 'Bạn không thể xóa card mặc định']);
        }
        # Delete card
        $card->deleted = 1;
        $card->save();
        $this->response->setJsonContent([
            'status' => 'OK',
            'message'=> 'Card đã xóa'
        ]);
        return $this->response;
    }

    /**
    * @api {post} /api/card/initdefault Init Default
    * @apiName Init default card
    * @apiGroup Card
    * 
    * @apiParam {String} front_img base64 encode string
    * @apiParam {String} back_img base64 encode string
    * @apiParam {String} name Name on card
    * @apiParam {String} mobile
    * @apiParam {String} email
    * @apiParam {String} address
    * @apiParam {String} company_name
    * @apiParam {String} company_mobile
    * @apiParam {String} company_address
    * @apiParam {String} website
    * 
    * @apiSuccess {String} status OK
    * @apiSuccess {Object} data
    * @apiSuccessExample {json} Success-Response:
    *     HTTP/1.1 200 OK
    *     {
    *         "status": "OK",
    *         "data": {
    *             "id": "19",
    *             "user_id": "7",
    *             "name": "Nguyen Vu Loc",
    *             "mobile": "01283538080",
    *             "email": "asddasdas@ddef.com",
    *             "address": "",
    *             "company_name": null,
    *             "company_mobile": null,
    *             "company_address": null
    *             "website": null,
    *             "status": "1",
    *             "deleted": "0",
    *             "front_img": "/uploads/12430831_uf.jpg",
    *             "back_img": "/uploads/123456789_ub.jpg",
    *             "created_at": "2017-01-23 14:26:21",
    *             "is_default": "1",
    *         }
    *     }
    */
    public function initdefaultAction() {
        # Check if default existed for this user
        if ( !empty(Cards::count(["user_id = {$this->userId} AND is_default = ".Cards::DEFAULT])) ) {
            return $this->requestFail(401, ['message' => 'Bạn đã có card mặc định, không thể tạo card mặc định khác']);
        }

        $input = $this->request->getJsonRawBody();

        $card = new Cards;
        $card->scenario = Cards::SCENARIO_INITDEAFULT;
        $card->deleted = 0;
        $card->created_at = new RawValue('NOW()');
        # info
        $card->user_id    = $this->userId;
        $card->name       = $input->name;
        $card->mobile     = $input->mobile;
        $card->email      = $input->email;
        $card->address    = $input->address ?? '';
        $card->is_default = Cards::DEFAULT;
        $card->status     = Cards::STATUS_NEW;
        # Image
        $card->front_img = $input->front_img ?? null;
        $card->back_img  = $input->back_img ?? null;

        if ( !$card->save() ) {
            $errors = $card->getMessages();
            return $this->requestFail(403, ['message' => $errors[0]->getMessage()]);
        } 
        else {
            $card->refresh();
            $card->saveImages();
            $log = new CardModifiedLogs;
            $log->save([
                'card_id' => $card->id,
                'content' => $card->serialize()
            ]);
        }

        $this->response->setJsonContent([
            'status' => 'OK',
            'data'   => array_merge($card->toArray(), $card->getImages())
        ]);
        return $this->response;
    }

    /**
    * @api {get} /api/card/listshareable List shareable
    * @apiName List card able to share
    * @apiGroup Card
    * 
    * @apiSuccess {String} status OK
    * @apiSuccess {Object} data
    * @apiSuccessExample {json} Success-Response:
    *     HTTP/1.1 200 OK
    *     {
    *         "status": "OK",
    *         "data": [
    *             {
    *                 "id": "19",
    *                 "user_id": "7",
    *                 "name": "Nguyen Vu Loc",
    *                 "mobile": "01283538080",
    *                 "email": "asddasdas@ddef.com",
    *                 "address": "",
    *                 "company_name": null,
    *                 "company_mobile": null,
    *                 "company_address": null
    *                 "website": null,
    *                 "status": "1",
    *                 "deleted": "0",
    *                 "created_at": "2017-01-23 14:26:21",
    *                 "is_default": "1",
    *             },
    *             {
    *                 "id": "18",
    *                 "user_id": "7",
    *                 "name": "Nguyen Vu Loc",
    *                 "mobile": "01283538080",
    *                 "email": "asddasdas@ddef.com",
    *                 "address": "",
    *                 "company_name": null,
    *                 "company_mobile": null,
    *                 "company_address": null
    *                 "website": null,
    *                 "status": "1",
    *                 "deleted": "0",
    *                 "created_at": "2017-01-23 14:26:21",
    *                 "is_default": "1",
    *             }
    *         ]
    *     }
    */
    public function listshareableAction() {
        $cards = Cards::find([
            "user_id = {$this->userId} AND deleted = 0",
            'order' => 'created_at desc'
        ]);
        
        $this->response->setJsonContent([
            'status' => 'OK',
            'data' => $cards->toArray() ?? []
        ]);
        return $this->response;
    }
}
