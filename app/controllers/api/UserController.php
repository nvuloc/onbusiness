<?php
namespace app\controllers\api;

use Phalcon\Http\Request;
use Phalcon\Http\Response;

use app\models\Users;
use app\models\Cards;

use Phalcon\Validation;
use Phalcon\Validation\Validator\Email as EmailValidator;


class UserController extends ControllerBase
{
    public function initialize()
    {
        $this->view->disable();
        $action = $this->dispatcher->getActionName();

        # Validate authorization for specific action
        $authActions = ['update', 'listcard', 'get', 'changepassword', 'setfirebasetoken', 'logout'];
        parent::authenticate($action, $authActions);

        # Validate HTTP method
        $rules = [
            'isGet' => ['get'],
            'isPost' => ['register', 'login', 'validate'],
            'isPut' => ['update', 'changepassword', 'setfirebasetoken'],
        ];
        parent::checkMethod($action, $rules);
    }

    /**
    * @api {post} /api/user/register Register
    * @apiName Register
    * @apiGroup User
    *
    * @apiParam {String} fullname
    * @apiParam {String} mobile Chỉ chấp nhận ký tự số 0-9
    * @apiParam {String} email Đúng định dạng mail abc@qwe.example
    * @apiParam {String} password
    * @apiParam {String} access_token Code returned by Facebook
    *
    * @apiSuccess {String} status OK
    * @apiSuccess {String} token eyJ0eXAiOiJKV1QiLCJhbGzi...
    * @apiSuccess {String} message Bạn đã đăng ký thành công
    * @apiSuccessExample {json} Success-Response:
    *     HTTP/1.1 200 OK
    *     {
    *       "status": "OK",
    *       "token": "eyJ0eXAiOiJKV1QiLCJhbGzi",
    *       "message": "Bạn đã đăng ký thành công"       
    *     }
    */
    public function registerAction() {
        $request = $this->request;
        $response = $this->response;

        # Validate method
        if (!$request->isPost()) {
            $response->setStatusCode(405, 'Method Not Allowed');
            return $response;
        }
        # Validate required fields
        // $input = $request->getPost();
        $input = (array)$request->getJsonRawBody();
        $requiredFields = array_sum(array_map('array_key_exists', ['fullname', 'mobile', 'password', 'access_token'], array_fill(0, count($input), $input)));
        if (!$requiredFields || !$request->isPost()) {
            $response->setStatusCode(400, 'Bad Request');
            $response->setJsonContent(['error' => ['Required attributes missing']]);
            return $response;
        }
        # Create new user
        $user                = new Users();
        $user->fullname      = $input['fullname'];
        $user->mobile        = $input['mobile'];
        $user->email         = $input['email'];
        $user->skype         = $input['skype'] ?? '';
        $user->password_hash = $input['password'];
        
        if (!$user->setFbUserId($input['access_token'])) {
            $response->setStatusCode(401, 'Unauthorized');
            $response->setJsonContent([
                'message' => 'Không thể xác nhận số điện thoại'
            ]);
            return $response;
        }
        if ($user->create() === false) {
            $firstError = $user->getMessages()[0];
            $response->setStatusCode(406, 'Not Acceptable');
            $response->setJsonContent([
                'message' => $firstError->getMessage()
            ]);
            return $response;
        } else {
            $response->setJsonContent([
                'status' => 'OK',
                'token' => (string)$user->generateToken(),
                'message' => 'Bạn đã đăng ký thành công',
            ]);
            return $response;
        }
    }

    /**
    * @api {post} /api/user/login Login
    * @apiName Login
    * @apiGroup User
    *
    * @apiParam {String} mobile Chỉ chấp nhận ký tự số 0-9
    * @apiParam {String} password
    *
    * @apiSuccess {String} status
    * @apiSuccess {Object} data
    * @apiSuccess {String} token eyJ0eXAiOiJKV1QiLCJhbGzi...
    * @apiSuccess {String} message Bạn đã đăng nhập thành công
    * @apiSuccessExample {json} Success-Response:
    *     HTTP/1.1 200 OK
    *     {
    *       "status": "OK",
    *       "token": "eyJ0eXAiOiJKV1QiLCJhbGzi",
    *       "message": "Bạn đã đăng nhập thành công",
    *       "user": {
    *           "id": "6",
    *           "mobile": "0908833061",
    *           "email": "crazyfrogs1@mailnesia.com",
    *           "fullname": "Nguyen Vu Loc",
    *           "dob": "07-09-2002",
    *           "address": "2 Nguyen Anh Thu, Q.12",
    *           "skype": "crazyfrogs1"
    *        }
    *     }
    */
    public function loginAction() {
        $request = $this->request;
        $response = $this->response;

        # Validate required fields
        $input = (array)$request->getJsonRawBody();
        if (!isset($input['mobile']) || !isset($input['password'])) {
            $response->setStatusCode(400, 'Bad request');
            $response->setJsonContent([
                'message' => 'Yêu cầu nhập Điện thoại và password',
            ]);
            return $response;
        }

        $user = Users::findFirst([
            "mobile = ?1",
            "bind"       => [
                1 => $input['mobile'],
            ]
        ]);

        if (empty($user)) {
            $response->setStatusCode(404, 'Not found');
            $response->setJsonContent([
                'message' => 'Không tìm thấy User tương ứng'
            ]);
            return $response;
        }
        
        if ( !password_verify($input['password'] , $user->password_hash) ) {
            $response->setStatusCode(400, 'Bad request');
            $response->setJsonContent([
                'message' => 'Sai mật khẩu'
            ]);
            return $response;
        } 
        $response->setJsonContent([
            'status' => 'OK',
            'token' => (string)$user->generateToken(),
            'message' => 'Bạn đã đăng nhập thành công',
            'data' => $user->publicAttributes()
        ]);
        return $response;
    }


    /**
    * @api {put} /api/user/update Update
    * @apiName Update
    * @apiGroup User
    *
    * @apiParam {String} fullname 
    * @apiParam {String} email 
    * @apiParam {String} skype 
    * @apiParam {DateTime} dob DD-MM-YYYY
    * @apiParam {String} address 
    *
    * @apiSuccess {String} status OK
    * @apiSuccess {String} message Bạn đã cập nhập thông tin thành công
    * @apiSuccessExample {json} Success-Response:
    *     HTTP/1.1 200 OK
    *     {
    *         "status": "OK",
    *         "message": "Bạn đã cập nhập thông tin thành công"
    *     }
    */
    public function updateAction() {
        $user = Users::findFirst($this->userId);
        if (empty($user)) {
            $this->response->setStatusCode(400, 'Bad Request');
            $this->response->setJsonContent([
                'message' => 'Không tìm thấy nội dung tương thích'
            ]);
        }

        $input = (array)$this->request->getJsonRawBody();
        $allowFields = ['fullname', 'email', 'skype', 'dob', 'address'];
        foreach ($allowFields as $attr) {
            if (isset($input[$attr])) { // Only update submited data
                $user->$attr = $input[$attr];
            }
        }
        if ($user->save()) {
            $this->response->setJsonContent([
                'status' => 'OK',
                'message' => 'Bạn đã cập nhập thông tin thành công'
            ]);
        } else {
            $validateErrors = $user->getMessages();
            if ( count($validateErrors) ) {
                $this->response->setStatusCode(400, 'Bad request');
                $this->response->setJsonContent([
                    'message' => $validateErrors[0]->getMessage()
                ]);
            } else {
                $this->response->setStatusCode(500);
            }
        }
        return $this->response;
    }

    /**
    * @api {get} /api/user/get/ Get
    * @apiName Get user profile
    * @apiGroup User
    *
    * @apiSuccess {String} status OK
    * @apiSuccess {Object} data 
    * @apiSuccessExample {json} Success-Response:
    *     HTTP/1.1 200 OK
    *     {
    *         "status": "OK",
    *         "data": {
    *           "id": "6",
    *           "mobile": "0908833061",
    *           "email": "crazyfrogs1@mailnesia.com",
    *           "fullname": "Nguyen Vu Loc",
    *           "dob": "07-09-2002",
    *           "address": "2 Nguyen Anh Thu, Q.12",
    *           "skype": "crazyfrogs1"
    *         }
    *     }
    */
    public function getAction() {
        $user = Users::findFirst($this->userId);
        if ( empty($user) ) {
            $this->response->setStatusCode(404, 'Not Found');
            $this->response->setJsonCOntent([
                'message' => 'Không tìm thấy User tương ứng'
            ]);
            
        } else {
            $this->response->setJsonContent([
                'status' => 'OK',
                'data' => $user->publicAttributes()
            ]);         
        }
        return $this->response;
    }

    /**
    * @api {post} /api/user/validate Validate
    * @apiName Validate user info before register
    * @apiGroup User
    * @apiParam {String} mobile 
    * @apiParam {String} email
    *
    * @apiSuccess {String} status OK
    * @apiSuccessExample {json} Success-Response:
    *     HTTP/1.1 200 OK
    *     {
    *         "status": "OK"
    *     }
    */
    public function validateAction() {
        $request = $this->request;
        $response = $this->response;

        # Validate required fields
        $input = $this->request->getJsonRawBody();

        $validateFields = ['mobile', 'email'];
        // Mobile format
        
        if ( empty ($input->mobile) || !preg_match('/^0[0-9]{9,12}$/', $input->mobile) ) {
            $this->response->setStatusCode(403, 'Not Acceptable');
            $this->response->setJsonContent([
                'message' => 'Số điện thoại không hợp lệ'
            ]);
            return $this->response;
        }
        // Account existen
        if ( Users::count(["mobile = '{$input->mobile}'"]) ) {
            $this->response->setStatusCode(403, 'Not Acceptable');
            $this->response->setJsonContent([
                'message' => 'Số điện thoại đã được đăng ký'
            ]);
            return $this->response;
        }
        // Email format
        $input = (array)$this->request->getJsonRawBody();
        $validation = new Validation();
        $validation->add("email", new EmailValidator([
            "message" => "Email không hợp lệ"
        ]));
        $errors = $validation->validate($input);
        if ( count($errors) ) {
            $this->response->setStatusCode(403, 'Not Acceptable');
            $this->response->setJsonContent([
                'message' => 'Email không hợp lệ'
            ]);
            return $this->response;
        }

        $this->response->setJsonContent([
            'status' => 'OK'
        ]);
        return $this->response;
    }


    /**
    * @api {put} /api/user/changepassword Change Password
    * @apiName Change password
    * @apiGroup User
    * @apiParam {String} password_old 
    * @apiParam {String} password_new
    *
    * @apiSuccess {String} status OK
    * @apiSuccessExample {json} Success-Response:
    *     HTTP/1.1 200 OK
    *     {
    *         "status": "OK"
    *     }
    */
    public function changepasswordAction() {
        $input = $this->request->getJsonRawBody();
        $user  = Users::findFirst($this->userId);

        # Check old password
        if ( !password_verify($input->password_old , $user->password_hash) ) {
            $this->response->setStatusCode(403, 'Not Acceptable');
            $this->response->setJsonContent([
                'message' => 'Sai mật khẩu'
            ]);
            return $this->response;
        }

        # Update new password
        $user->password_hash = $input->password_new;
        $user->save();
        $this->response->setJsonContent([
            'status' => 'OK',
            'message' => 'Bạn đã đổi mật khẩu thành công.',
        ]);
        return $this->response;
    }


    /**
    * @api {put} /api/user/setfirebasetoken Set Firebase token
    * @apiName Change Firebase token
    * @apiGroup User
    * @apiParam {String} firebase_token
    *
    * @apiSuccess {String} status OK
    * @apiSuccessExample {json} Success-Response:
    *     HTTP/1.1 200 OK
    *     {
    *         "status": "OK"
    *     }
    */
    public function setfirebasetokenAction() {
        $input = $this->request->getJsonRawBody();

        if ( empty($input->firebase_token) ) {
            return $this->requestFail(406, [
                'message' => 'Trường dữ liệu không hợp lệ'
            ]);
        }

        $user = Users::findFirst($this->userId);

        if (empty($user)) {
            return $this->requestFail(403, [
                'message' => 'User không tồn tại'
            ]);
        } else {
            $user->firebase_token = $input->firebase_token;
            $user->save();
            $this->response->setJsonContent([
                'status' => 'OK'
            ]);
            return $this->response;
        }
    }

    /**
    * @api {post} /api/user/logout Logout
    * @apiName Logout
    * @apiGroup User
    *
    * @apiSuccess {String} status OK
    * @apiSuccessExample {json} Success-Response:
    *     HTTP/1.1 200 OK
    *     {
    *         "status": "OK"
    *     }
    */
    public function logoutAction() {
        $user = Users::findFirst($this->userId);
        $user->firebase_token = '';
        $user->save();

        $this->response->setJsonContent([
            'status' => 'OK'
        ]);
        return $this->response;
    }
}