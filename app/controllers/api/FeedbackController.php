<?php
namespace app\controllers\api;

use app\models\{Feedback};

class FeedbackController extends ControllerBase
{
	public function initialize()
    {
        $this->view->disable();
        $action = $this->dispatcher->getActionName();

        # Validate authorization for specific action
        $authActions = ['index'];
        parent::authenticate($action, $authActions);

        # Validate HTTP method
        $rules = [
			'isGet'  => [],
			'isPost' => ['index'],
			'isPut'  => [],
        ];
        parent::checkMethod($action, $rules);
    }

    /**
    * @api {post} /api/feedback Send
    * @apiName create new Feedback
    * @apiGroup Feedback
    *
    * @apiParam {String} title
    * @apiParam {String} content
    *
    * @apiSuccess {String} status OK
    * @apiSuccessExample {json} Success-Response:
    *     HTTP/1.1 200 OK
    *     {
    *       "status": "OK"
    *     }
    */
    public function indexAction() {
    	$input = $this->request->getJsonRawBody();

    	$requiredFields = ['title', 'content'];
    	$feedback = new Feedback;
    	$feedback->user_id = $this->userId;
    	if ( !$feedback->save((array)$input) ) {
    		$errors = $feedback->getMessages();
    		return $this->requestFail(406, [
    			'message' => $errors[0]->getMessage()
    		]);
    	}
    	$this->response->setJsonContent([
    		'status' => 'OK'
    	]);
    	return $this->response;
    }

}

