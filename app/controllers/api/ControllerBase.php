<?php
namespace app\controllers\api;

use Phalcon\Mvc\Controller;
use Phalcon\Http\Request;
use Phalcon\Http\Response;
use Phalcon\Translate\Adapter\NativeArray;

use Lcobucci\JWT\{Builder, Parser, ValidationData};
use app\models\{AppConfig};

use InvalidArgumentException;
use RuntimeException;

class ControllerBase extends Controller {
	protected $userId = 0;
    protected $obConfig;

    public $translate;

    public function initialize() {
        $this->translate = new NativeArray(['content' => $this->config->onbusiness->messages->toArray()]);
        $dbResult = AppConfig::find()->toArray();
        $configKeys = array_map(function($key, $type) { return "{$type}.{$key}"; }, array_column($dbResult, 'key'), array_column($dbResult, 'type'));
        $this->obConfig = array_combine($configKeys, array_column($dbResult, 'value'));
    }

    /**
     * Validate user token
     * @param string $action Current action
     * @param array $authAction List of action required authorized
     */
    public function authenticate($action, $authActions) {
    	if (!in_array($action, $authActions)) {
       		return true;
    	}
		$appConfig = $this->config->application;
		$data = new ValidationData(); 
		$data->setId($appConfig->jwtId);

		try {			
			$token = (new Parser())->parse((string) $this->request->getHeader('token')); 
			if ($token->validate($data)) {
				$this->userId = $token->getClaim('uid');
				return true;
			} else {
				$this->response->setStatusCode(401, "Unauthorized");
				$this->response->setJsonContent([
                    'message' => "Không thể xác thực người dùng"
                ]);
                $this->response->send(); die;
			}
		} catch (RuntimeException $e) {			
			$this->response->setStatusCode(401, "Unauthorized");
			$this->response->setJsonContent([
                    'message' => "Token không hợp lệ"
                ]);
            $this->response->send(); die;
		} catch (InvalidArgumentException $e) {
			$this->response->setStatusCode(401, "Unauthorized");
			$this->response->setJsonContent([
                'message' => "Token không hợp lệ"
            ]);
            $this->response->send(); die;
		}
    }

    /**
     * Validate method available to specific action
     * @param  string $action current action
     * @param  array $rules Array( <checkMethod> => <List of action to be checked> )
     */
    public function checkMethod($action, $rules) {
    	foreach ($rules as $method => $raction) {
    		if (in_array($this->dispatcher->getActionName(), $raction)) {
    			if (!$this->request->$method()) {
    				$this->response->setStatusCode(405, 'Method Not Allowed');
    				$this->response->setJsonContent([
                        'message' => 'Method Not Allowed'
                    ]);
                    $this->response->send(); die;
    			}
    		}
    	}
    }

    public function requestFail($statusCode, $data) {
        switch ($statusCode) {
            case 400:
                $this->response->setStatusCode($statusCode, 'Bad Request');
                break;
            case 401:
                $this->response->setStatusCode($statusCode, 'Unauthorized');
                break;
            case 402:
                $this->response->setStatusCode($statusCode, 'Payment Required');
                break;
            case 403:
                $this->response->setStatusCode($statusCode, 'Forbidden');
                break;
            case 404:
                $this->response->setStatusCode($statusCode, 'Not Found');
                break;
            case 405:
                $this->response->setStatusCode($statusCode, 'Method Not Allowed');
                break;
            case 406:
                $this->response->setStatusCode($statusCode, 'Not Acceptable');
                break;
            case 407:
                $this->response->setStatusCode($statusCode, 'Proxy Authentication Required');
                break;
            case 408:
                $this->response->setStatusCode($statusCode, 'Request Timeout');
                break;
            case 409:
                $this->response->setStatusCode($statusCode, 'Conflict');
                break;
            case 410:
                $this->response->setStatusCode($statusCode, 'Gone');
                break;
            case 411:
                $this->response->setStatusCode($statusCode, 'Length Required');
                break;
            case 412:
                $this->response->setStatusCode($statusCode, 'Precondition Failed');
                break;
            case 413:
                $this->response->setStatusCode($statusCode, 'Request Entity Too Large');
                break;
            case 414:
                $this->response->setStatusCode($statusCode, 'Request-URI Too Long');
                break;
            case 415:
                $this->response->setStatusCode($statusCode, 'Unsupported Media Type');
                break;
            case 416:
                $this->response->setStatusCode($statusCode, 'Requested Range Not Satisfiable');
                break;
            case 417:
                $this->response->setStatusCode($statusCode, 'Expectation Failed');
                break;
        }
        $this->response->setJsonContent($data);
        $this->response->send();
        return $this->response;
    }

    public function _t($key, $params = null) {
        return $this->translate->t($key, $params);
    }
}