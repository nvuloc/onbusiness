<?php
namespace app\controllers\api;
use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;

use app\models\{CardErrorReport, Cards};

class CardReportController extends ControllerBase
{
    public function initialize() {
        $this->view->disable();
        $action = $this->dispatcher->getActionName();

        # Validate authorization for specific action
        $authActions = ['add'];
        parent::authenticate($action, $authActions);

        # Validate HTTP method
        $rules = [
			'isGet'    => [],
			'isPost'   => ['add'],
			'isPut'    => [],
			'isDelete' => []
        ];
        parent::checkMethod($action, $rules);

        parent::initialize();
    }

    /**
    * @api {Post} /api/card-report/add/:id Add
    * @apiName Create card error report
    * @apiGroup Card report
    *
    * @apiParam {String} title
    * @apiParam {String} Content
    *
    * @apiSuccess {String} message OK
    * @apiSuccess {Object} data Card info
    * @apiSuccessExample {json} Success-Response:
    *     HTTP/1.1 200 OK
    *     {
    *         "message": "Gửi thành công",
    *         "data": {}
    *     }
    */
    public function addAction($id) {
    	$requestParams = $this->request->getJsonRawBody();
    	$card = Cards::findFirst($id);
    	if (empty($card)) {
    		return $this->requestFail(404, ['message' => $this->translate->t('card_not_exist')]);
    	}
    	if ($this->userId != $card->user_id) {
    		return $this->requestFail(401, ['message' => $this->translate->t('this_card_not_belong_to_you')]);
    	}

    	$validation = new Validation;
    	$validation->add(['title', 'content'], new PresenceOf([
    		'message' => [
    			'title' => $this->translate->t('validate_field_not_available', ['field' => $this->translate->t('title')]),
    			'content' => $this->translate->t('validate_field_not_available', ['field' => $this->translate->t('content')]),
    		]
    	]));
    	$messages = $validation->validate($requestParams);
    	if ($messages->count() > 0) {
    		return $this->requestFail(400, ['message' => $messages->current()->getMessage()]);
    	}

    	$report = new CardErrorReport;
		$report->title   = $requestParams->title;
		$report->content = $requestParams->content;
		$report->card_id = $id;
		$report->user_id = $this->userId;
		$report->save();

    	$this->response->setJsonContent([
			'message' => $this->translate->t('request_success'),
			'data'    => new \StdClass() // empty object if not data
    	]);
    	return $this->response;
    }
 
}

