<?php
namespace app\controllers\admin;

use Phalcon\Paginator\Adapter\Model as PaginatorModel;
use Phalcon\Db\RawValue;
use app\models\{Users, Cards, CardModifiedLogs, CardImages, CardDesignDeal, CoinSpendingLogs, Notifications, Staff};

use app\helpers\FireBase;

class CardController extends ControllerBase
{

	public function initialize() {
		parent::initialize();
		$this->view->controllerName = 'card';
	}


	/**
	 * List all cards
	 * @param  integer $page Current page
	 * @return
	 */
	public function indexAction() {

		$search = $this->request->getQuery('search') ?? null;
		$filter = $this->request->getQuery('filter') ?? null;

		$searchQuery = Cards::query()->where("app\\models\\Cards.deleted = 0");

		if (!empty($search['user_mobile'])) {
			$searchQuery->join('app\\models\\Users', 'app\\models\\Users.id = app\\models\\Cards.user_id')
						->andWhere("( app\\models\\Cards.mobile LIKE ?2)", [
							// 1 => "%{$search['user_mobile']}%",
							2 => "%{$search['user_mobile']}%"
						]);
		}

		if ($this->staff->role == Staff::ROLE_DEISGNER) {
			$searchQuery->inWhere("app\\models\\Cards.status", [Cards::STATUS_NEW, Cards::STATUS_PROCEED]);
		}
		elseif ( !empty($filter['status'] )) {
			$searchQuery->andWhere("app\\models\\Cards.status = :status:", ['status' => $filter['status']]);
		}

		$searchQuery->orderBy("app\\models\\Cards.created_at desc");

		$cards = $searchQuery->execute();

		$paginator = new PaginatorModel([
			"data"  => $cards,
			"limit" => 12,
			"page"  => (int) $this->request->getQuery('page', null),
		]);


		$this->view->setVars([
			'title' => 'Card',
			'cards' => $paginator->getPaginate(),
			'search' => $search,
			'filter' => $filter
		]);
	}


	/**
	 * Update card
	 * @param  Integer $id Card id
	 */
	public function updateAction($id) {
		# Register JS library
		$libraryCollection = $this->assets->collection("library");	
		$libraryCollection->addJs('js/qrcode.min.js');

		$card = Cards::findFirst($id);

		$this->view->card = $card;
		$this->view->title = $this->view->card->name;

		if ($this->request->isPost() && $this->security->checkToken()) {
			$card->cfront_img = null;
			$card->cback_img = null;
			$input = $this->request->getPost('Cards');
			$user  = $card->user;

			# Upload image
			if ($this->request->hasFiles() == true) {
				foreach ($this->request->getUploadedFiles() as $file) {
					if (empty($file->getExtension())) continue;
					
					$imageContent = $file->getKey();
					$$imageContent = new CardImages;

					switch ($imageContent) {
						case 'back_img':
							$fileName = time() . '_ab.'.$file->getExtension();
							$$imageContent->type = CardImages::ADMIN_UPLOAD_BACK;
							$card->cback_img = '/uploads/'.$fileName;
							break;
						case 'front_img':
							$fileName = time() . '_af.'.$file->getExtension();
							$$imageContent->type = CardImages::ADMIN_UPLOAD_FRONT;
							$card->cfront_img = '/uploads/'.$fileName;
							break;
					}
					$file->moveTo( $this->config->application->uploadsDir.$fileName );

					$$imageContent->location = '/uploads/'.$fileName;
					$$imageContent->card_id = $card->id;
					if (!$$imageContent->save() ) {
						var_dump($$imageContent->getMessages()); die;
					}
				}
			}
			
			//-- Reset to last image uploaded by admin
			$adminLastUpload = $card->getLastImageAdminUpload();
			if (empty($card->cfront_img)) {
				$card->cfront_img = $adminLastUpload['front_img'];
			}
			if (empty($card->cback_img)) {
				$card->cback_img = $adminLastUpload['back_img'];
			}

			//-- Send notification to user if admin change card status
			if ($input['status'] != $card->status && $input['status'] == Cards::STATUS_DONE) {
				$card->notifyStatusChange($input['status']);
				// Send notification user friend in contact
	            $this->queue->put(['notifyFriendsCardUpdate' => [
	                'card_id' => $card->id]
	            ]);	
	        } 
			
			$card->updated_at = new RawValue('NOW()');
			$card->save($this->request->getPost('Cards'));

			if ($card->status == Cards::STATUS_NEW) {
				$card->resetImage();
			}

			return $this->response->redirect('admin/card/index');
		}
	}

	/**
	 * List all card modified log
	 * @param  Integer $id card_id
	 */
	public function modifylogAction($id) {
		$this->view->card = Cards::findFirst($id);
		$this->view->logs = CardModifiedLogs::find(['card_id = ?1', 'bind' => [1 => $id], 'limit' => 20, 'order' => 'created_at desc' ]);;
	}

	public function createdesigndealAction($id) {
		return;
		if ($this->request->isPost() && $this->security->checkToken()) {
			$card = Cards::findFirst($id);
			if ( empty ($card) ) {
				echo 'Not Found'; die;
			}

			$input = $this->request->getPost();
			$deal = new CardDesignDeal;
			$action = $deal->save([
				'card_id' => $id,
				'amount'  => $input['amount'],
				'status'  => CardDesignDeal::STATUS_CREATED
			]);

			if ($action) {

				# Send notification to user
				$user = Users::findFirst($card->user_id);
				$firebase = new FireBase($this->config->firebase->cloud_messaging);
				$firebase->sendNotification($user->firebase_token, [
					'title'        => 'Admin đã duyệt card của bạn',
					'body'         => "Chi phí thiết kế card của bạn là {$deal->amount} xu",
					'click_action' => "CARD_VIEW|{$card->id}",
				]);

				return $this->response->redirect('admin/card/index');			
			} else {
				var_dump($deal->getMessages()); die;
			}
		}
	}
}