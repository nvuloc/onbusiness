<?php
namespace app\controllers\admin;

use Phalcon\Mvc\Controller;
use Phalcon\Translate\Adapter\NativeArray;

use app\models\{AppConfig, Staff};

class ControllerBase extends Controller
{
    public $messages;
    public $translate;
    public $staff;

    protected $obConfig;

	public function initialize() {
        $this->view->setViewsDir($this->config->application->viewsDir.'admin');
        $this->assets->collection("library");
        $this->messages = $this->config->onbusiness->messages;
        $this->translate = new NativeArray(['content' => $this->config->onbusiness->messages->toArray()]);
        $this->view->t = $this->translate;

        # Load config from DB
        $dbResult = AppConfig::find()->toArray();
        $configKeys = array_map(function($key, $type) { return "{$type}.{$key}"; }, array_column($dbResult, 'key'), array_column($dbResult, 'type'));
        $this->obConfig = array_combine($configKeys, array_column($dbResult, 'value'));

        # User role
        $this->view->userRole = $this->staff->role;
        $this->view->staff    = $this->staff;
    }

    /**
     * Require authentication 
     * @param  Dispatcher $dispatcher
     */
    public function beforeExecuteRoute($dispatcher) {
        $action     = $this->dispatcher->getActionName();
        $controller = $this->dispatcher->getControllerName();

        $auth = $this->session->get("auth");
        if (!$auth) {
        	if ($controller != 'index') {
        		return $this->response->redirect('/admin/login');
        	} else {
	 	        if ($action != 'login') {
		        	return $this->response->redirect('/admin/login');
		        }        		
        	}
        }
        $this->staff = Staff::findFirst($auth['id']);
    }

    public function _t($key, $params = null) {
        return $this->translate->t($key, $params);
    }
}