<?php
namespace app\controllers\admin;

use Phalcon\Paginator\Adapter\Model as PaginatorModel;

use app\models\{Users, CardModifiedLogs, CardImages, CardDesignDeal, CoinSpendingLogs, Notifications};

use app\helpers\FireBase;

class NotificationController extends ControllerBase
{

	public function initialize() {
		parent::initialize();
		$this->view->controllerName = 'notifications';
	}

	/**
	 * List all cards
	 * @param  integer $page Current page
	 * @return
	 */
	public function indexAction() {

		if ( empty($this->request->getQuery('search') ) ||  empty($this->request->getQuery("select"))) {
			$notifications = Notifications::find([
				'order' => 'created_at desc',
			]);
		} else {
			$search = $this->request->getQuery('search');
			$select = $this->request->getQuery("select");

			$notifications = Notifications::find([
				 $select . '=' . $search ,
				'order' => 'created_at desc',
			]);
		};

		$paginator = new PaginatorModel([
			"data"  => $notifications,
			"limit" => 12,
			"page"  => (int) $this->request->getQuery('page', null),
		]);

		$this->view->setVars([
			'title' => 'Notifications',
			'notifications' => $paginator->getPaginate()
		]);
	}
}