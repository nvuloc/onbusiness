<?php
namespace app\controllers\admin;

use Phalcon\Paginator\Adapter\Model as PaginatorModel;

use app\models\{Users, Cards, CardModifiedLogs, CardImages, CardDesignDeal, CoinSpendingLogs, Notifications, AppLogs};

use app\helpers\FireBase;

class ApplogController extends ControllerBase
{

	public function initialize() {
		parent::initialize();
		$this->view->controllerName = 'payment';
	}


	/**
	 * List all cards
	 * @param  integer $page Current page
	 * @return
	 */
	public function indexAction() {

		if ( empty($this->request->getQuery('search') ) ) {
			$applog = AppLogs::find([
				'order' => 'created_at desc',
			]);
		} else {
			$search = $this->request->getQuery('search');
			$select = $this->request->getQuery("select");

			$applog = AppLogs::find([
				 $select . '=' . $search ,
				'order' => 'created_at desc',
			]);
		};

		$paginator = new PaginatorModel([
			"data"  => $applog,
			"limit" => 12,
			"page"  => (int) $this->request->getQuery('page', null),
		]);
			$listApplog = $paginator->getPaginate()->items;
			foreach ($listApplog as $value) {
				$user = Users::findFirst([
					'columns' => 'fullname',
					'id = ' . $value->user_id,
				]);
				$value->name_user = $user->fullname; 
			}
		$this->view->setVars([
			'title' => 'Payment',
			'contacts' => $listApplog,
			'page' => $paginator->getPaginate(),
		]);
	}
}