<?php
namespace app\controllers\admin;

use Phalcon\Validation;
use Phalcon\Validation\Validator\Confirmation;

use app\models\Staff;

class StaffController extends ControllerBase
{

    public function indexAction()
    {

    }

    public function changepasswordAction() {
    	if ($this->request->isPost() && $this->security->checkToken()) {
    		$validation = new Validation();
    		$validation->add('password', new Confirmation([
				'message' => 'Xác nhận mật khẩu không đúng',
				'with'    => 'password_confirm'
    		]));

    		$messages = $validation->validate($this->request->getPost());
    		if ( $messages->count() ) {
    			$this->view->error = $messages->current()->getMessage();
    		} else {
    			$this->staff->password_hash = $this->security->hash($this->request->getPost('password'));
    			$this->staff->save();
    		}
    	}
    }

    public function chpwddesignerAction() {
        if ($this->staff->role == Staff::ROLE_DEISGNER) exit('Not Authorized');

        if ($this->request->isPost() && $this->security->checkToken()) {
            $validation = new Validation();
            $validation->add('password', new Confirmation([
                'message' => 'Xác nhận mật khẩu không đúng',
                'with'    => 'password_confirm'
            ]));

            $messages = $validation->validate($this->request->getPost());
            if ( $messages->count() ) {
                $this->view->error = $messages->current()->getMessage();
            } else {
                $designers = Staff::find(["role = 2"]);
                foreach ($designers as $staff) {
                    $staff->password_hash = $this->security->hash($this->request->getPost('password'));
                    $staff->save();
                }
            }
        }
    }

}

