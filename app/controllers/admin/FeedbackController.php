<?php

namespace app\controllers\admin;

use Phalcon\Paginator\Adapter\Model as PaginatorModel;
use app\models\{Feedback};

class FeedbackController extends ControllerBase
{

    public function initialize() {
        parent::initialize();
    }

    public function indexAction()
    {
        $feedback = Feedback::find();
        $paginator = new PaginatorModel([
            'data' => Feedback::find(),
            'limit' => 10,
            'current_page' => $this->request->getQuery('page', 1)
        ]);
        
        $this->view->data = $paginator->getPaginate();
    }

}

