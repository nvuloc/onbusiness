<?php
namespace app\controllers\admin;

use Phalcon\Paginator\Adapter\Model as PaginatorModel;
use app\models\{CardPrintOrder, Cards};

class CardPrintController extends ControllerBase
{

	public function initialize() {
        parent::initialize();
		$this->view->controllerName = 'cardprint'; 
    }

    public function indexAction() {
        $search = $this->request->getQuery('search', null);

        $searchQuery = CardPrintOrder::query();

        # Filter by status
        $this->view->search_status = $search['status'] ?? [];
        if (!empty($search['status'])) {
            $searchQuery->inWhere('status', $search['status']);
        }

        # Filter by string relate to fields `mobile`
        $this->view->search_field = $search['field'] ?? '';
        if (!empty($search['field'])) {
            $searchQuery->andWhere("mobile like :mobile:", ['mobile' => "%{$search['field']}%"]);
        }

        $searchQuery->orderBy('created_at desc');
        $orders = $searchQuery->execute();


    	// Create a Model paginator, show 15 rows by page starting from $currentPage
		$paginator = new PaginatorModel(
		    [
		        "data"  => $orders,
		        "limit" => 15,
		        "page"  => (int) $this->request->getQuery('page', null),
		    ]
		);

		$this->view->setVars([
			'title' => $this->translate->t('card_print'),
			'page'  => $paginator->getPaginate(),
            'search' => $search
		]);
    }

    public function updatestatusAction($id) {
    	$order = CardPrintOrder::findFirst($id);

    	$updatedStatus = $this->request->getPost('status') ?? null;
    	if (!empty($updatedStatus)) {
    		$order->status = $updatedStatus;
    		$order->save();

    		return $this->response->redirect('/admin/card-print/index');
    	}
    	else {
    		var_dump($this->request->getPost()); die;
    	}
    }

}

