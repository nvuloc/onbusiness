<?php
namespace app\controllers\admin;

use app\models\{AppConfig, CardPrintMaterial};

class ConfigController extends ControllerBase
{

	public function initialize() {
		parent::initialize();
		$this->view->controllerName = 'config';
	}

    public function indexAction() {
        # Load config from DB
        $dbResult = AppConfig::find()->toArray();
        $configKeys = array_map(function($key, $type) { return "{$type}.{$key}"; }, array_column($dbResult, 'key'), array_column($dbResult, 'type'));

        $this->view->obConfig = array_combine($configKeys, array_column($dbResult, 'value'));
        $this->view->cardPrintMaterial = CardPrintMaterial::find(["active = 1"]);

    }

    public function storeMobileSettingAction() {
    	$validFields = [
    		'iOSPublishVersion', 
    		'iOSAppVersion', 
    		'AdVersion', 
    		'AdNewVersionLink'
    	];

    	foreach($validFields as $field) {
   			if (empty($this->request->getPost($field))) continue;
    		$config = AppConfig::findFirst(["key = '{$field}'"]);
    		if (empty($config)) continue;
    		$config->value = $this->request->getPost($field);
    		$config->save();
    	}

    	return $this->response->redirect('/admin/config');
    }

    public function storeObSettingAction() {
        $validFields = [
            'ObMaxFreeCard',
            'ObCardDesignFee'
        ];

        foreach($validFields as $field) {
            if (empty($this->request->getPost($field))) continue;
            $config = AppConfig::findFirst(["key = '{$field}'"]);
            if (empty($config)) continue;
            $config->value = $this->request->getPost($field);
            $config->save();
        }

        return $this->response->redirect('/admin/config');
    }

    public function storeCardPrintMaterialAction() {
        $requestParams = $this->request->getPost('material');

        try {
            foreach ($requestParams as $key => $value) {
                $printMaterial = CardPrintMaterial::findFirst($key);
                if (!$printMaterial->save($value)) {
                    throw new \Exception(json_encode($printMaterial->getMessages()));
                }
            }
            return $this->response->redirect('/admin/config');
        } catch (\Exception $e) {
            var_dump($e->getMessage()); die;
        }
    }

    public function storeobconfigAction() {
        $requestParams = $this->request->getPost('config');
        try {
            foreach ($requestParams as $key => $value) {
                $config = AppConfig::findFirst([
                    "key = ?1 AND type = ?2",
                    "bind" => [
                        1 => $key,
                        2 => 'onbusiness'
                    ]
                ]);
                if (empty($config)) continue;
                $config->value = $value;
                if (!$config->save()) {
                    throw new Exception("Config: ".$config->getMessages());
                    
                }
            }
            return $this->response->redirect('/admin/config');
        } catch (Exception $e) {
            var_dump($e->getMessage()); die;
        }
    }

}

