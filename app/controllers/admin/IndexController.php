<?php
namespace app\controllers\admin;

use Phalcon\Paginator\Adapter\Model as PaginatorModel;
use Phalcon\Mvc\View;

use app\models\Staff;

class IndexController extends ControllerBase
{

	/**
	 * Log user in
	 * @param  Staff $staff
	 */
	private function _registerSession($staff) {
		$this->session->set('auth', [
			'id'       => $staff->id,
			'username' => $staff->username
		]);
	}

	/**
	 * Login system
	 * @return [type] [description]
	 */
	public function loginAction() {
		$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

		if ( $this->request->isPost() ) {
			$staff = Staff::findFirst([
				'username = ?1', 
				'bind' => [
					1 => $this->request->getPost('username')
				]
			]);
			if ( empty ($staff) || !$this->security->checkHash($this->request->getPost('password'), $staff->password_hash) ) {
				// Set error flash
				$this->flash->error('Sai username hoặc password');
				$this->view->error = 'Sai username hoặc password';
				return $this->view;
			}

			$this->_registerSession($staff);
			$this->flash->success('Đăng nhập thành công');
			return $this->response->redirect('/admin');
		}
	}

	/**
	 * Register staff for app management
	 * @return [type] [description]
	 */
	public function registerAction() {
		if ( $this->request->isPost() ) {
			$staff = new Staff;
			$staff->username = $this->request->getPost('username');
			$staff->password_hash = $this->security->hash($this->request->getPost('password'));
			if ( $staff->create() === false ) {
				$this->response->setStatusCode(400, 'Bad request');
				$this->response->setJsonContent([
					'error' => array_map(function($err) {
						return $err->getMessage();
					}, $staff->getMessages()),
				]);
				return $this->response;
			}

			$this->response->setJsonContent([
				'message' => 'Register success'
			]);
			return $this->response;
		}
	}

	/**
	 * Logout
	 * @return [type] [description]
	 */
	public function logoutAction() {
		$this->session->destroy();
		return $this->response->redirect('/admin/login');
	}

	public function dashboardAction() {

	}
}