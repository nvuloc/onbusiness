<?php
namespace app\controllers\admin;

use Phalcon\Paginator\Adapter\Model as PaginatorModel;
use app\models\{CardErrorReport};

class CardReportController extends ControllerBase
{
	public function initialize() {
        parent::initialize();
		$this->view->controllerName = 'cardreport'; 
    }

    public function indexAction()
    {

    	$search = $this->request->getQuery('search', null);

        $searchQuery = CardErrorReport::query();

        $searchQuery->orderBy('id desc');
        $reports = $searchQuery->execute();


    	// Create a Model paginator, show 15 rows by page starting from $currentPage
		$paginator = new PaginatorModel(
		    [
		        "data"  => $reports,
		        "limit" => 15,
		        "page"  => (int) $this->request->getQuery('page', null),
		    ]
		);

		$this->view->setVars([
			'title'  => $this->translate->t('card_error_report'),
			'page'   => $paginator->getPaginate(),
			'search' => $search
		]);
    }

}

