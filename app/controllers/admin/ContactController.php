<?php
namespace app\controllers\admin;

use Phalcon\Paginator\Adapter\Model as PaginatorModel;

use app\models\{Users, Contact, CardModifiedLogs, CardImages, CardDesignDeal, CoinSpendingLogs, Notifications};

use app\helpers\FireBase;

class ContactController extends ControllerBase
{

	public function initialize() {
		parent::initialize();
		$this->view->controllerName = 'contact';
	}


	/**
	 * List all cards
	 * @param  integer $page Current page
	 * @return
	 */
	public function indexAction() {

		if ( empty($this->request->getQuery('search') ) ) {
			$contact = Contact::find([
				'order' => 'created_at desc',
			]);
		} else {
			$search = $this->request->getQuery('search');
			$select = $this->request->getQuery("select");

			$contact = Contact::find([
				 $select . '=' . $search ,
				'order' => 'created_at desc',
			]);
		};

		$paginator = new PaginatorModel([
			"data"  => $contact,
			"limit" => 12,
			"page"  => (int) $this->request->getQuery('page', null),
		]);
			$listContact = $paginator->getPaginate()->items;
			foreach ($listContact as $value) {
				$user = Users::findFirst([
					'columns' => 'fullname',
					'id = ' . $value->user_id,
				]);
				$value->name_user = $user->fullname; 
			}
		$this->view->setVars([
			'title' => 'Contact',
			'contacts' => $listContact,
			'page' => $paginator->getPaginate(),
		]);
	}
}