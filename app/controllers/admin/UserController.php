<?php
namespace app\controllers\admin;

use Phalcon\Paginator\Adapter\Model as PaginatorModel;

use app\models\{Users, PaymentLogs, Contact, CoinSpendingLogs};

# Validation
use Phalcon\Validation;
use Phalcon\Validation\Validator\Confirmation;
use Phalcon\Validation\Validator\PresenceOf;

class UserController extends ControllerBase
{

	public function initialize() {
		parent::initialize();
		$this->view->controllerName = 'user';

		if ($this->staff->role == 2) exit('Bạn không có quyền truy cập trang cập');
	}

	/**
	 * List all registerd user
	 * @param  integer $page Current page
	 * @return
	 */
	public function indexAction() {
		 
		// The data set to paginate
		$search = $this->request->getQuery('search') ?? null;
		$this->view->search = $search;

		$searchQuery = Users::query();

		if (!empty($search)) {
			$searchQuery->where('( email like :email: OR fullname like :fullname: OR mobile like :mobile: )', [
					'email' => "%{$search}%",
					'fullname' => "%{$search}%",
					'mobile' => "%{$search}%"	
			]);
		}
		$searchQuery->orderBy('created_at desc');

		$users = $searchQuery->execute();

		// Create a Model paginator, show 10 rows by page starting from $currentPage
		$paginator = new PaginatorModel(
		    [
		        "data"  => $users,
		        "limit" => 10,
		        "page"  => (int) $this->request->getQuery('page', null),
		    ]
		);

		$this->view->setVars([
			'title' => 'Người dùng',
			'page'  => $paginator->getPaginate()
		]);
	}

	/**
	 * Update user
	 * @param  integer $id
	 * @return
	 */
	public function updateAction($id) {
		if (!is_numeric($id)) {
			$this->response->setStatusCode(400, 'Bad request');
			throw new \Exception ('Request không hợp lệ');
			return $this->response;
		}
		$user = Users::findFirst($id);
		if (empty($user)) {
			$this->response->setStatusCode(404, 'Not found');
			throw new \Exception('Không tìm thấy người dùng tương ứng');
			return $this->response;
		}

		if ($this->request->isPost() && $this->security->checkToken()) {
			$user->save($this->request->getPost('Users', []));
			return $this->dispatcher->forward([
				'controller' => 'user',
				'action'     => 'index'
			]);
		}

		# Contact
		$rawContact = Contact::find(["user_id = {$user->id}", 'order' => 'created_at desc']);
		$contactLists = [];
		foreach ($rawContact as $contact) {
			$contactLists[] = $contact->info;
		}
		$this->view->contactLists = $contactLists;

	
		// Variables
		$this->view->setVars([
			'title' => '#' . $user->id . ' - ' . $user->fullname,
			'user'  => $user
		]);

		// View assets
		$libs = $this->assets->collection("library");
		# Date range picker
		$libs->addJs('gentelella/vendors/moment/min/moment.min.js');
		$libs->addJs('gentelella/vendors/bootstrap-daterangepicker/daterangepicker.js');
		$this->assets->addCss('gentelella/vendors/bootstrap-daterangepicker/daterangepicker.css');
		
	}

	public function paymentlogAction($id) {
		$this->view->user = Users::findFirst($id);
		$this->view->paymentLogs = PaymentLogs::getLog($id, true);
	}

	public function coinspentlogAction($id) {
		$this->view->user = Users::findFirst($id);
		$this->view->coinspentLogs = CoinSpendingLogs::find([
			"user_id = {$id}",
			'order' => 'created_at desc'
		]);
	}

	public function addxuAction() {
		$requestParams = $this->request->getPost();
		extract($requestParams);

		if ( empty($uid) || empty($xu) ) 
			return $this->response->redirect('/admin/user/index');

		$user = Users::findFirst($uid);
		if (empty($user)) 
			return $this->response->redirect('/admin/user/index');

		$user->balance += intval($xu);
		if (!$user->save()) 
			return $this->response->redirect('/admin/user/index');

		$log = new PaymentLogs;
		$log->gateway = PaymentLogs::GATEWAY_ADMINADD;
		$log->status = PaymentLogs::STATUS_SUCCESS;
		$log->amount = 0;
		$log->xu_recharge = $xu;
		$log->user_id = $user->id;
		$log->content = 'Admin thêm vào '.$xu.' xu';

		if (!$log->save()) {
			var_dump($log->getMessages()); die;
		}

		return $this->response->redirect('/admin/user/update/'.$user->id);
	}

	public function changepasswordAction($id) {
		$validator = new Validation;
		$validator->add('password', new Confirmation([
			'message' => 'Xác nhận mật khẩu không đúng',
			'with'    => 'password_confirmation'
		]));
		$validator->add(['password'], new PresenceOf([
			'message' => [
				'password' => 'Password không đuợc trống',
			]
		]));

		$messages = $validator->validate($this->request->getPost());
		if (!empty($messages->count())) {
			$this->flashSession->error($messages->current()->getMessage());
		} 
		else {			
			$user = Users::findFirst($id);
			$password = $this->request->getPost('password');
			$user->password_hash = $password;
			if ($user->save()) {
				$this->flashSession->success("Đổi mật khẩu thành công");		
			}
		}

		return $this->response->redirect('/admin/user/update/'.$id);
	}
}

