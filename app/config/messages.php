<?php

return [
	# Message for admin
	'admin_card_design_not_deal_yet' => 'Card này chưa báo giá cho client',
	'admin_user_balance_not_available_for_payment' => 'Tài khoản người dùng không đủ để thực hiện thanh toán',

	'admin_card_print_material_paper_type_1' => 'Giấy lụa',
	'admin_card_print_material_paper_type_2' => 'Giấy bóng',
	'admin_card_print_material_paper_type_3' => 'Giấy carton',
	
	# Message for api
	'api_payment_success_coin_import_title' => 'Bạn đã nạp xu thành công',
	'api_payment_success_coin_import_body' => 'Bạn vừa nạp %amount% xu. Tài khoảng hiện tại của bạn là %balance% xu',
	'api_card_balance_not_available_for_new_card' => 'Tài khỏan của bạn không đủ xu để tạo card mới',
	'api_card_balance_not_available_for_print_card' => 'Tài khỏan của bạn không đủ để in card',

	'api_card_success_card_add_title' => 'Bạn đã thêm card thành công',
	'api_card_success_card_add_body' => 'Bạn đã thêm card thành công',
	'api_card_not_found' => 'Không tìm thấy card',
	'api_card_unauthorize' => 'Đây không phải là card của bạn',
	'api_card_card_not_available_for_share' => 'Card không được phép share',

	'api_card_print_order_success' => 'Bạn đã order thành công',
	'api_card_print_paper_type_not_exist' => 'Chưa chọn loại giấy',

	# Notification
	'notification_card_status_procceed_title' => 'Hệ thống đang xử lý card của bạn',
	'notification_card_status_procceed_body' => 'Card %name% đang được xử lý',

	'notification_card_status_done_title' => 'Hệ thống đã duyệt card của bạn',
	'notification_card_status_done_body' => 'Card của bạn đã được thiết kế xong',
	
	'notification_card_status_cancel_title' => 'Hệ thống đã hủy card của bạn',
	'notification_card_status_cancel_body' => 'Card của bạn đã bị hủy',
	
	'notification_contact_scan_success_title' => 'Chia sẻ card',
	'notification_contact_scan_success_body' => '%name% vừa thêm bạn vào danh bạ',

	'notification_friend_update_card' => '%name% đã thay đổi thông tin thành công',

	# Log
	'log_payment_recharge_success' => 'Bạn vừa nạp %amount% xu',
	'log_payment_recharge_title' => 'Bạn đã nạp xu',
	'log_coin_spend_card_design_title' => 'Bạn đã tạo card mới',
	'log_coin_spend_card_design' => 'Bạn sử dụng %amount% để thiết kế card',
	'log_coin_spend_card_print_title' => 'Bạn in card',
	'log_coin_spend_card_print' => 'Chi phí %amount% xu',

	# Validation
	'validate_field_not_available' => '%field% không hợp lệ',

	# Common
	'request_success' => 'Gửi thành công',
	'card_not_exist' => 'Card không tồn tại',
	'this_card_not_belong_to_you' => 'Card này không thuộc về bạn',
	'title' => 'Tiêu đề',
	'content' => 'Nội dung',
	'card_print' => 'In card',
	'card_error_report' => 'Báo lỗi card',
];