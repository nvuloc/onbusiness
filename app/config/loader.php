<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerDirs(
    [
        $config->application->modelsDir
    ]
);

$loader->registerNamespaces([
    # Controllers
    'app\controllers' => $config->application->controllersDir,
    'app\controllers\api' => $config->application->controllersDir.'api',
    'app\controllers\admin' => $config->application->controllersDir.'admin',

    # Models
    'app\models' => $config->application->modelsDir,

	# External resource
    'Facebook'      => BASE_PATH.'/vendor/facebook/graph-sdk/src/Facebook',
    'Lcobucci\JWT'  => BASE_PATH.'/vendor/lcobucci/jwt/src',
    'app\helpers'   => APP_PATH.'/helpers',
]);

$loader->register();
