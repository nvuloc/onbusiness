<?php
/*
 * Modified: prepend directory path of current file, because of this file own different ENV under between Apache and command line.
 * NOTE: please remove this comment.
 */
defined('BASE_PATH') || define('BASE_PATH', getenv('BASE_PATH') ?: realpath(dirname(__FILE__) . '/../..'));
defined('APP_PATH') || define('APP_PATH', BASE_PATH . '/app');

$messages = include(__DIR__.'/messages.php');
$db = include(__DIR__.'/db.php');

return new \Phalcon\Config([
	'database' => $db,
	'application' => [
		'appDir'         => APP_PATH . '/',
		'controllersDir' => APP_PATH . '/controllers/',
		'modelsDir'      => APP_PATH . '/models/',
		'migrationsDir'  => APP_PATH . '/migrations/',
		'viewsDir'       => APP_PATH . '/views/',
		'pluginsDir'     => APP_PATH . '/plugins/',
		'libraryDir'     => APP_PATH . '/library/',
		'cacheDir'       => BASE_PATH . '/cache/',

		// This allows the baseUri to be understand project paths that are not in the root directory
		// of the webpspace.  This will break if the public/index.php entry point is moved or
		// possibly if the web server rewrite rules are changed. This can also be set to a static path.
		'baseUri'   => preg_replace('/(public([\/\\\\])index.php|index.php)$/', '', $_SERVER["PHP_SELF"]),
		
		'jwtId'      => 'Gu77puG82spLgxwY',
		'uploadsDir' => BASE_PATH.'/public/uploads/',
	],
	
	'facebook' => [
		'app_id'                  => '241841336162962',
		'app_secret'              => 'e2145ba344ccc3fb992584acf59cd883',
		'account_kit_secrect'	  => 'e9159d1850434ac28ff96df0e9b1da8f',
		'default_graph_version'   => 'v2.2'
	],
	'nganluong' => [
		'Email'        => 'chutieu7@gmail.com',
		'MerchantID'   => '48799',
		'MerchantPass' => '951b152946d94c9f53c3ec73cb554841',
	],

	'firebase' => [
		'cloud_messaging' => [
			'server_key'        => 'AAAA3fZ3w2w:APA91bHdFKSPsER_v578OdgG11bZyU9Jk-uKcSdNAFZJUxQa_Adym5Cbm5UAAcoL6GCrh4Z2J0dyHHVAHrOkolXCZnA3d3XYfqWbTNspFoILZEWvJvULy-h_arofGqQpLO5EFI0Mu62A',
			'legacy_server_key' => 'AIzaSyBTLokL5_68zJnXikUgtSDmqJHXF85JIjQ',
			'sender_id'         => '953322816364'			
		]
	],

	'onbusiness' => [
		'max_free_card'   => 1,
		'card_design_fee' => 100,
		'messages'        => $messages,
	]
	
]);
