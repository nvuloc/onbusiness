<?php
$router = new Phalcon\Mvc\Router(false);

$router->setDefaultNamespace('app\controllers');
$router->add('/:controller/:action/:params', [
    'namespace'  => 'app\controllers',
    'controller' => 1,
    'action'     => 2,
    'params'     => 3,
]);
$router->add('/:action', [
    'namespace'  => 'app\controllers',
    'controller' => 'index'
]);

$router->add('/:action', [
    'namespace' => 'app\controllers',
    'controller' => 'index',
    'action' => 1
]);




// Api module
$router->add('/api/:controller/:action/:params', [
    'namespace'  => 'app\controllers\api',
    'controller' => 1,
    'action'     => 2,
    'params'     => 3,
]);
$router->add('/api/:controller', [
    'namespace'  => 'app\controllers\api',
    'controller' => 1
]);

// Admin module
//-- Admin login



$router->add('/admin/:controller/:action/:params', [
    'namespace'  => 'app\controllers\admin',
    'controller' => 1,
    'action'     => 2,
    'params'     => 3,
]);

$router->add('/admin/:controller', [
    'namespace'  => 'app\controllers\admin',
    'controller' => 1,
    'action'     => 'index',
]);

$router->add('/admin/login', [
    'namespace' => 'app\controllers\admin',
    'controller' => 'index',
    'action' => 'login'
]); 

$router->add('/admin', [
    'namespace'  => 'app\controllers\admin',
    'controller' => 'index',
    'action'     => 'dashboard'
]);



$router->handle();


return $router;