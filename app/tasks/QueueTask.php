<?php

use Phalcon\Cli\Task;
use app\helpers\FireBase;

use app\models\{Contact, Cards, Users, Notifications};

class QueueTask extends Task
{
    public function mainAction() {
    	$queue = $this->queue;
        while (($job = $queue->peekReady()) !== false) {
		    $message = $job->getBody();

		    foreach ($message as $key => $value) {
		    	$this->$key($value);
		    }

		    $job->delete();
		}
    }

    protected function notifyFriendsCardUpdate($params) {
    	$card = Cards::findFirst($params['card_id']);
    	if (empty($card)) return false;

    	# Notification data
    	$firebaseNotificationData = [
			'title'   => $this->translate->t('notification_friend_update_card', ['name' => $card->name]),
			'body' => $this->translate->t('notification_friend_update_card', ['name' => $card->name]),
    	];

    	$obNotificationData = [
			'title'  => $this->translate->t('notification_friend_update_card'),
			'body'   => $this->translate->t('notification_friend_update_card'),
			'params' => [
				['key' => 'name', 'value' => $card->name]
			]
    	];

    	$contactMaps = Contact::find([
    		"card_id = {$card->id}"
    	]);
    	foreach ($contactMaps as $map) {
    		$targetUser = Users::findFirst($map->user_id);
    		if (empty($targetUser)) continue;

    		echo "Send to @{$targetUser->fullname}".PHP_EOL;
    		# Firebase
    		$fbr = $this->firebase->sendNotification($targetUser->firebase_token, array_merge($firebaseNotificationData, [
    			'click_action' => 'CONTACT_VIEW|'.$map->id])
    		);
    		echo "-- Firebase: ".json_encode($fbr).PHP_EOL;

    		# OB notification
    		$obNotify = new Notifications;
    		$obn = $obNotify->save(array_merge($obNotificationData, [
					'user_id'      => $targetUser->id,
					'click_action' => 'CONTACT_VIEW|'.$map->id,
    			])
    		);
    		echo "-- OB Notification: "; var_dump($obn); echo PHP_EOL;
    	}
    }
}