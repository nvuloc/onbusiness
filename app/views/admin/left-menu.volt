<div class="left_col scroll-view">
	<div class="navbar nav_title" style="border: 0;">
		<a href="index.html" class="site_title">
			<img src="/ob.png" class="img-circle" style="width:50px;height:50px;"/>
			<span>On Business</span>
		</a>
	</div>

	<div class="clearfix"></div>

	<!-- menu profile quick info -->
	<div class="profile clearfix">
		<div class="profile_pic">
			<img src="/img/avatar.jpg" alt="..." class="img-circle profile_img">
		</div>
		<div class="profile_info">
			<span>Welcome,</span>
			<h2>{{ staff.username }}</h2>
		</div>
	</div>
	<!-- /menu profile quick info -->

	<br />

	<!-- sidebar menu -->
	<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
		<div class="menu_section">
			<h3>General</h3>
			<ul class="nav side-menu">
				{% if (userRole is 1) %}
				<li class="{% if controllerName is 'user' %} active {% endif %}">
					<a href="{{ url('/admin/user') }}"><i class="fa fa-users"></i>Người dùng </a>
				</li>
				{% endif %}
				<li class="{% if controllerName is 'card' %} active {% endif %}">
					<a href="{{ url('/admin/card') }}"><i class="fa fa-credit-card"></i>Card </a>
				</li>
				{% if (userRole is 1) %}
				<li class="{% if controllerName is 'contact' %} active {% endif %}">
					<a href="{{ url('/admin/card-print') }}"><i class="fa fa-print"></i>In card </a>
				</li>
				{% endif %}
				{% if (userRole is 1) %}
				<li class="{% if controllerName is 'Applog' %} active {% endif %}">
					<a href="{{ url('/admin/applog') }}"><i class="fa fa-credit-card"></i>Payment </a>
				</li>
				{% endif %}
				{% if (userRole is 1) %}
				<li class="{% if controllerName is 'cardreport' %} active {% endif %}">
					<a href="{{ url('/admin/card-report') }}"><i class="fa fa-warning"></i><?= $t->_('card_error_report') ?></a>
				</li>
				{% endif %}
				{% if (userRole is 1) %}
				<li class="{% if controllerName is 'config' %} active {% endif %}">
					<a href="{{ url('/admin/config') }}"><i class="fa fa-cogs"></i>Config </a>
				</li>
				{% endif %}
				{% if (userRole is 1) %}
				<li class="{% if controllerName is 'config' %} active {% endif %}">
					<a href="{{ url('/admin/feedback') }}"><i class="fa fa-feed"></i>Feedback </a>
				</li>
				{% endif %}
			</ul>
		</div>
	</div>
	<!-- /sidebar menu -->

	<!-- /menu footer buttons -->
	<div class="sidebar-footer hidden-small">
		<a data-toggle="tooltip" data-placement="top" title="Logout" onclick="Ob.logout()">
			<span class="glyphicon glyphicon-off" aria-hidden="true"></span>
		</a>
	</div>
	<!-- /menu footer buttons -->
</div>	