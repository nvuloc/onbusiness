<div class="card-index">
	<div class="page-title">
		<div class="title_left">
			<h3>Báo cáo lỗi </h3>
		</div>
		<div class="title_right">
			<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="x_panel">
				<div class="x_title">
                    <h2>Danh sách báo cáo lỗi</h2>                    
                  	<div class="clearfix"></div>
                </div>
				<div class="x_content">
					<table class="table jambo_table table-bordered">
						<thead>
							<tr class="headings">
								<th class="column-title">#</th>
								<th>Card</th>
								<th class="column-title"><?= $t->_('title') ?></th>
								<th class="column-title"><?= $t->_('content') ?></th>
							</tr>
						</thead>
						<tbody>
							{% for report in page.items %}
							<tr>
								<td>{{ report.id }}</td>
								<td>
									<a href="{{ url('/admin/card/update/' ~ report.card_id) }}">#{{ report.card_id }}</a>
								</td>
								<td>{{ report.title }}</td>
								<td>{{ report.content }}</td>
							</tr>
							{% endfor %}
						</tbody>
					</table>
					{% if page.total_pages > 1 %}
					{{ partial("_paging", ['paginate': page, 'baseUrl': '/admin/card-report/index'] ) }}
					{% endif %}
				</div>
			</div>
		</div>
	</div>
</div>