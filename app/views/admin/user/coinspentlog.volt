<div class="user-payment-log row">
	<div class="x_panel" style="padding-bottom:0px;">
		<div class="x_header">
			<h3>{{ user.fullname }}</h3>
			<p class="animated fadeInDown">
				<a href="{{ url('/admin/user/index') }}">Người dùng</a>				
				<span class="fa-angle-right fa"></span> 
				<a href="{{ url('/admin/user/update/' ~ user.id) }}">{{ user.fullname }}</a>
				<span class="fa-angle-right fa"></span> 
				Log nạp xu			
			</p>
		</div>
	</div>

	<div class="x_panel">
		<div class="x_content">
			<table class="table table-bordered">
				<tr>
					<th>Thời gian</th>
					<th>Hành động</th>
					<th>Số xu</th>
				</tr>
				{% for log in coinspentLogs %}
				<tr>
					<td>{{ log.created_at }}</td>
					<td>
						{{ log.readable_message }}
					</td>
					<td>{{ log.amount }}</td>
				</tr>
				{% endfor %}
			</table>
		</div>
	</div>
</div>