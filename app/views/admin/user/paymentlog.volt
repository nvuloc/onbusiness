<div class="user-payment-log row">
	<div class="x_panel" style="padding-bottom:0px;">
		<div class="x_header">
			<h3>{{ user.fullname }}</h3>
			<p class="animated fadeInDown">
				<a href="{{ url('/admin/user/index') }}">Người dùng</a>				
				<span class="fa-angle-right fa"></span> 
				<a href="{{ url('/admin/user/update/' ~ user.id) }}">{{ user.fullname }}</a>
				<span class="fa-angle-right fa"></span> 
				Log nạp xu			
			</p>
		</div>
	</div>

	<div class="x_panel">
		<div class="x_content">
			<table class="table table-bordered">
				<tr>
					<th>Thời gian</th>
					<th>Gateway</th>
					<th>Kết quả</th>
					<th>Số xu</th>
				</tr>
				{% for log in paymentLogs %}
				<tr>
					<td>{{ log['created_at'] }}</td>
					<td>{{ log['gateway'] }}</td>
					<td>{{ log['status'] }}</td>
					<td><span>{{ log['xu_recharge'] }}</span></td>
				</tr>
				{% endfor %}
			</table>
		</div>
	</div>
</div>