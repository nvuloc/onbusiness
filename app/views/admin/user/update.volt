<div class="user-update row">
	<div class="x_panel" style="padding-bottom:0px;">
		<div class="x_header">
			<h3>{{ user.fullname }}</h3>
			<p class="animated fadeInDown">
				<a href="{{ url('/admin/user/index') }}">Người dùng</a>				
				<span class="fa-angle-right fa"></span> 
				#{{ user.id }}			
			</p>
		</div>
	</div>
	<div class="col-md-8">
		<p><?php $this->flashSession->output(); ?></p>
		<div class="x_panel" style="padding-bottom:0px;">
			<div class="x_title">
				<h2>Thông tin cơ bản</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_body">
				<form class="form-horizontal form-label-left" method="POST">
					<input type="hidden" name="<?= $this->security->getTokenKey() ?>" value="<?= $this->security->getToken() ?>"/>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Họ và tên</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="text" class="form-control" name="Users[fullname]" value="{{ user.fullname }}" required />
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Điện thoại</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="text" class="form-control" value="{{ user.mobile }}" disabled />
						</div>
					</div>
				
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="text" class="form-control" name="Users[email]" value="{{ user.email }}" required />
						</div>
					</div>
				
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Skype</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="text" class="form-control" name="Users[skype]" value="{{ user.skype }}" />
						</div>
					</div>
				
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="dob" >Ngày sinh</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="text" id="dob" class="form-control" name="Users[dob]" value="{{ user.dob }}" />
						</div>
					</div>
				
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">Địa chỉ</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input type="text" class="form-control" name="Users[address]" value="{{ user.address }}" />
						</div>
					</div>
					<div class="ln_solid"></div>
					<div class="form-group">
	                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
	                        <a href="{{ url('/admin/user/index') }}" class="btn btn-primary">Hủy</a>
	                        <button type="submit" class="btn btn-success">Cập nhập</button>
	                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#changepassword">
	                        	Đổi mật khẩu
	                        </button>
	                    </div>
	                </div>
				</form>
			</div>
		</div>
		<!-- Cards -->
		<div class="x_panel">
			<div class="x_title">
				<h2>Danh sách card</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table class="table jambo_table table-bordered">
					<thead>
						<th>#</th>
						<th>Thông tin</th>
						<th>Trạng thái</th>
					</thead>
					<tbody>
						{% for card in user.cards %}
						<tr {% if (card.deleted == 1) %} style="display:none" {% endif %}>
							<td>{{ card.id }}</td>
							<td>
								<a href="{{ url('/admin/card/update/' ~ card.id) }}">
									<b>{{ card.name }}</b><br/>
									Điện thoại: {{ card.mobile }}<br/>
									Địa chỉ: {{ card.address }}
								</a>
							</td>
							<td>{{ card.text_status }}</td>
						</tr>
						{% endfor %}
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="x_panel">
			<div class="x_title">
				<h2><i class="fa fa-money"></i> Số dư tài khoản</h2>
				<button class="btn btn-primary pull-right" type="button" data-toggle="modal" data-target="#addXu">
					<i class="fa fa-fw fa-plus"></i> Thêm xu
				</button>
				<div class="clearfix"></div>
				<div class="modal fade" tabindex="-1" role="dialog" id="addXu">
  					<div class="modal-dialog" role="document">
    					<div class="modal-content">
    						<form method="POST" class="form" action="{{ url('/admin/user/addxu') }}">
      						<div class="modal-header">
        						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        						<h4 class="modal-title">Thêm xu</h4>
      						</div>
      						<div class="modal-body">
      							<input type="hidden" name="uid" value="{{ user.id }}"  />
      							<label>Xu</label>
        						<input type="number" class="form-control" name="xu" />
      						</div>
      						<div class="modal-footer">
						        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						        <button type="submit" class="btn btn-primary">Lưu lại</button>
      						</div>
      						</form>
    					</div><!-- /.modal-content -->
  					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->
			</div>
			<div class="x_content">
				<div class="jumbotron">
	    			<h1><span class="">{{user.balance}}</span></h1>
	    		</div>
    			<a href="{{url ('/admin/user/paymentlog/' ~ user.id) }}" title="Log nạp xu">
    				Log nạp xu	
    			</a> | 
    			<a href="{{url ('/admin/user/coinspentlog/' ~ user.id) }}" title="Log sử dụng xu">
    				Log sử dụng xu
    			</a>
		    </div>
		</div>
		<div class="x_panel">
			<div class="x_title">
				<h2><i class="fa fa-users"></i> Danh bạ</h2>
				<ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                </ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content" style="display:none;">
				<table class="table jambo_table table-bordered">
					<thead>
						<th>#</th>
						<th>Thông tin</th>
					</thead>
					<tbody>
						{% for contact in contactLists %}
						<tr {% if (contact.deleted == 1) %} style="display:none" {% endif %}>
							<td>{{ contact['id'] }}</td>
							<td>
								<b>{{ contact['name'] }}</b><br/>
								Điện thoại: {{ contact['mobile'] }}<br/>
								Địa chỉ: {{ contact['company_name'] }}
							</td>
						</tr>
						{% endfor %}
					</tbody>
				</table>
		    </div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="changepassword">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
    		<form method="POST" action="{{ url('/admin/user/changepassword/' ~ user.id) }}" >
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title">Đổi mật khẩu</h4>
      		</div>
	      	<div class="modal-body">
	        	<div class="form-group">
	        		<label>Mật khẩu mới</label>
	        		<input type="text" class="form-control" name="password" />
	        	</div>	
	        	<div class="form-group">
	        		<label>Xác nhận mật khẩu</label>
	        		<input type="text" class="form-control" name="password_confirmation" />
	        	</div>	
	      	</div>
	      	<div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        <button type="submit" class="btn btn-primary">Cập nhập</button>
	      	</div>
		</div>
		</form>
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->