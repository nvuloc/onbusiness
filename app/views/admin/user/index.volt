<div class="user-index">
	<div class="page-title">
		<div class="title_left">
			<h3>User </h3>
		</div>
		<div class="title_right">
			<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
				<form class="form">
				<!-- <div class="form-group" style="float:left;">
					<select class="form-control" name="select" style="border-radius: 25px;">
						<option value="all">all</option>
						<option value="id">id</option>
						<option value="fullname">name</option>
						<option value="mobile">phone</option>
						<option value="email">email</option>
					</select>
				</div> -->
				<div class="input-group">
					<input class="form-control" placeholder="SĐT hoặc tên người dùng..." type="text" name="search" value="{{ search }}">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button">Go!</button>
					</span>
				</div>
				</form>
			</div>
		</div>
	</div>	
	<div class="row">
		<div class="col-md-12">
			<div class="x_panel">
				<div class="x_title">
                    <h2>Danh sách người dùng</h2>
                    <div class="clearfix"></div>
                </div>
				<div class="x_content">
					<table class="table jambo_table table-bordered">
						<thead>
							<tr class="headings">
								<th class="column-title">#</th>
								<th class="column-title">Họ và tên</th>
								<th class="column-title">SĐT</th>
								<th class="column-title">Email</th>
							</tr>
						</thead>
						<tbody>
							{% for user in page.items %}
				        	<tr>
								<td scope="row"> {{ user.id }}</td>
								<td>
									<a href="{{ url('/admin/user/update/' ~ user.id) }}">{{ user.fullname }}</a>
								</td>
								<td>{{ user.mobile }}</td>
								<td>{{ user.email }}</td>
							</tr>
					    	{% endfor %}
						</tbody>
					</table>
					{% if page.total_pages > 1 %}
					{{ partial("_paging", ['paginate': page, 'baseUrl': '/admin/user/index'] ) }}
					{% endif %}
				</div>
			</div>
		</div>
	</div>
</div>