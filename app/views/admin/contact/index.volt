<div class="card-index">
	<div class="page-title">
		<div class="title_left">
			<h3>Contact </h3>
		</div>
		<div class="title_right">
			<div class="top_search">
				<form class="form">
				<div class="form-group"  style="float:left;">
					<select class="form-control" name="select" style="border-radius: 25px;">
						<option value="id">id</option>
						<option value="user_id">userId</option>
						<option value="card_id">cardId</option>
					</select>
				</div>
				<div class="input-group">
					<input class="form-control" placeholder="Search for..." type="text" name="search">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button">Go!</button>
					</span>
				</div>
				</form>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="x_panel">
				<div class="x_title">
                    <h2>Danh sách contact</h2>
                    <div class="clearfix"></div>
                </div>
				<div class="x_content">
					<table class="table jambo_table table-bordered">
						<thead>
							<tr class="headings">
								<th class="column-title">#</th>
								<th class="column-title">User</th>
								<th class="column-title">Card</th>
							</tr>
						</thead>
						<tbody>
							{% for contact in contacts %}
							<tr>
								<td>{{ contact.id }}</td>
								<td>
									<a href="{{ url('/admin/user/update/' ~ contact.id) }}">
										{{ contact.name_user }}
									<a>
								</td>
								<td>{{ contact.card_id }}</td>
							</tr>
							{% endfor %}
						</tbody>
					</table>
					{% if page.total_pages > 1 %}
					{{ partial("_paging", ['paginate': page, 'baseUrl': '/admin/contact/index'] ) }}
					{% endif %}
				</div>
			</div>
		</div>
	</div>
</div>