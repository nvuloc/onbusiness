<div class="staff-changepassword">
	<div class="page-title">
		<div class="title_left">
			<h3>{{ staff.username }}</h3>
		</div>
	</div>	
	<div class="row">
		<div class="col-md-12">
			<div class="x_panel">
				<div class="x_title">
                    <h2>Đổi mật khẩu</h2>
                    <div class="clearfix"></div>
                </div>
				<div class="x_content row">
					{% if error is not empty %}
					<div class="alert alert-danger">
						{{ error }}
					</div>
					{% endif %}
					<form class="form" method="POST" action="">
						<input type="hidden" name="<?= $this->security->getTokenKey() ?>" value="<?= $this->security->getToken() ?>"/>
						<div class="col-md-6 form-group">
							<label>Mật khẩu</label>
							<input type="password" name="password" class="form-control" />
						</div>
						<div class="col-md-6 form-group">
							<label>Xác nhận mật khẩu</label>
							<input type="password" name="password_confirm" class="form-control" />
						</div>
						<div class="col-md-12 ">
							<button type="submit" class="btn btn-primary pull-right">Cập nhập</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>