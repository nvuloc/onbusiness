<div class="card-index">
	<div class="page-title">
		<div class="title_left">
			<h3>In Card </h3>
		</div>
		<div class="title_right">
			<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
				<form class="form" method="get">
				<div class="input-group">
					<input class="form-control" name="search[field]" type="text" value="{{ search_field }}"/>
					<span class="input-group-btn">
						<button class="btn btn-default" type="submit">Go!</button>
					</span>
				</div>
				<div class="clearfix"></div>
				<div class="input-group">
					<i>Trạng thái</i>: 
					<label for="status_new">Mới</label> 
					<input id="status_new" type="checkbox" name="search[status][]" value="1" {% if 1 in search_status %}checked{% endif %}/>
					<label for="status_proceed">Đang xử lý:</label> 
					<input id="status_proceed" type="checkbox" name="search[status][]" value="2" {% if 2 in search_status %}checked{% endif %}/>
					<label for="status_done">Đã xong</label> 
					<input id="status_done" type="checkbox" name="search[status][]" value="3" {% if 3 in search_status %}checked{% endif %}/>
				</div>
				</form>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="x_panel">
				<div class="x_title">
                    <h2>Danh sách in card</h2>
                    <div class="clearfix"></div>
                </div>
				<div class="x_content">
					<table class="table jambo_table table-bordered">
						<thead>
							<tr class="headings">
								<th class="column-title">#</th>
								<th class="column-title">Thông tin Card</th>
								<th class="column-title">SĐT / Địa chỉ</th>
								<th class="column-title">Hình thức in</th>
								<th class="column-title">Chi phí </th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							{% for order in page.items %}
							<tr>
								<td>{{ order.id }}</td>
								<td>
									<a href="{{ url('/admin/card/update/' ~ order.card.id) }}">
									<b>{{ order.card.name }}</b><br/>
									{{ order.card.mobile}}<br/>
									{{ order.card.company_name }}
									</a>
								</td>
								<td>
									{{ order.mobile }}<br/>
									{{ order.address }}
								</td>
								<td>
									{% if (order.print_type == 1) %}
									<p><label class="label label-primary">In thuờng</label></p>
									{% else %}
									<p><label class="label label-success">In nhanh</label></p>
									{% endif %}
									<p><label>Loại giấy: </label> {{ order.paperType.name }}</p>
								</td>
								<td>{{ order.total }} xu</td>
								<td>
									<form style="display:inline;" method="POST" action="{{ url('/admin/card-print/updatestatus/' ~ order.id) }}">
										<select class="form-control" name="status">
											<option value="1" {% if order.status is 1 %}{{'selected'}}{% endif %}>Mới</option>
											<option value="2" {% if order.status is 2 %}{{'selected'}}{% endif %}>Đang xử lý</option>
											<option value="3" {% if order.status is 3 %}{{'selected'}}{% endif %}>Đã xong</option>
										</select>
										<button type="submit" class="btn btn-default pull-right">Cập nhập</button>
									</form>
								</td>
							</tr>
							{% endfor %}
						</tbody>
					</table>
					{% if page.total_pages > 1 %}
					{{ partial("_pagination", ['paginate': page, 'baseUrl': '/admin/card/index'] ) }}
					{% endif %}
				</div>
			</div>
		</div>
	</div>
</div>