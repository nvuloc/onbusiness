<div class="card-update">
	<div class="x_panel" style="padding-bottom:0px;">
		<div class="x_header">
			<h3>{{ card.name }}</h3>
			<div class="row">
				<div class="col-md-6">
					<p class="animated fadeInDown">
						<a href="{{ url('/admin/card/index') }}">Card</a>				
						<span class="fa-angle-right fa"></span> 
						<a href="{{ url('/admin/user/update/' ~ card.user_id) }}">{{ card.user.fullname }}</a>				
						<span class="fa-angle-right fa"></span> 
						#{{ card.id }}			
					</p>
				</div>
				<div class="col-md-6">
					<a href="{{ url('/admin/card/modifylog/' ~ card.id) }}"><i class="fa fa-ellipsis-h"></i> Log</a> | 
					<a href="{{ url('/admin/user/update/' ~ card.user_id) }}"><i class="fa fa-user"></i> User</a>
				</div>
			</div>
		</div>
	</div>
	<div class="x_panel" style="padding-bottom:0px;">
		<div class="x_body">
			<div class="col-md-12">

			</div>
			{% if errors is not empty %}
			<div class="col-md-12">
				<div class="alert alert-danger">
					<strong>Lỗi!</strong> 
					<ul>
						{% for error in errors %}
							<li>{{ error }}</li>
						{% endfor %}
					</ul>
				</div>
			</div>
			{% endif %}
			<form class="form-horizontal form-label-left" method="POST" enctype="multipart/form-data">
				<input type="hidden" name="<?= $this->security->getTokenKey() ?>" value="<?= $this->security->getToken() ?>"/>
				<!-- Name -->
				<div class="col-md-6">
					<div class="x_panel" style="padding-bottom:0px;">
						<h3>Thông tin card</h3>
						<div class="x_body">
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Họ và tên</label>
								<div class="col-md-9 col-sm-9 col-xs-12">
									<input type="text" class="form-control" name="Cards[name]" value="{{ card.name }}" required />
								</div>
							</div>
							<!-- Mobile -->
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Điện thoại</label>
								<div class="col-md-9 col-sm-9 col-xs-12">
									<input type="text" class="form-control" name="Cards[mobile]" value="{{ card.mobile }}" required />
								</div>
							</div>
							<!-- Email -->
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
								<div class="col-md-9 col-sm-9 col-xs-12">
									<input type="text" class="form-control" name="Cards[email]" value="{{ card.email }}" required />
								</div>
							</div>
							<!-- Address -->
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Địa chỉ</label>
								<div class="col-md-9 col-sm-9 col-xs-12">
									<input type="text" class="form-control" name="Cards[address]" value="{{ card.address }}" />
								</div>
							</div>
							<!-- Company name -->
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Công ty</label>
								<div class="col-md-9 col-sm-9 col-xs-12">
									<input type="text" class="form-control" name="Cards[company_name]" value="{{ card.company_name }}" />
								</div>
							</div>
							<!-- Company address -->
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Công ty địa chỉ</label>
								<div class="col-md-9 col-sm-9 col-xs-12">
									<input type="text" class="form-control" name="Cards[company_address]" value="{{ card.company_address }}" />
								</div>
							</div>
							<!-- Website -->
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Website</label>
								<div class="col-md-9 col-sm-9 col-xs-12">
									<input type="text" class="form-control" name="Cards[website]" value="{{ card.website }}" />
								</div>
							</div>

							<!-- Status -->
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">Trạng thái</label>
								<div class="col-md-9 col-sm-9 col-xs-12">
									<select name="Cards[status]" class="form-control">
										{% for id, status in card.statusTranslate %}
										<option value="{{ id }}" {{ id == card.status ? 'selected':'' }}>{{ status }}</option>
										{% endfor %}
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12">QR code</label>
								<div class="col-md-9 col-sm-9 col-xs-12">
									<div id="qrcode"></div>
									<script type="text/javascript">
									new QRCode(document.getElementById("qrcode"), "{{ card.id }}");
									</script>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Image -->
				<div class="col-md-6">
					<div class="x_panel" style="padding-bottom:0px;">
						<div class="x_body">
							<div class="form-group">
								<label>Hình ảnh user upload</label>
								<div class="col-md-12 img-user-upload">
									{% for image in card.image_user_upload %}										
										<div class="col-md-3 image-wrapper">
											<a href="{{ image['location'] }}" target="_blank"><img src="{{ image['location'] }}" class="img-thumbnail" /></a>
											{% if image['type'] == 'uf' %}
											<label class="label label-info">Mặt truớc</label>
											{% else %}
											<label class="label label-warning">Mặt sau</label>
											{%endif%}
										</div>										
									{% endfor %}
								</div>
							</div>
						</div>
					</div>
					<div class="x_panel" style="padding-bottom:0px;">
						<div class="x_body">
							<div class="form-group">
								<label>Hình ảnh admin upload</label>
								<div class="col-md-12 form-group img-admin-upload">
									{% for image in card.image_admin_upload %}
										<div class="col-md-3 image-wrapper">
											<img src="{{ image['location'] }}" class="img-thumbnail" />
											{% if image['type'] == 'af' %}
											<label class="label label-info">Mặt truớc</label>
											{% else %}
											<label class="label label-warning">Mặt sau</label>
											{%endif%}
										</div>
									{% endfor %}
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label col-md-3">Ảnh trước</label>
										<div class="col-md-9">
											<input type="file" name="front_img" />
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Ảnh sau</label>
										<div class="col-md-9">
											<input type="file" name="back_img" />
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="clearfix"></div>
				<div class="ln_solid"></div>				
				<div class="form-group">
					<a href="{{ url('/admin/card/index') }}" class="btn btn-default">Quay lại</a>
					<button type="submit" class="btn btn-success">Cập nhập</button>
				</div>
			</form>
		</div>
	</div>
</div>