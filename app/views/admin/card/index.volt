<div class="card-index">
	<div class="page-title">
		<div class="title_left">
			<h3>Card </h3>
		</div>
		<div class="title_right">
			<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
				<form class="form">
					<div class="input-group">
						<input class="form-control" placeholder="SĐT nguời dùng hoặc SĐT trên card..." type="text" name="search[user_mobile]" value="{{ search['user_mobile'] }}">
						<span class="input-group-btn">
							<button class="btn btn-default" type="button">Go!</button>
						</span>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="x_panel">
				<div class="x_title">
                    <h2>Danh sách card</h2>
                    
                    <ul class="nav navbar-right panel_toolbox">
                    	<li class="dropdown">
                      		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-filter"></i> Lọc theo trạng thái</a>
                      		<ul class="dropdown-menu" role="menu">
                      			<li><a href="{{ url('/admin/card/index') }}">Tất cả</a></li>
                      			<li role="presentation" class="divider"></li>
                        		<li><a href="{{ url('/admin/card/index?filter[status]=1') }}">Mới</a></li>
                        		<li><a href="{{ url('/admin/card/index?filter[status]=2') }}">Đang xử lý</a></li>
                        		<li><a href="{{ url('/admin/card/index?filter[status]=3') }}">Đã xử lý</a></li>
                        		<li><a href="{{ url('/admin/card/index?filter[status]=4') }}">Hủy</a></li>
                      		</ul>
                    	</li>
                  	</ul>
                  	<div class="clearfix"></div>
                </div>
				<div class="x_content">
					<table class="table jambo_table table-bordered">
						<thead>
							<tr class="headings">
								<th class="column-title">#</th>
								<th class="column-title">Thông tin</th>
								<th class="column-title">Trạng thái</th>
							</tr>
						</thead>
						<tbody>
							{% for card in cards.items %}
							<tr>
								<td>{{ card.id }}</td>
								<td>
									<a href="{{ url('/admin/card/update/' ~ card.id) }}">
										<b>{{ card.name }}</b><br/>
										Điện thoại: {{ card.mobile }}<br/>
										Địa chỉ: {{ card.address }}
									<a>
								</td>
								<td>{{ card.text_status|e }}</td>
							</tr>
							{% endfor %}
						</tbody>
					</table>
					{% if cards.total_pages > 1 %}
					{{ partial("_paging", ['paginate': cards, 'baseUrl': '/admin/card/index'] ) }}
					{% endif %}
				</div>
			</div>
		</div>
	</div>
</div>