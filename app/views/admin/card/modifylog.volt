<div class="card-log">
	<div class="x_panel" style="padding-bottom:0px;">
		<div class="x_header">
			<h3>{{ card.name }}</h3>
			<p class="animated fadeInDown">
				<a href="{{ url('/admin/card/index') }}">Card</a>				
				<span class="fa-angle-right fa"></span> 
				<a href="{{ url('admin/card/update/' ~ card.id) }}">#{{ card.id }}</a>
				<span class="fa-angle-right fa"></span> 
				Log chỉnh sửa
			</p>
		</div>
	</div>
	<div class="x_panel" style="padding-bottom:0px;">
		<div class="x_body">
			<table class="table jambo_table table-bordered">
				<thead>
					<tr class="headings">
						<th class="column-title">Thời gian</th>
						<th class="column-title">Nội dung</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($logs as $log) : ?>
					<tr>
						<td><?= $log->created_at ?></td>
						<td><?= json_encode(unserialize($log->content)) ?></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>