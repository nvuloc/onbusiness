<div>Trang {{ paginate.current }} / {{ paginate.total_pages }}</div>
<ul class="pagination">
	{% if paginate.current != 1 %}
	<li><a href="{{ url(baseUrl) }}">Trang đầu</a></li>
	{% endif %}

	<li><a href="{{ url(baseUrl ~ '?page= ' ~ cards.before ) }}"><i class="fa fa-angle-double-left"></i></a></li>
	<li><a href="{{ url(baseUrl ~ '?page= ' ~ cards.next ) }}"><i class="fa fa-angle-double-right"></i></a></li>

	{% if paginate.current != paginate.last %}
	<li><a href="{{ url(baseUrl ~ '?page=' ~ paginate.last) }}">Trang cuối</a></li>
	{% endif %}
</ul>