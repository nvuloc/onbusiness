<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>On Business |  {{ title }}</title>
		<link rel="apple-touch-icon" href="/ob.png">
    	<link rel="shortcut icon" href="/ob.png">

		<!-- Bootstrap -->
		<link href="/gentelella/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
		<!-- Font Awesome -->
		<link href="/gentelella/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
		<link href="/css/ob.css" rel="stylesheet" />
		
		{{ assets.outputCss() }}

		<!-- Custom Theme Style -->
		<link href="/gentelella/build/css/custom.min.css" rel="stylesheet">
		
		<!-- jQuery -->
		<script src="/gentelella/vendors/jquery/dist/jquery.min.js"></script>
		<!-- Bootstrap -->
		<script src="/gentelella/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
	</head>

	<body class="nav-md">
		{{ assets.outputJs("library") }}
		<div class="container body">
			<div class="main_container">
				<div class="col-md-3 left_col">
					{% include "left-menu" with ['controllerName': controllerName] %}
				</div>

				<!-- top navigation -->
				<div class="top_nav">
					<div class="nav_menu">
						<nav>
							<div class="nav toggle">
								<a id="menu_toggle"><i class="fa fa-bars"></i></a>
							</div>

							<ul class="nav navbar-nav navbar-right">
								<li class="">
									<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
										<img src="/img/avatar.jpg" alt="">{{ staff.username }}
										<span class=" fa fa-angle-down"></span>
									</a>
									<ul class="dropdown-menu dropdown-usermenu pull-right">
										<li><a href="{{ url('/admin/staff/changepassword') }}" ><i class="fa fa-password pull-right"></i> Đổi mật khẩu</a></li>
										{% if (userRole is 1) %}
										<li>
											<a href="{{ url('/admin/staff/chpwddesigner') }}">Reset mật khẩu designer</a>
										</li>
										{% endif %}
										<li><a href="#" role="button" onclick="Ob.logout()"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
										<form style="display:none" id="logout" action="{{ url('/admin/index/logout') }}"></form>
									</ul>
								</li>
							</ul>
						</nav>
					</div>
				</div>
				<!-- /top navigation -->

				<!-- page content -->
				<div class="right_col" role="main">
					{{ content() }}
				</div>
				<!-- /page content -->

				<!-- footer content -->
				<footer>
					<div class="pull-right">
						Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
					</div>
					<div class="clearfix"></div>
				</footer>
				<!-- /footer content -->
			</div>
		</div>

				
		
		<!-- Custom Theme Scripts -->
		<script src="/gentelella/build/js/custom.min.js"></script>
		<script src="/js/autoNumeric.min.js"></script>
		<script>
		(function($, Ob) {
			var Ob = {};
			Ob.logout = function() {
				$("form#logout").submit();
				$("form#logout").trigger('submit');
			};
			window.Ob = Ob;
		})(window.jQuery, window.Ob);
		$(function() {
			$('.money-format').autoNumeric('init', {aPad:false});
		})
		</script>
		<!-- /gauge.js -->
	</body>
</html>
