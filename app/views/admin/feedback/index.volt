<div class="feedback-index">
	<div class="page-title">
		<div class="title_left">
			<h3>Feedback </h3>
		</div>
		<div class="title_right">
			<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
				<form class="form">
				<div class="input-group">
					<input class="form-control" placeholder="SĐT hoặc tên người dùng..." type="text" name="search" value="{{ search }}">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button">Go!</button>
					</span>
				</div>
				</form>
			</div>
		</div>
	</div>	
	<div class="row">
		<div class="col-md-12">
			<div class="x_panel">
				<div class="x_title">
                    <h2>Danh sách feedback</h2>
                    <div class="clearfix"></div>
                </div>
				<div class="x_content">
					<table class="table jambo_table table-bordered">
						<thead>
							<tr class="headings">
								<th class="column-title">#</th>
								<th class="column-title">Tiêu đề</th>
								<th class="column-title">Nội dung</th>
							</tr>
						</thead>
						<tbody>
							{% for feedback in data.items %}
				        	<tr>
								<td>{{ loop.index }}</td>
								<td scope="row"> {{ feedback.title }}</td>
								<td>
									{{ feedback.content }}
								</td>
							</tr>
					    	{% endfor %}
						</tbody>
					</table>
					{% if data.total_pages > 1 %}
					{{ partial("_paging", ['paginate': data, 'baseUrl': '/admin/feedback/index'] ) }}
					{% endif %}
				</div>
			</div>
		</div>
	</div>
</div>