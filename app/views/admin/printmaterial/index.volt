<div class="card-index">
	<div class="page-title">
		<div class="title_left">
			<h3>Chất liệu in </h3>
		</div>
		<div class="title_right">
			<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
				<form class="form">
				<div class="input-group">
					<input class="form-control" placeholder="Search for..." type="text">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button">Go!</button>
					</span>
				</div>
				</form>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="x_panel">
				<div class="x_title">
                    <h2>Chất liệu in</h2>
                    <div class="clearfix"></div>
                </div>
				<div class="x_content">

					<div>
						<a href="#" class="btn btn-primary">
							<i class="fa fa-fw fa-plus"></i> Tạo mới
						</a>
					</div>
					<table class="table jambo_table table-bordered">
						<thead>
							<tr class="headings">
								<th class="column-title">#</th>
								<th class="column-title">Tên</th>
								<th class="column-title">Loại</th> 
								<th class="column-title">Giá</th> 
								<th class="column-title">Trạng thái</th>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
					
				</div>
			</div>
		</div>
	</div>
</div>