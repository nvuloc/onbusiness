<div class="config-index">
	<div class="page-title">
		<div class="title_left">
			<h3>Config </h3>
		</div>
		<div class="title_right">
			<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
				<form class="form">
					<div class="input-group">
						<input class="form-control" placeholder="" type="text" name="search[user_mobile]">
						<span class="input-group-btn">
							<button class="btn btn-default" type="button">Go!</button>
						</span>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="">
		<div class="col-md-12">
			<div class="x_panel">
				<div class="x_title">
				</div>
				<div class="x_content">
					<div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                      	<div class="panel">
                        	<a class="panel-heading collapsed" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                          		<h4 class="panel-title"><i class="fa fa-fw fa-gear"></i> Configs</h4>
                        	</a>
                        	<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="">
                          		<div class="panel-body">
                            		<form class="form form-horizontal" action="{{ url('/admin/config/storeObSetting') }}" method="POST">
                            			<div class="col-md-6">
                            				<div class="form-group">
                            					<label class="control-label col-md-4">Số card đuợc miễn phí</label>
                            					<div class="col-md-8">
                            						<input type="number" name="ObMaxFreeCard" value="{{ obConfig['onbusiness.ObMaxFreeCard'] }}" class="form-control" />
                            					</div>
                            				</div>
                            			</div>
                            			<div class="col-md-6">
                            				<div class="form-group">
                            					<label class="control-label col-md-4">Chi phí thiết kế card</label>
                            					<div class="col-md-8">
                            						<input type="number" name="ObCardDesignFee" value="{{ obConfig['onbusiness.ObCardDesignFee'] }}" class="form-control" />
                            					</div>
                            				</div>
                            			</div>
                            			<div class="col-md-12">
                            				<button class="btn btn-primary pull-right" type="submit">Cập nhập</button>
                            			</div>
                            		</form>
                          		</div>
                        	</div>
                      	</div>
                  		<div class="panel">
                    		<a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                     			<h4 class="panel-title"><i class="fa fa-fw fa-mobile-phone"></i>Mobile app setting</h4>
                    		</a>
                    		<div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" style="">
                      			<div class="panel-body">
                        			<form class="form form-horizontal" action="{{ url('/admin/config/storeMobileSetting') }}" method="POST">
		                        		<!-- iOS -->
		                          		<div class="col-md-6">
		                          			<div class="form-group" style="text-align: center;">
		                          				<h3><i class="fa fa-apple"></i></h3>
		                          			</div>
		                          			<div class="form-group">
		                          				<label class="control-label col-md-3">Publish version</label>
		                          				<div class="col-md-9">
		                          					<input type="text" class="form-control" value="{{ obConfig['iOS.iOSPublishVersion'] }}" name="iOSPublishVersion" />
		                          				</div>
		                          			</div>
		                          			<div class="form-group">
		                          				<label class="control-label col-md-3">App version</label>
		                          				<div class="col-md-9">
		                          					<input type="text" class="form-control" name="iOSAppVersion" value="{{ obConfig['iOS.iOSAppVersion'] }}" />
		                          				</div>
		                          			</div>
		                          		</div>
		                          		<!-- Android -->
		                          		<div class="col-md-6">
		                          			<div class="form-group" style="text-align: center;">
		                          				<h3><i class="fa fa-android"></i></h3>
		                          			</div>
		                          			<div class="form-group">
		                          				<label class="control-label col-md-3">App version</label>
		                          				<div class="col-md-9">
		                          					<input type="text" class="form-control" name="AdVersion" value="{{ obConfig['android.AdVersion'] }}" />
		                          				</div>
		                          			</div>
		                          			<div class="form-group">
		                          				<label class="control-label col-md-3">Download new version</label>
		                          				<div class="col-md-9">
		                          					<input type="text" class="form-control" name="AdNewVersionLink" value="{{ obConfig['android.AdNewVersionLink'] }}" />
		                          				</div>
		                          			</div>
		                          		</div>
		                          		<div class="col-md-12">
		                          			<button class="btn btn-primary pull-right" type="submit">Cập nhập</button>
		                          		</div>
                      				</form>
                      			</div>
                    		</div>
                  		</div>
                  		<div class="panel">
                        	<a class="panel-heading collapsed" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                          		<h4 class="panel-title"><i class="fa fa-fw fa-print"></i> Chất liệu in</h4>
                        	</a>
                        	<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="">
                          		<div class="panel-body">
                            		<form class="form form-horizontal" action="{{ url('/admin/config/storeCardPrintMaterial') }}" method="POST">
                            			<table class="table">
                            				<tr>
                            					<th>#</th>
                            					<th>Tên</th>
                            					<th>Giá</th>
                            				</tr>
	                            			{% for material in cardPrintMaterial %}
	                            			<tr>
	                            				<td>{{ material.id }}</td>
	                            				<td><input class="form-control" value="{{ material.name }}" name="material[{{material.id}}][name]" /></td>
	                            				<td><input class="form-control" value="{{ material.price }}" name="material[{{material.id}}][price]" /></td>
	                            			</tr>
	                            			{% endfor %}
	                            		</table>
	                            		<div class="col-md-12">
	                            			<button type="submit" class="btn btn-primary pull-right">Cập nhập</button>
	                            		</div>
                            		</form>
                          		</div>
                        	</div>
                      	</div>
                      	<div class="panel">
                        	<a class="panel-heading collapsed" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                          		<h4 class="panel-title"><i class="fa fa-fw fa-print"></i> Loại in</h4>
                        	</a>
                        	<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="">
                          		<div class="panel-body">
                            		<form class="form form-horizontal" action="{{ url('/admin/config/storeobconfig') }}" method="POST">
                            			<div class="col-md-6">
                            				<div class="form-group">
                            					<label>In thường</label>
                            					<input type="number" class="form-control" value="{{ obConfig['onbusiness.ObPrintTypeNormal'] }}" name="config[ObPrintTypeNormal]"/>
                            				</div>
                            			</div>
                            			<div class="col-md-6">
                            				<div class="form-group">
                            					<label>In nhanh</label>
                            					<input type="number" class="form-control" value="{{ obConfig['onbusiness.ObPrintTypeFast'] }}" name="config[ObPrintTypeFast]" />
                            				</div>
                            			</div>
	                            		<div class="col-md-12">
	                            			<button type="submit" class="btn btn-primary pull-right">Cập nhập</button>
	                            		</div>
                            		</form>
                          		</div>
                        	</div>
                      	</div>
                    </div>                    
				</div>
			</div>
		</div>
	</div>
</div>
