<div>Trang {{ paginate.current }} / {{ paginate.total_pages }}</div>
<ul class="pagination">
	{% if paginate.current != 1 %}
	<li><a href="{{ url(baseUrl) }}">Trang đầu</a></li>
	{% endif %}

	{% if paginate.current > 1 %}
		<li><a href="{{ url(baseUrl ~ '?page=' ~ (paginate.current - 1)) }}">{{ paginate.current - 1}}</a></li>
	{% endif %}

	<li><a style="background:#e0e0e4" href="{{ url(baseUrl ~ '?page=' ~ paginate.current) }}">{{ paginate.current }}</a></li>

	{% if paginate.current < paginate.total_pages %}
		<li><a href="{{ url(baseUrl ~ '?page=' ~ (paginate.current + 1)) }}">{{ paginate.current + 1}}</a></li>
	{% endif %}

	{% if paginate.current != paginate.last %}
	<li><a href="{{ url(baseUrl ~ '?page=' ~ paginate.last) }}">Trang cuối</a></li>
	{% endif %}
</ul>