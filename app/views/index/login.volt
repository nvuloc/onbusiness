<div class="row"><div class="col-md-12">
	<form id="my_form" action="/api/user/verifyphone" method="POST">
		<div class="form-group">
				<label>Enter country code (e.g. +1):</label>
				<input type="text" id="country_code" class="form-group" />
		</div>
		<div class="form-group">
				<label>Enter phone number without spaces (e.g. 444555666):</label>
				<input type="text" id="phone_num" class="form-group" />
		</div>
		<button onclick="phone_btn_onclick();" type="button">Login via SMS</button>
		<input type="text" id="code" />
		<input type="text" id="csrf_nonce" />

	</form>
</div></div>

<script>
	// initialize Account Kit with CSRF protection
	AccountKit_OnInteractive = function(){
		AccountKit.init(
			{
				appId:241841336162962, 
				state:"e9159d1850434ac28ff96df0e9b1da8f", 
				version:"v1.0"
			}
		);
	};

	// login callback
	function loginCallback(response) {
		console.log(response);
		if (response.status === "PARTIALLY_AUTHENTICATED") {
			document.getElementById("code").value = response.code;
			document.getElementById("csrf_nonce").value = response.state;
			// document.getElementById("my_form").submit();
		}
		else if (response.status === "NOT_AUTHENTICATED") {
			// handle authentication failure
		}
		else if (response.status === "BAD_PARAMS") {
			// handle bad parameters
		}
	}

	// phone form submission handler
	function phone_btn_onclick() {
		var country_code = document.getElementById("country_code").value;
		var ph_num = document.getElementById("phone_num").value;
		AccountKit.login('PHONE', 
			{countryCode: country_code, phoneNumber: ph_num}, // will use default values if this is not specified
			loginCallback);
	}

</script>