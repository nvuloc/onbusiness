<?php
namespace app\models;

use Phalcon\Translate\Adapter\NativeArray;

use app\helpers\FireBase;

class PaymentLogs extends \Phalcon\Mvc\Model
{
    const STATUS_SUCCESS = 1;
    const STATUS_FAIL = 2;

    const GATEWAY_INAPPPURCHASE = 1;
    const GATEWAY_GOOGLEPAID    = 2;
    const GATEWAY_NGANLUONG     = 3;
    const GATEWAY_ADMINADD      = 4;
    
    public $id;
    public $user_id;
    public $gateway;
    public $status;
    public $error_message;
    public $content;
    public $created_at;
    public $amount;
    public $xu_recharge;


    protected $xu;
    protected $translate = null;

    private $inAppPurchaseProducts = [
        'com.khoaphan.onlinebusiness.purchase45' => 45,
        'com.khoaphan.onlinebusiness.purchase75' => 110,
        'com.khoaphan.onlinebusiness.purchase350' => 500,
        'com.khoaphan.onlinebusiness.purchase700' => 1000,
    ];

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("tfl_onbusiness");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'payment_logs';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return PaymentLogs[]|PaymentLogs
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return PaymentLogs
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public static function getLog($userId, $all = false) {
        $rawData = self::find([
            'user_id = '.$userId.(!$all ? ' AND status = '.self::STATUS_SUCCESS : '' ),
            'order' => 'created_at desc',
            'limit' => '10'
        ]);

        $result = [];
        foreach ($rawData as $payment) {
            $params = [
                'gateway'    => $payment->textGateway(),
                'status'     => $payment->textStatus(),
                'amount'     => $payment->amount,
                'error'      => $payment->error,
                'created_at' => $payment->created_at,
                'xu_recharge'=> $payment->xu_recharge,
            ];
            if ($all) {
                $params['content'] = $payment->content;
            }

            $result[] = $params;
        }
        return $result;
    }

    /**
     * Translate numeric to text
     * @return String
     */
    public function textGateway () {
        $reference = [
            self::GATEWAY_INAPPPURCHASE => 'In-app Purchase',
            self::GATEWAY_GOOGLEPAID    => 'Google Paid',
            self::GATEWAY_NGANLUONG     => 'Ngan Luong',
            self::GATEWAY_ADMINADD      => 'Admin',
        ];
        return $reference[$this->gateway];
    }

    /**
     * Translate numeric to text
     * @return String
     */
    public function textStatus () {
        $reference = [
            self::STATUS_SUCCESS => 'Thành công',
            self::STATUS_FAIL    => 'Thất bại'
        ];
        return $reference[$this->status];
    }

    /**
     * Convert to xu
     */
    public function getXu() {
        if ($this->gateway == self::GATEWAY_NGANLUONG) {
            $amount = intval($this->amount);
            $conversion = 1/1000;
            return ($amount * $conversion);  
        }
        elseif ($this->gateway == self::GATEWAY_INAPPPURCHASE) {
            $productIds = $this->inAppPurchaseProducts;
            return $productIds[$this->amount] ?? 0;
        }
        return 0;
    }


    public function beforeSave() {
        $appLogs = new AppLogs;
        if ( !empty($this->amount && $this->status == self::STATUS_SUCCESS) )  {
            try {
                $appLog = new AppLogs;
                $appLog->user_id = $this->user_id;
                $appLog->title   = $this->_t('log_payment_recharge_title');
                $appLog->message = $this->_t('log_payment_recharge_success', ['amount' => $this->getXu()]);
                $appLog->type    = AppLogs::TYPE_COIN_RECHARGE;
                if (!$appLog->save()) {
                    throw new \Exception('Log error');
                }
            } catch (\Exception $e) {
                print_r($appLog->getMessages());
                die;
            }
        }
    }

    protected function _t($key, $params = null) {
        if (empty($this->translate)) {
             // Translate resource
            $messages = $this->getDI()->get('config')->onbusiness->messages->toArray();
            $this->translate = new NativeArray(['content' => $messages]);
        }
        return $this->translate->t($key, $params);
    }

    
    public function validateAppPurchaseProduct($productId) {
        return array_key_exists($productId, $this->inAppPurchaseProducts);
    }

}
