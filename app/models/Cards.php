<?php
namespace app\models;

use Phalcon\Validation;
use Phalcon\Validation\Validator\Email as EmailValidator;
use Phalcon\Validation\Validator\PresenceOf as PresentValidator;
use Phalcon\Mvc\Model\Message;
use Phalcon\Mvc\Url;
use Phalcon\Translate\Adapter\NativeArray;

use app\helpers\FireBase;

class Cards extends \Phalcon\Mvc\Model
{
    const DEFAULT = 1;

    /** Status */
    const STATUS_NEW     = 1;
    const STATUS_PROCEED = 2;
    const STATUS_DONE    = 3;
    const STATUS_CANCEL  = 4;

    /** Scenario used for API */
    const SCENARIO_INITDEAFULT = 'initDefault';
    const SCENARIO_ADDNEW      = 'addNew';
    const SCENARIO_UPDATE      = 'update';

    public $id;
    public $user_id;
    public $name;
    public $mobile;
    public $email;
    public $address;
    public $company_name;
    public $company_mobile;
    public $compary_address;    
    public $website;    
    public $status;    
    public $deleted;    
    public $created_at;
    public $is_default;
    public $updated_at;
    public $cfront_img;
    public $cback_img;

    public $statusTranslate = [
        self::STATUS_NEW     => 'Mới',
        self::STATUS_PROCEED => 'Đang xử lý',
        self::STATUS_DONE    => 'Đã xử lý',
        self::STATUS_CANCEL  => 'Hủy',
    ];


    public $scenario = null;

    protected $front_img;
    protected $back_img;

    protected $text_status;

    protected $image_user_upload;
    protected $image_admin_upload;
    protected $last_image_admin_upload;


    /**
     * Validations and business logic
     *
     * @return boolean
     */
    public function validation()
    {
        # Validate images
        #-- init default card
        if ($this->scenario === self::SCENARIO_INITDEAFULT) {
            if ( empty($this->front_img) !== empty($this->back_img) ) {
                $this->appendMessage(
                    new Message('Không được để trống 1 trong 2 hình ảnh card', 'front_img', 'InvalidValue')
                );
                return false;
            }
        }
        #-- add new card
        if ($this->scenario === self::SCENARIO_ADDNEW) {
            if ( empty($this->front_img) || empty($this->back_img) ) {
                $this->appendMessage(
                    new Message('Không được để trống 1 trong 2 hình ảnh card', 'front_img', 'InvalidValue')
                );
                return false;
            }
        }
        #-- default
        if (!empty($this->front_img) && !$this->front_img->file->isImage() ) {
            $this->appendMessage(
                new Message('Định dạng ảnh mặt trước card không hợp lệ', 'front_img', 'InvalidValue')
            );
            return false;
        }

        if (!empty($this->back_img) && !$this->back_img->file->isImage() ) {
            $this->appendMessage(
                new Message('Định dạng ảnh mặt sau card không hợp lệ', 'back_img', 'InvalidValue')
            );
            return false;
        }
        

        # Validate info
        $validator = new Validation();

        $validator->add(
            'email',
            new EmailValidator(
                [
                    'model'   => $this,
                    'message' => 'Email không hợp lệ',
                ]
            )
        );

        $validator->add(['name', 'mobile', 'email'], new PresentValidator([
            'model' => $this,
            'message' => [
                'name' => 'Tên không được để trống',
                'mobile' => 'Số điện thoại không được để trống',
                'email' => 'Email không được để trống'
            ]
        ]));

        return $this->validate($validator);
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("tfl_onbusiness");
        $this->hasMany('id', 'app\\models\\CardModifiedLogs', 'card_id', ['alias' => 'modifylogs']);
        $this->hasMany('id', 'app\\models\\CardShareMaps', 'card_id', ['alias' => 'sharemap']);
        $this->hasMany('id', 'app\\models\\CardImages', 'card_id', ['alias' => 'images']);
        $this->belongsTo('user_id', 'app\\models\\Users', 'id', ['alias' => 'user']);
    }

    public function getTranslate() {
        $messages = $this->getDI()->get('config')->onbusiness->messages->toArray();
        return (new NativeArray(['content' => $messages]));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cards';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Cards[]|Cards
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Cards
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Get general info of card
     * @return [type] [description]
     */
    public function generalAttributes() {
        $attributes = $this->toArray();
        $attributes['front_img'] = $this->getFrontImg();
        $attributes['back_img'] = $this->getBackImg();
        return $attributes;
    }

    /**
     * Translate integer to text
     * @return [type] [description]
     */
    public function getTextStatus() {
        $reference = self::textStatus();
        return $reference[$this->status];
    }

    public static function textStatus() {
        return [
            self::STATUS_NEW     => 'Mới',
            self::STATUS_PROCEED => 'Đang xử lý',
            self::STATUS_DONE    => 'Đã xử lý',
            self::STATUS_CANCEL  => 'Hủy',
        ];
    }


    /**
     * Getter for property $front_img
     */
    public function getFrontImg() {
        return $this->front_img;
    }

    /**
     * Getter for property $back_img
     */
    public function getBackImg() {
        return $this->back_img;
    }

    /**
     * Setter for property $front_img
     */
    public function setFrontImg($input) {
        if (empty($input)) {
            $this->front_img = null;
        } else {
            $this->front_img = new CardImages;
            $this->front_img->file = $input;    
        }
        return $this->front_img;
    }
    /**
     * Setter for property $back_img
     */
    public function setBackImg($input) {
        if (empty($input)) {
            $this->back_img = null;
        } else {
            $this->back_img = new CardImages;
            $this->back_img->file = $input;            
        }
        return $this->back_img;
    }

    /**
     * [API function] Save image attach with card to disk
     */
    public function saveImages() {
        # If no image uploaded, use default
        if ($this->scenario == self::SCENARIO_INITDEAFULT && empty($this->front_img) && empty($this->back_img)) {
            $frontImg = new CardImages;
            $frontImg->save([
                'card_id'  => $this->id,
                'location' => '/img/default_card_front.png',
                'type'     => CardImages::USER_UPLOAD_FRONT
            ]);

            $backImg = new CardImages;
            $backImg->save([
                'card_id'  => $this->id,
                'location' => '/img/default_card_back.png',
                'type'     => CardImages::USER_UPLOAD_BACK
            ]);

            $this->cfront_img = $frontImg->location;
            $this->cback_img = $backImg->location;
            $this->save();
            return true;
        }

        if ( !empty($this->front_img) ) { 
            # Front image
            $this->front_img->card_id = $this->id;
            $this->front_img->type = CardImages::USER_UPLOAD_FRONT;
            $this->front_img->writeToFile();
            $this->front_img->save();
        }
        if ( !empty($this->back_img) ) {
            # Back image
            $this->back_img->card_id = $this->id;
            $this->back_img->type = CardImages::USER_UPLOAD_BACK;
            $this->back_img->writeToFile();
            $this->back_img->save();
        }

        switch ($this->status) {
            case self::STATUS_NEW:
                $this->cfront_img = '/img/default_card_front.png';
                $this->cback_img = '/img/default_card_back.png';
                break;
            default: 
                $this->cfront_img = $frontImg->location;
                $this->cback_img = $backImg->location;
                break;
        }
        $this->save();
        return true;
    }

    /**
     * Get images from cards
     */
    public function getImages() {
        return [
            'front_img' => $this->cfront_img,
            'back_img' => $this->cback_img
        ];

        # Return user uploads images
        if ( in_array($this->status, [self::STATUS_NEW, self::STATUS_PROCEED]) ) {
            $frontImg = CardImages::findFirst([
                "card_id = {$this->id} AND type = '".CardImages::USER_UPLOAD_FRONT."'", 
                'order' => 'created_at desc',
            ]);

            $backImg = CardImages::findFirst([
                "card_id = {$this->id} AND type = '".CardImages::USER_UPLOAD_BACK."'", 
                'order' => 'created_at desc'
            ]);

            return [
                'front_img' => $frontImg->location ?? null,
                'back_img' => $backImg->location ?? null
            ];
        }

        if ( $this->status == self::STATUS_DONE ) {
            if ($this->is_default == self::DEFAULT) {
                $adminImages = $this->getImageAdminUpload();
                if (!empty($adminImages['front_img']) && !empty($adminImages['back_img'])) {
                    return $adminImages;
                } else {
                    $frontImg = CardImages::findFirst([
                        "card_id = {$this->id} AND type = '".CardImages::USER_UPLOAD_FRONT."'", 
                        'order' => 'created_at desc',
                    ]);

                    $backImg = CardImages::findFirst([
                        "card_id = {$this->id} AND type = '".CardImages::USER_UPLOAD_BACK."'", 
                        'order' => 'created_at desc'
                    ]);

                    return [
                        'front_img' => $frontImg->location ?? null,
                        'back_img' => $backImg->location ?? null
                    ];                    
                }

            }
            return $this->getImageAdminUpload();
        }
    }

    /**
     * Get all images uploaded by user
     * @return [type] [description]
     */
    public function getImageUserUpload() {
        $images = CardImages::find([
            "card_id = {$this->id} AND type IN ('".CardImages::USER_UPLOAD_FRONT."', '".CardImages::USER_UPLOAD_BACK."')",
            'order' => 'created_at desc'
        ]);

        return $images->toArray();
    }

    /**
     * Get image uploaded by admin
     * @return [type] [description]
     */
    public function getImageAdminUpload() {
        $image = CardImages::find([
            "type IN ('".CardImages::ADMIN_UPLOAD_FRONT."', '".CardImages::ADMIN_UPLOAD_BACK."') AND card_id = {$this->id}",
            "order" => 'created_at desc'
        ])->toArray();
        return $image;
    }

    /**
     * Get last image  uploaded by admin
     * @return [type] [description]
     */
    public function getLastImageAdminUpload() {
        $frontImg = CardImages::findFirst([
            "card_id = {$this->id} AND type = '".CardImages::ADMIN_UPLOAD_FRONT."'",
            'order'  => 'created_at desc'
        ]);

        $backImg = CardImages::findFirst([
            "card_id = {$this->id} AND type = '".CardImages::ADMIN_UPLOAD_BACK."'",
            'order' => 'created_at desc'
        ]);

        return [
            'front_img' => $frontImg->location ?? null,
            'back_img'  => $backImg->location ?? null
        ];
    }

    /**
     * Notify user about status of card
     * @param  Integer  $statusId 
     * @param  boolean $fb        Send Firebase notification
     * @param  boolean $ob        Onbusiness notification
     */
    public function notifyStatusChange($statusId, $fb = true, $ob = true) {
        if ($this->status == $statusId) return false;
        // Check if status has change
        $t = $this->getTranslate();
        $notificationData = ['click_action' => "CARD_VIEW|{$this->id}"];

        # Send notification via FireBase
        if ($fb) {
            switch ($statusId) {
                case Cards::STATUS_PROCEED:
                    $notificationData['title'] = $t->t('notification_card_status_procceed_title', ['name' => $this->name]);
                    $notificationData['body'] = $t->t('notification_card_status_procceed_body', ['name' => $this->name]);
                    break;
                case Cards::STATUS_DONE:
                    $notificationData['title'] = $t->t('notification_card_status_done_title', ['name' => $this->name]);
                    $notificationData['body'] = $t->t('notification_card_status_done_body', ['name' => $this->name]);
                    break;
                case Cards::STATUS_CANCEL:
                    $notificationData['title'] = $t->t('notification_card_status_cancel_title', ['name' => $this->name]);
                    $notificationData['body'] = $t->t('notification_card_status_cancel_body', ['name' => $this->name]);
                    break;
            }

            $cloudMessageConfig = $this->getDI()->get('config')->firebase->cloud_messaging;
            $firebase = new FireBase($cloudMessageConfig);
            $firebase->sendNotification($this->user->firebase_token, $notificationData);
        }

        # Save on OB notifications
        if ($ob) {
            switch ($statusId) {
                case Cards::STATUS_PROCEED:
                    $notificationData['title'] = $t->t('notification_card_status_procceed_title');
                    $notificationData['body'] = $t->t('notification_card_status_procceed_body');
                    $notificationData['params'] = [
                        ['key' => 'name', 'value' => $this->name]
                    ];
                    break;
                case Cards::STATUS_DONE:
                    $notificationData['title'] = $t->t('notification_card_status_done_title');
                    $notificationData['body'] = $t->t('notification_card_status_done_body');
                    $notificationData['params'] = [
                        ['key' => 'name', 'value' => $this->name]
                    ];
                    break;
                case Cards::STATUS_CANCEL:
                    $notificationData['title'] = $t->t('notification_card_status_cancel_title');
                    $notificationData['body'] = $t->t('notification_card_status_cancel_body');
                    $notificationData['params'] = [
                        ['key' => 'name', 'value' => $this->name]
                    ];
                    break;
            }

            $notify = new Notifications;
            $notify->save(array_merge($notificationData, ['user_id' => $this->user_id]));
        }
        return true;
    }

    public function resetImage() {
        $this->cfront_img = '/img/default_card_front.png';
        $this->cback_img = '/img/default_card_back.png';
        $this->save();
    }
}
