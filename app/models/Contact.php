<?php
namespace app\models;


class Contact extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $user_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $user_target_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $card_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=2, nullable=true)
     */
    public $deleted;

    /**
     *
     * @var integer
     * @Column(type="integer", length=2, nullable=true)
     */
    public $name_user;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $created_at;

    protected $info;
    protected $full_info;
    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("tfl_onbusiness");
        $this->belongsTo('card_id', 'app\\models\\Cards', 'id', ['alias' => 'card']);
        $this->belongsTo('user_target_id', 'app\\models\\Users', 'id', ['alias' => 'friend']);
        $this->belongsTo('user_id', 'app\\models\\Users', 'id', ['alias' => 'me']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'contact';
    }


    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Contact[]|Contact
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Contact
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }


    /**
     * Insert multiple contact
     * @param  [Array] $contacts
     * @return [Boolean]
     */
    public static function insertMultiple ($contacts) {
        $bait = new Contact;

        # Validate
        $validated = array_filter($contacts, function ($elem) {
            if ( empty($elem['user_id']) || empty($elem['user_target_id']) || empty($elem['card_id']) ) {
                return false;
            }
            return true;
        });

        # Input array
        $valueInput = array_map(function($elem) {
            return "( {$elem['user_id']}, {$elem['user_target_id']}, {$elem['card_id']} )";
        }, $validated);

        # SQL execute
        $sql = "INSERT INTO contact (user_id, user_target_id, card_id) VALUES ". implode(',', $valueInput);
        try {
            $bait->getReadConnection()->query($sql);
            return count($validated);
        } catch (\PDOException $e) {
            return false; //$e->getMessage();
        }
    }

    /**
     * Get basic contact info
     * @return Array
     */
    public function getInfo() {
        return array_merge([
            "id"      => $this->id,
            'mobile'  => $this->card->mobile,
            'name'    => $this->card->name,
            'company' => $this->card->company_name
        ], $this->card->getImages());
    }

    /**
     * Get card info with related cards
     * @return Array
     */
    public function getFullInfo() {
        $info = $this->card;

	    $related = [];
    	$cards = Cards::find(["user_id = {$info->user_id} AND mobile = '{$this->mobile}'"]);

        $related[0] = null;
    	foreach ($cards as $card) {
            if ($card->id == $this->card_id) {
                $related[0] = array_merge($card->toArray(), $card->getImages());
            } else {
                $related[] = array_merge($card->toArray(), $card->getImages());
            }
    	}

        if ( count($related) === 0 ) {
            return array_merge($this->getInfo(), ['cards' => [] ]);
        } else {
            return array_merge($this->getInfo(), ['cards' => $related]);
        }
    }
}
