<?php
namespace app\models;

class CardPrintMaterial extends \Phalcon\Mvc\Model
{

    
    public $id;    
    public $name;    
    public $type;
    public $price;
    public $active;
    public $created_at;
    

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("tfl_onbusiness");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'card_print_material';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CardPrintMaterial[]|CardPrintMaterial
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CardPrintMaterial
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
