<?php
namespace app\models;

class AppLogs extends \Phalcon\Mvc\Model
{   
    public $id;
    public $title;
    public $message;
    public $type;
    public $created_at;

    const TYPE_COIN_RECHARGE = 'coin_recharge'; // nap coin
    const TYPE_COIN_CHARGE = 'coin_charge'; // xai coin

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("tfl_onbusiness");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'app_logs';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return AppLogs[]|AppLogs
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return AppLogs
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }
}
