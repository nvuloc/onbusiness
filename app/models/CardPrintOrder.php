<?php
namespace app\models;

class CardPrintOrder extends \Phalcon\Mvc\Model
{    
    public $id;
    public $card_id;
    public $quantity;
    public $total;
    public $paper_type;
    public $print_type;
    public $mobile;
    public $address;
    public $status;
    public $created_at;

    const PAPERTYPE_1 = 1;
    const PAPERTYPE_2 = 2;
    const PAPERTYPE_3 = 3;
    const PAPERTYPE_4 = 4;
    const PAPERTYPE_5 = 5;
    const PAPERTYPE_6 = 6;

    const PRINTTYPE_NORMAL = 1;
    const PRINTTYPE_FAST   = 2;

    const STATUS_NEW     = 1;
    const STATUS_PROCEED = 2;
    const STATUS_DONE    = 3;

    protected $text_status;

    public function initialize()
    {
        $this->setSchema("tfl_onbusiness");
        $this->belongsTo('card_id', 'app\\models\\Cards', 'id', ['alias' => 'card']);
        $this->hasOne('paper_type', 'app\\models\\CardPrintMaterial', 'id', ['alias' => 'paperType']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'card_print_order';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CardPrintOrder[]|CardPrintOrder
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CardPrintOrder
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    
    public function calculateTotal() {
        $materials = CardPrintMaterial::find(["active = 1"])->toArray();
        $paperType = array_combine(array_column($materials, 'id'), $materials);

        // Get print price from DB
        $dbResult = AppConfig::find()->toArray();
        $configKeys = array_map(function($key, $type) { return "{$type}.{$key}"; }, array_column($dbResult, 'key'), array_column($dbResult, 'type'));
        $obConfig = array_combine($configKeys, array_column($dbResult, 'value'));

        $printType = [
            self::PRINTTYPE_NORMAL => $obConfig['onbusiness.ObPrintTypeNormal'],
            self::PRINTTYPE_FAST   => $obConfig['onbusiness.ObPrintTypeFast']
        ];
        
        $this->total = ($paperType[$this->paper_type]['price'] + $printType[$this->print_type]) * $this->quantity;
        return $this->total;
    }

    public function getTextStatus() {
        $translate = [
            self::STATUS_NEW     => 'Mới',
            self::STATUS_PROCEED => 'Đang xử lý',
            self::STATUS_DONE    => 'Đã xử lý',
        ];
        return $translate[$this->status];
    }

}
