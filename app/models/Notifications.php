<?php
namespace app\models;

class Notifications extends \Phalcon\Mvc\Model
{

    public $id;
    public $user_id;
    public $title;
    public $body;
    public $click_action;
    public $params;
    public $created_at;
    public $has_read;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("tfl_onbusiness");
        $this->belongsTo('user_id', 'app\\models\\Users', 'id', ['alias' => 'user']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'notifications';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Notifications[]|Notifications
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Notifications
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function beforeSave() {
        if ( !empty ($this->params) ) {
            $this->params = serialize($this->params);
        } else {
            $this->params = serialize([]);
        }
    }

    public function afterFetch()
    {
        if (!empty($this->params)) {
            $this->params = unserialize($this->params);            
        } else {
            $this->params = [];
        }
    }
}
