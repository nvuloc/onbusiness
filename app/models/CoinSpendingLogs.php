<?php
namespace app\models;

use Phalcon\Translate\Adapter\NativeArray;
use Phalcon\Mvc\Url;

class CoinSpendingLogs extends \Phalcon\Mvc\Model
{

    public $id;
    public $action_type;
    public $action_id;
    public $amount;
    public $created_at;
    
    const ACTIONTYPE_CARDDESIGN = 'card_design';
    const ACTIONTYPE_CARDPRINT = 'card_print';

    protected $translate = null;
    protected $readable_message;

    public function initialize() {
        $this->setSchema("tfl_onbusiness");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'coin_spending_logs';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CoinSpendingLogs[]|CoinSpendingLogs
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CoinSpendingLogs
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * translate
     * @param  string $key
     * @param  array $params
     * @return string
     */
    protected function _t($key, $params = null) {
        if (empty($this->translate)) {
             // Translate resource
            $messages = $this->getDI()->get('config')->onbusiness->messages->toArray();
            $this->translate = new NativeArray(['content' => $messages]);
        }
        return $this->translate->t($key, $params);
    }

    /**
     * Things to be done before save this log
     */
    public function beforeSave() {
        // Log spending coin
        try {
            $applog = new AppLogs;
            $applog->user_id = $this->user_id;
            $applog->type    = AppLogs::TYPE_COIN_CHARGE;
            switch ($this->action_type) {
                case self::ACTIONTYPE_CARDDESIGN:
                    $applog->title = $this->_t('log_coin_spend_card_design_title');
                    $applog->message = $this->_t('log_coin_spend_card_design', ['amount' => $this->amount]);
                    break;
                case self::ACTIONTYPE_CARDPRINT:
                    $applog->title = $this->_t('log_coin_spend_card_print_title');
                    $applog->message = $this->_t('log_coin_spend_card_print', ['amount' => $this->amount]);
                    break;
                default:
                    $applog->message = '';
                    break;
            }
            if (!$applog->save()) {
                throw new \Exception('Error');
            }
        } catch (\Exception $e) {
            print_r($applog->getMessages()); die;
        }
    }

    public function getReadableMessage() {
        $url = new Url;
        if ($this->action_type == self::ACTIONTYPE_CARDDESIGN) {
            $link =  empty($this->action_id) ? '' : '<a href="'.$url->get('/admin/card/update/'.$this->action_id).'">#'.$this->action_id.'</a>';
            return 'Thiết kế card '.$link;
        }
        if ($this->action_type == self::ACTIONTYPE_CARDPRINT) {
            $printOrder = CardPrintOrder::findFirst($this->action_id);
            $link =  empty($this->action_id) ? '' : '<a href="'.$url->get('/admin/card/update/'.$printOrder->card_id).'">#'.$printOrder->card_id.'</a>';
            return 'In card '.$link;
        }
    }

}
