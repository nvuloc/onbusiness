<?php
namespace app\models;

use Phalcon\Validation;
use Phalcon\Mvc\Url;

use app\helpers\Base64Reader;

class CardImages extends \Phalcon\Mvc\Model
{

    const ADMIN_UPLOAD_FRONT = 'af';
    const ADMIN_UPLOAD_BACK  = 'ab';
    const USER_UPLOAD_FRONT  = 'uf';
    const USER_UPLOAD_BACK   = 'ub';
    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $card_id;

    
    public $type;

    
    public $location;

    protected $file;

    

    /**
     * Validations and business logic
     *
     * @return boolean
     */
    public function validation()
    {
        $validator = new Validation();


        return $this->validate($validator);
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("tfl_onbusiness");
        $this->belongsTo('card_id', 'app\\modes\\Cards', 'id', ['alias' => 'card']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'card_images';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Cards[]|Cards
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Cards
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    
    public function setFile($base64String) {
        $this->file = new Base64Reader($base64String);
        return $this->file;
    }

    public function getFile() {
        return $this->file;
    }

    public function writeToFile() {
        $config = $this->getDI()->get('config');
        $fileName = time()."_{$this->type}.".$this->file->extension;

        $this->file->writeToFile($config->application->uploadsDir.$fileName);
        $this->location = '/uploads/'.$fileName;
    }
}
