<?php
namespace app\models;

use Phalcon\Validation;
use Phalcon\Mvc\Url;

class CardErrorReport extends \Phalcon\Mvc\Model
{

    
    public $id;
    public $card_id;
    public $title;
    public $content;
    public $created_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("tfl_onbusiness");
        $this->belongsTo('card_id', '\Cards', 'id', ['alias' => 'Cards']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'card_error_report';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return CardErrorReport[]|CardErrorReport
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return CardErrorReport
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
