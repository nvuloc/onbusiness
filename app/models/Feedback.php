<?php
namespace app\models;

use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf as PresentValidator;

class Feedback extends \Phalcon\Mvc\Model
{
       
    public $id;
    public $user_id;
    public $title;
    public $content;
    public $rate;
    public $status;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("tfl_onbusiness");
        $this->belongsTo('user_id', 'app\\models\\Users', 'id', ['alias' => 'user']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource() {
        return 'feedback';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Feedback[]|Feedback
     */
    public static function find($parameters = null) {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Feedback
     */
    public static function findFirst($parameters = null) {
        return parent::findFirst($parameters);
    }

    public function validation() {
        # Validate info
        $validator = new Validation();
        $validator->add(['title', 'content'], new PresentValidator([
            'model' => $this
        ]));

        return $this->validate($validator);
    }

}
