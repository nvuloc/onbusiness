<?php
namespace app\models;

use Phalcon\Validation;
use Phalcon\Validation\Validator\Email as EmailValidator;
use Phalcon\Validation\Validator\Uniqueness as UniquenessValidator;
use Phalcon\Validation\Validator\PresenceOf as PresenceOfValidator;
use InvalidArgumentException;

use DateTime;

use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\ValidationData;

class Users extends \Phalcon\Mvc\Model
{

	/**
	 *
	 * @var integer
	 * @Primary
	 * @Identity
	 * @Column(type="integer", length=11, nullable=false)
	 */
	public $id;

	/**
	 *
	 * @var string
	 * @Column(type="string", length=12, nullable=false)
	 */
	public $mobile;

	/**
	 *
	 * @var string
	 * @Column(type="string", length=100, nullable=true)
	 */
	public $email;

	/**
	 *
	 * @var string
	 * @Column(type="string", length=100, nullable=false)
	 */
	public $fullname;

	/**
	 *
	 * @var string
	 * @Column(type="string", nullable=true)
	 */
	protected $dob;

	/**
	 *
	 * @var string
	 * @Column(type="string", length=100, nullable=false)
	 */
	public $address;

	/**
	 *
	 * @var integer
	 * @Column(type="integer", length=11, nullable=false)
	 */
	public $fb_user_id;

	/**
	 *
	 * @var string
	 * @Column(type="string", length=255, nullable=true)
	 */
	protected $password_hash;

	public $skype;

	/**
	 *
	 * @var integer
	 * @Column(type="integer", length=11, nullable=true)
	 */
	public $default_card;

	/**
	 *
	 * @var string
	 * @Column(type="string", nullable=false)
	 */
	public $created_at;
	public $balance;
	public $firebase_token;

	/**
	 * Validations and business logic
	 *
	 * @return boolean
	 */
	public function validation()
	{
		$validator = new Validation();

		# Validate Email
		if ( !empty($this->email) ) {
			$validator->add('email',new EmailValidator([
				'model'   => $this,
				'message' => 'Email không hợp lệ',
			]));			
		}
		# Validate mobile unique
		$validator->add('mobile', new UniquenessValidator([
			 'model' => $this,
			 'message' => 'Số điện thoại này đã được đăng ký'
		 ]));
		# Required attributes
		$validator->add( 
			['fullname', 'mobile', 'password_hash', 'fb_user_id'], 
			new PresenceOfValidator([
			 	'model' => $this,
			 	'message' => [
					'fullname'      => 'Tên không được để trống',
					'mobile'        => 'Điện thoại không được trống',
					'password_hash' => 'Password không được trống',
					'fb_user_id'    => 'Điện thoại chưa được xác thực'
			 	]
		]));

		return $this->validate($validator);
	}

	/**
	 * Initialize method for model.
	 */
	public function initialize()
	{
		$this->setSchema("tfl_onbusiness");
		$this->hasMany('id', 'app\\models\\Cards', 'user_id', ['alias' => 'cards']);
		$this->hasMany('id', 'app\\models\\Contact', 'user_id', ['alias' => 'contact']);
	}

	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource()
	{
		return 'users';
	}

	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters
	 * @return Users[]|Users
	 */
	public static function find($parameters = null)
	{
		return parent::find($parameters);
	}

	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters
	 * @return Users
	 */
	public static function findFirst($parameters = null)
	{
		return parent::findFirst($parameters);
	}

	/**
	 * Generate password hash
	 * @param  string $password 
	 * @return string;
	 */
	public static function genratePassword($password) {
		return password_hash($password, PASSWORD_DEFAULT);
	}

	public function setPasswordHash($password) {
		$this->password_hash = self::genratePassword($password);
		return true;
	}

	public function getPasswordHash() {
		return $this->password_hash;
	}

	/**
	 * Get facebook user id registered with Account kit
	 * @param string $code Facebook return access_token
	 */
	public function setFbUserId($access_token) {
		$facebookConfig = $this->getDI()->get('config')->facebook;
				
		$appsecret_proof = hash_hmac('sha256', $access_token, $facebookConfig->account_kit_secrect);
		$params = [
			'access_token' => $access_token,
			'appsecret_proof' => $appsecret_proof
		];	
		$ch = curl_init('https://graph.accountkit.com/v1.1/me/?'.http_build_query($params));
		curl_setopt_array($ch, [
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HTTPHEADER => [
				'Content-Type: application/json',
				'Accept: application/json'
			]
		]);
		$result = curl_exec($ch);
		curl_close($ch);
		$userInfo = json_decode($result, true);
		//-- Fail request
		if ( !isset($userInfo['id']) ) {
			return false;
		}
		$this->fb_user_id = $userInfo['id'];
		return $this->fb_user_id;
	}

	/**
	 * Set date of birth
	 * @param int $dob Timestamp
	 */
	public function setDob($dob) {
		$date = DateTime::createFromFormat('d-m-Y', $dob);
		$this->dob = $date->format('Y-m-d');
		return $this->dob;
	}

	/**
	 * Get date of birth
	 * @return string
	 */
	public function getDob() {
		if (empty($this->dob)) return '';
		$date = DateTime::createFromFormat('Y-m-d', $this->dob);
		return $date->format('d-m-Y');
	}

	/**
	 * Generate Token
	 * @return [type] [description]
	 */
	public function generateToken() {
		$appConfig = $this->getDI()->get('config')->application;
		$token = (new Builder())
						->setId($appConfig->jwtId, true) // Configures the id (jti claim), replicating as a header item
						->setIssuedAt(time()) // Configures the time that the token was issue (iat claim)
						->setExpiration(time() + 3600*24*30) // Configures the expiration time of the token (nbf claim)
						->set('uid', $this->id) // Configures a new claim, called "uid"
						->getToken(); // Retrieves the generated token
		return $token;
	}

	/**
	 * Public attributes
	 * @return [type] [description]
	 */
	public function publicAttributes() {
		$publicAttr = ['id', 'mobile', 'fullname', 'email', 'dob', 'address', 'skype', 'balance'];
		$attributes = array_merge($this->toArray(), ['dob' => $this->getDob()]);

		return array_filter($attributes, function($key) use ($publicAttr) { 
			return in_array($key, $publicAttr); 
		}, ARRAY_FILTER_USE_KEY);
	}
}
